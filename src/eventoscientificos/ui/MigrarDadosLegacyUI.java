/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controller.ImportarEventosController;
import eventoscientificos.controller.MigrarDadosLegacyController;
import eventoscientificos.model.*;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

/**
 *
 * @author Tiago
 */
public class MigrarDadosLegacyUI extends JFrame {

    JPanel jp1, jp2;
    JLabel jl_questao;
    JButton jb_OK, jb_Cancelar;
    Empresa m_empresa;
    private ImportarEventosController m_controllerIE;

    public MigrarDadosLegacyUI(Empresa m_empresa) {
        super("Importação");
        this.m_empresa = m_empresa;
        m_controllerIE = new ImportarEventosController(this.m_empresa);
        GridLayout g1 = new GridLayout(2, 1);
        setLayout(g1);
        criarComponentes();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        setLocation(600, 350);
        pack();
        setVisible(true);
    }

    public void criarComponentes() {
        criarPainel1();
        criarPainel2();
        add(jp1);
        add(jp2);
    }

    public void criarPainel1() {
        jp1 = new JPanel();
        jl_questao = new JLabel("Deseja iniciar a importação de dados legacy?");
        jp1.add(jl_questao);
    }

    public void criarPainel2() {
        jp2 = new JPanel();
        criarBotaoOK();
        criarBotaoCancelar();
        jp2.add(jb_OK);
        jp2.add(jb_Cancelar);

    }

    public void criarBotaoOK() {
        jb_OK = new JButton("OK");
        getRootPane().setDefaultButton(jb_OK);
        jb_OK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                JFileChooser fileEventosChooser = new JFileChooser();
                fileEventosChooser.setDialogTitle("Seleção de Eventos");
                int userEventosSelection = fileEventosChooser.showOpenDialog(MigrarDadosLegacyUI.this);
                if (userEventosSelection == JFileChooser.APPROVE_OPTION) {
                    File fileEventosToRead = fileEventosChooser.getSelectedFile();
                    if (fileEventosToRead.getName().endsWith("csv")) {
                        MigrarDadosLegacyController mdC = new MigrarDadosLegacyController(m_empresa);
                        try {
                            mdC.setFileEventos(fileEventosToRead);
                            JOptionPane.showMessageDialog(null, "Evento(s) importado(s) com sucesso!", "Sucesso!", JOptionPane.PLAIN_MESSAGE);
                        } catch (IOException ex) {
                            Logger.getLogger(MigrarDadosLegacyUI.class.getName()).log(Level.SEVERE, null, ex);
                        }

                        JFileChooser fileRevisoresChooser = new JFileChooser();
                        fileRevisoresChooser.setDialogTitle("Seleção de Revisores");
                        int userRevisoresSelection = fileRevisoresChooser.showOpenDialog(MigrarDadosLegacyUI.this);
                        if (userRevisoresSelection == JFileChooser.APPROVE_OPTION) {
                            File fileRevisoresToRead = fileRevisoresChooser.getSelectedFile();
                            if (fileRevisoresToRead.getName().endsWith("csv")) {
                                mdC.setFileRevisores(fileRevisoresToRead);
                                JOptionPane.showMessageDialog(null, "Revisor(es) importado(s) com sucesso!", "Sucesso!", JOptionPane.PLAIN_MESSAGE);
                                JFileChooser fileArtigosChooser = new JFileChooser();
                                fileArtigosChooser.setDialogTitle("Seleção de Artigos");
                                int userArtigosSelection = fileArtigosChooser.showOpenDialog(MigrarDadosLegacyUI.this);
                                if (userArtigosSelection == JFileChooser.APPROVE_OPTION) {
                                    File fileArtigosToRead = fileArtigosChooser.getSelectedFile();
                                    if (fileArtigosToRead.getName().endsWith("csv")) {
                                        mdC.setFileArtigos(fileArtigosToRead);
                                        JOptionPane.showMessageDialog(null, "Artigo(s) importado(s) com sucesso!", "Sucesso!", JOptionPane.PLAIN_MESSAGE);
                                        
                                        JFileChooser fileRevisoesChooser = new JFileChooser();
                                        fileRevisoesChooser.setDialogTitle("Seleção de Revisões");
                                        int userRevisoesSelection = fileRevisoesChooser.showOpenDialog(MigrarDadosLegacyUI.this);
                                        if (userRevisoesSelection == JFileChooser.APPROVE_OPTION) {
                                            File fileRevisoesToRead = fileRevisoesChooser.getSelectedFile();
                                            if (fileRevisoesToRead.getName().endsWith("csv")) {
                                                mdC.setFileRevisoes(fileRevisoesToRead);
                                                JOptionPane.showMessageDialog(null, "Revisões importadas com sucesso!", "Sucesso!", JOptionPane.PLAIN_MESSAGE);
                                            } else {
                                                JOptionPane.showMessageDialog(null, "O tipo de ficheiro selecionado não é válido", "ERRO!", JOptionPane.ERROR_MESSAGE);
                                            }
                                        }
                                    } else {
                                        JOptionPane.showMessageDialog(null, "O tipo de ficheiro selecionado não é válido", "ERRO!", JOptionPane.ERROR_MESSAGE);
                                    }
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "O tipo de ficheiro selecionado não é válido", "ERRO!", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "O tipo de ficheiro selecionado não é válido", "ERRO!", JOptionPane.ERROR_MESSAGE);
                    }

                }
            }

        });

    }

    public void criarBotaoCancelar() {
        jb_Cancelar = new JButton("Cancelar");
        jb_Cancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

    }

    private void fechar() {
        String[] optYesNo = {"Sim", "Não"};
        int ans = JOptionPane.showOptionDialog(this, "Pretende fechar a importação de eventos?", "Sair", 0, JOptionPane.QUESTION_MESSAGE, null, optYesNo, optYesNo[1]);
        final int YES = 0;

        if (ans == YES) {
            dispose();
        }
    }
}
