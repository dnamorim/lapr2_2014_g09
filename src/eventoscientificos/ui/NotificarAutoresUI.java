package eventoscientificos.ui;

import eventoscientificos.controller.NotificarAutoresController;
import eventoscientificos.controller.ReverArtigosController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Utilizador;
import java.awt.Frame;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author dnamorim
 */
public class NotificarAutoresUI {
    private Frame pai;
    private Empresa empresa; 
    private Utilizador user;
    private NotificarAutoresController nac;
    
    public NotificarAutoresUI(Frame pai, Empresa e, Utilizador u) {
        this.empresa = e;
        this.user = u;
        this.pai = pai;
        
        nac = new NotificarAutoresController(this.empresa, this.user);
        Object[] itemsEvento = nac.getListaEvento();
        
        if(itemsEvento.length == 0)  {
            JOptionPane.showMessageDialog(pai, "Não possui eventos para notificar.", "Notificar Autores", JOptionPane.WARNING_MESSAGE);
        } else {
            Object objEv = JOptionPane.showInputDialog(pai, "Seleccione o Evento:", "Notificar Autores", JOptionPane.PLAIN_MESSAGE, null, itemsEvento, itemsEvento[0]);
            if (objEv != null) {
                Evento ev = ((NotificarAutoresController.ItemEvento) objEv).getEvento();
                nac.setEvento(ev);
                try {
                    seleccionarFicheiro();
                    JOptionPane.showMessageDialog(pai, "Ficheiro de notificação criado com sucesso.", "Notificar Autores", JOptionPane.INFORMATION_MESSAGE);
                } catch (FileNotFoundException ex) {
                    JOptionPane.showMessageDialog(pai, "Não foi possível criar a lista de notificação.", "Notificar Autores", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    public void seleccionarFicheiro() throws FileNotFoundException {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Gravar lista de notificação");   
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Ficheiros XML (.xml)", "xml");
        fileChooser.setFileFilter(filter);
        
        int userSelection = fileChooser.showSaveDialog(pai);
 
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            String fileToSave = fileChooser.getSelectedFile().getAbsolutePath();
            
            if (!fileToSave.endsWith(".xml")) {
                fileToSave += ".xml";
            }
            nac.criarFicheiroNotificacao(fileToSave);
            
        }
    }
}


