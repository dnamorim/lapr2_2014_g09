/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import eventoscientificos.controller.ExportarPagamentosController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Utilizador;
import java.awt.Frame;
import java.io.FileNotFoundException;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author dnamorim
 */
public class ExportarPagamentosUI {
    private Frame pai;
    private Empresa empresa;
    private Utilizador user;
    private ExportarPagamentosController controllerEP;
    
    public ExportarPagamentosUI(Frame pai, Empresa emp, Utilizador u) {
        this.pai = pai;
        this.empresa = emp;
        this.user = u;
        
        controllerEP = new ExportarPagamentosController(this.empresa, this.user);
        if (seleccionarEvento()) {
            try {
                if(seleccionarFicheiro()) {
                    JOptionPane.showMessageDialog(pai, "Lista de pagamentos criada com sucesso.", "Exportar Pagamentos", JOptionPane.INFORMATION_MESSAGE);
                } else {
                   JOptionPane.showMessageDialog(pai, "Não foi possível exportar a lista de pagamentos.", "Exportar Pagamentos", JOptionPane.ERROR_MESSAGE); 
                }
            } catch (FileNotFoundException ex) {
                JOptionPane.showMessageDialog(pai, "Não foi possível exportar a lista de pagamentos.", "Exportar Pagamentos", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    
    public boolean seleccionarEvento() {
        Object[] itemsEvento = controllerEP.getEventosPodeExportar();
        
        if(itemsEvento == null || itemsEvento.length == 0 )  {
            JOptionPane.showMessageDialog(pai, "Não existem eventos disponíveis para exportar os pagamentos.", "Exportar Pagamentos", JOptionPane.WARNING_MESSAGE);
        } else {
            Object objEv = JOptionPane.showInputDialog(pai, "Seleccione o Evento:", "Criar Tópicos de Evento", JOptionPane.PLAIN_MESSAGE, null, itemsEvento, itemsEvento[0]);
            if (objEv != null) {
                Evento ite = ((Evento) objEv);
                controllerEP.setEvento(ite);
                return true;
            }
        }
        return false;
    }
    
    public boolean seleccionarFicheiro() throws FileNotFoundException {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Gravar lista pagamentos");   
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Ficheiros CSV (.csv)", "csv");
        fileChooser.setFileFilter(filter);
        
        int userSelection = fileChooser.showSaveDialog(pai);
 
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            String fileToSave = fileChooser.getSelectedFile().getAbsolutePath();
            
            if (!fileToSave.endsWith(".csv")) {
                fileToSave += ".csv";
            }
            return controllerEP.criarFicheiro(fileToSave);
            
        }
        return false;
    }
    
}
