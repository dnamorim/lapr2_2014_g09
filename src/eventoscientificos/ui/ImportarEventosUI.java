/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.controller.ImportarEventosController;
import eventoscientificos.model.*;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.*;

/**
 *
 * @author i130672
 */
public class ImportarEventosUI extends JFrame {

    JPanel jp1, jp2;
    JLabel jl_questao;
    JButton jb_OK, jb_Cancelar;
    Empresa m_empresa;
    private ImportarEventosController m_controllerIE;

    public ImportarEventosUI(Empresa m_empresa) {
        super("Importação");
        this.m_empresa = m_empresa;
        m_controllerIE = new ImportarEventosController(this.m_empresa);
        GridLayout g1 = new GridLayout(2, 1);
        setLayout(g1);
        criarComponentes();
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                fechar();
            }
        });
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        setLocation(600, 350);
        pack();
        setVisible(true);
    }

    public void criarComponentes() {
        criarPainel1();
        criarPainel2();
        add(jp1);
        add(jp2);
    }

    public void criarPainel1() {
        jp1 = new JPanel();
        jl_questao = new JLabel("Deseja iniciar a importação de evento(s)");
        jp1.add(jl_questao);
    }

    public void criarPainel2() {
        jp2 = new JPanel();
        criarBotaoOK();
        criarBotaoCancelar();
        jp2.add(jb_OK);
        jp2.add(jb_Cancelar);

    }

    public void criarBotaoOK() {
        jb_OK = new JButton("OK");
        getRootPane().setDefaultButton(jb_OK);
        jb_OK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();

                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setDialogTitle("Seleção de Ficheiros");
                int userSelection = fileChooser.showOpenDialog(ImportarEventosUI.this);
                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToRead = fileChooser.getSelectedFile();
                    if (fileToRead.getName().endsWith("csv") || fileToRead.getName().endsWith("xml")) {
                        m_controllerIE.setFile(fileToRead);
                        for (Evento evento : m_controllerIE.getListaEventos()) {
                            AlterarEventoUI aeUI = new AlterarEventoUI(m_empresa, evento);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "O tipo de ficheiro selecionado não é válido", "ERRO!", JOptionPane.PLAIN_MESSAGE);
                    }

                }

            }

        });

    }

    public void criarBotaoCancelar() {
        jb_Cancelar = new JButton("Cancelar");
        jb_Cancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

    }

    private void fechar() {
        String[] optYesNo = {"Sim", "Não"};
        int ans = JOptionPane.showOptionDialog(this, "Pretende fechar a importação de eventos?", "Sair", 0, JOptionPane.QUESTION_MESSAGE, null, optYesNo, optYesNo[1]);
        final int YES = 0;
        if (ans == YES) {
            dispose();
        }
    }
}
