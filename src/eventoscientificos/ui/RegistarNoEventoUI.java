/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import eventoscientificos.controller.RegistarNoEventoController;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import java.awt.Frame;
import java.util.List;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 *
 * @author dnamorim
 */
public class RegistarNoEventoUI {
    
    private Empresa empresa;
    private Utilizador user;
    private RegistarNoEventoController controllerRE;
    private Frame pai;
    
    public RegistarNoEventoUI(Empresa empresa, Utilizador user, Frame pai) {
        this.empresa = empresa;
        this.user = user;
        this.pai = pai;
        controllerRE = new RegistarNoEventoController(empresa, user);
        
        if(seleccionarEvento()) {
            DialogSeleccionarArtigos dialogSA = new DialogSeleccionarArtigos(pai, true, controllerRE);
            List<Submissao> showDialog = dialogSA.showDialog();
            if(!showDialog.isEmpty()) {
                DialogMetodoPagamento dialogMP = new DialogMetodoPagamento(pai, true, controllerRE);
                boolean showDialog1 = dialogMP.showDialog();
                if(showDialog1) {
                    if(controllerRE.efectuarPagamento()) {
                        if(controllerRE.setSubmissoesRegistada()) {
                           JOptionPane.showMessageDialog(pai, "Registo das Submissões no Evento efectuado com sucesso.", "Registo no Evento", JOptionPane.INFORMATION_MESSAGE); 
                        } else {
                            JOptionPane.showMessageDialog(pai, "Ocorreu um erro inesperado ao Registar as Submissões no Evento.\nContacte a Organização do Evento.", "Registo no Evento", JOptionPane.ERROR_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(pai, "Ocorreu um erro inesperado ao efectuar o pagamento.\nContacte a Organização do Evento.", "Registo no Evento", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } 
        }
    }
    
    public boolean seleccionarEvento() {
        RegistarNoEventoController.ItemEvento[] itemsEvento = controllerRE.getEventosPodeRegistar();
        
        if(itemsEvento == null || itemsEvento.length == 0 )  {
            JOptionPane.showMessageDialog(pai, "Não existem eventos a registar disponíveis para o Utilizador Actual.", "Registar No Evento", JOptionPane.WARNING_MESSAGE);
        } else {
            Object objEv = JOptionPane.showInputDialog(pai, "Seleccione o Evento:", "Criar Tópicos de Evento", JOptionPane.PLAIN_MESSAGE, null, itemsEvento, itemsEvento[0]);
            if (objEv != null) {
                Evento ite = ((RegistarNoEventoController.ItemEvento) objEv).getEvento();
                controllerRE.setEvento(ite);
                return true;
            }
        }
        return false;
    }
    
}
