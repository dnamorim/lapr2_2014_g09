/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import eventoscientificos.controller.RegistarNoEventoController;
import eventoscientificos.controller.RegistarNoEventoController.ItemMetodoPagamento;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.MetodoPagamento;
import eventoscientificos.model.Utilizador;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

/**
 *
 * @author dnamorim
 */
public class DialogMetodoPagamento extends javax.swing.JDialog {

    private RegistarNoEventoController controllerRE;
    boolean registadoMP = false;
    /**
     * Creates new form DialogMetodoPagamento
     */
    public DialogMetodoPagamento(java.awt.Frame parent, boolean modal, RegistarNoEventoController controllerRE) {
        super(parent, modal);
        initComponents();
        this.controllerRE = controllerRE;
        
        controllerRE.getMetodosPagamento();
        DefaultComboBoxModel model = new DefaultComboBoxModel(controllerRE.getMetodosPagamento());
        cbxMetodosPagamento.setModel(model);
        cbxMetodosPagamento.setSelectedIndex(0);
    }

    public boolean showDialog() {
        setVisible(true);
        dispose();
        return registadoMP;
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cbxMetodosPagamento = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        txtNumero = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        dtpDataValidade = new org.jdesktop.swingx.JXDatePicker();
        btnOK = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Método de Pagamento");
        setResizable(false);

        jLabel1.setText("Método de Pagamento");

        cbxMetodosPagamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxMetodosPagamentoActionPerformed(evt);
            }
        });

        jLabel2.setText("Informações Cartão de Crédito:");

        jLabel3.setText("Número");

        jLabel4.setText("Data de Validade");

        btnOK.setText("OK");
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnOK)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel2)
                        .addComponent(jLabel1)
                        .addComponent(cbxMetodosPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, 354, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel3)
                                .addComponent(jLabel4))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtNumero)
                                .addComponent(dtpDataValidade, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap(27, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbxMetodosPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dtpDataValidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnOK)
                .addContainerGap(10, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
        MetodoPagamento mp = ((ItemMetodoPagamento) cbxMetodosPagamento.getSelectedItem()).getMetodoPagamento();
        controllerRE.setMetodoPagamento(mp);
        
        boolean canPay = true;
        if(this.txtNumero.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Número de Cartão de Crédito Inválido.", "Método de Pagamento", JOptionPane.WARNING_MESSAGE);
            canPay=false;
        } else if(this.dtpDataValidade.getDate().before(controllerRE.getEvento().getDataLimiteRegisto().getTime()) || 
                this.dtpDataValidade.getDate().before(new Date())) {
            JOptionPane.showMessageDialog(this, "Data de Validade Inválida (ultrapassa a Data Limite de Autorização ou o cartão está caducado).", "Método de Pagamento", JOptionPane.WARNING_MESSAGE);
            canPay=false;
        }  
        
        if(canPay) {
            DateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
            String strMsg = String.format("Confirma os dados do Método de Pagamento?%n%13s: %s%n %13s:%s%n %13s:%.2f €", "Número Cartão", txtNumero.getText(), " Data Validade", dtf.format(dtpDataValidade.getDate()), "Valor", controllerRE.getValorAPagar());
            String[] opts = {"Sim", "Não"};
            int ans = JOptionPane.showOptionDialog(this, strMsg, "Método de Pagamento", 0, JOptionPane.QUESTION_MESSAGE, null, opts, opts[1]);
            if(ans == 0) {
                controllerRE.setDadosPagamento(txtNumero.getText(), dtpDataValidade.getDate());
                registadoMP = true;
                dispose();
            }     
        }
    }//GEN-LAST:event_btnOKActionPerformed

    private void cbxMetodosPagamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxMetodosPagamentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbxMetodosPagamentoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOK;
    private javax.swing.JComboBox cbxMetodosPagamento;
    private org.jdesktop.swingx.JXDatePicker dtpDataValidade;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField txtNumero;
    // End of variables declaration//GEN-END:variables
}
