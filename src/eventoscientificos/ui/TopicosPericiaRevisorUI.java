/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.ui;

import eventoscientificos.controller.TopicosPericiaRevisorController;
import eventoscientificos.controller.TopicosPericiaRevisorController.ItemEvento;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Topico;
import eventoscientificos.model.TopicosACM;
import eventoscientificos.model.Utilizador;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 *
 * @author dnamorim
 */
public class TopicosPericiaRevisorUI extends javax.swing.JFrame {

    private Empresa empresa;
    private Utilizador utilizador;
    private TopicosPericiaRevisorController controllerTPR;
    private DefaultListModel modellst;
    
    /**
     * Creates new form TopicosPericiaRevisorUI
     */
    public TopicosPericiaRevisorUI(Empresa e, Utilizador u) {
        initComponents();
        
        this.empresa = e;
        this.utilizador = u;
        
        modellst = new DefaultListModel();
        this.lstTopicos.setModel(modellst);
        
        controllerTPR = new TopicosPericiaRevisorController(empresa, utilizador);
        
        if(seleccionarEvento()) {
            loadTopicos();
            setVisible(true);
        }
        
    }
    
    private void loadTopicos() {
        this.modellst.clear();
        for (Topico t : this.controllerTPR.getTopicosRevisor()) {
            modellst.addElement(t);
        }
    }
    
    public boolean seleccionarEvento() {
        ItemEvento[] itemsEvento = controllerTPR.getListaEventosRevisor();
        
        if(itemsEvento == null || itemsEvento.length == 0 )  {
            JOptionPane.showMessageDialog(this, "Não existem eventos disponíveis para o Revisor.", "Tópicos de Perícia do Revisor", JOptionPane.WARNING_MESSAGE);
        } else {
            Object objEv = JOptionPane.showInputDialog(this, "Seleccione o Evento:", "Tópicos de Perícia do Revisor", JOptionPane.PLAIN_MESSAGE, null, itemsEvento, itemsEvento[0]);
            if (objEv != null) {
                ItemEvento ite = (ItemEvento) objEv;
                controllerTPR.setEvento(ite.getEvento());
                return true;
            }
        }
        return false;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstTopicos = new javax.swing.JList();
        btnAdicionar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnRegistar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tópicos de Perícia");

        jLabel1.setText("Tópicos de Perícia:");

        jScrollPane1.setViewportView(lstTopicos);

        btnAdicionar.setText("Adicionar");
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });

        btnEliminar.setText("Eliminar");
        btnEliminar.setToolTipText("Seleccione vários elementos da lista");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnRegistar.setText("Registar");
        btnRegistar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(btnAdicionar)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(btnEliminar)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnRegistar))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(27, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdicionar)
                    .addComponent(btnEliminar)
                    .addComponent(btnRegistar))
                .addContainerGap(11, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
        String codigoACM = JOptionPane.showInputDialog(this, "Insira o código do Tópico a Adicionar: (ex. B.1.2)", "Adicionar Tópico", JOptionPane.PLAIN_MESSAGE);   
        
        if(TopicosACM.getTopicoByCodigoACM(codigoACM) == null) {
            JOptionPane.showMessageDialog(this, "O código inserido não é válido.", "Adicionar Tópico", JOptionPane.WARNING_MESSAGE);
        } else {
            if(!this.controllerTPR.adicionarTopico(codigoACM)) {
                JOptionPane.showMessageDialog(this, "Esse tópico já foi previamente adicionado.", "Adicionar Tópico", JOptionPane.WARNING_MESSAGE);
            } 
        }
        
        loadTopicos();
    }//GEN-LAST:event_btnAdicionarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        this.controllerTPR.eliminarTopicos(this.lstTopicos.getSelectedValuesList());
        loadTopicos();
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnRegistarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistarActionPerformed
        controllerTPR.registarTopicos();
        JOptionPane.showMessageDialog(this, "Tópicos de Perícia do Revisor actualizados com sucesso.", "Tópicos de Perícia do Revisor", JOptionPane.INFORMATION_MESSAGE);
        dispose();
    }//GEN-LAST:event_btnRegistarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnRegistar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList lstTopicos;
    // End of variables declaration//GEN-END:variables
}
