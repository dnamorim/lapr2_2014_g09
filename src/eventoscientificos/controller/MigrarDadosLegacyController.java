/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.CP;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.FicheiroLegacyArtigos;
import eventoscientificos.model.FicheiroLegacyEventos;
import eventoscientificos.model.FicheiroLegacyRevisoes;
import eventoscientificos.model.Submissao;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Tiago
 */
public class MigrarDadosLegacyController {

    private Empresa m_empresa;
    private File ficheiro_evento, ficheiros_artigos, ficheiro_revisoes, ficheiro_revisores;
    private File ficheiroEvento;
    private HashMap<Evento, String> hg_legacy;
    private HashMap<Submissao, String> hg_artigos;
    private FicheiroLegacyEventos fle;
    private FicheiroLegacyArtigos fla;
    private FicheiroLegacyRevisoes flr;

    public MigrarDadosLegacyController(Empresa m_empresa) {
        this.hg_legacy = new HashMap<>();
        this.m_empresa = m_empresa;
    }

    public File getFileEventos() {
        return this.ficheiro_evento;
    }

    public File getFileArtigos() {
        return this.ficheiros_artigos;
    }

    public File getFileRevisoes() {
        return this.ficheiro_revisoes;
    }

    public File getFileRevisores() {
        return this.ficheiro_revisores;
    }

    public void setFileEventos(File ficheiro) throws IOException {
        this.ficheiro_evento = ficheiro;
        fle = new FicheiroLegacyEventos();
        fle.setFileEventos(ficheiro);
        fle.setHg_legacy(hg_legacy);
        fle.criarListaEventos(fle.lerFile());
        hg_legacy = fle.getHg_legacy();
        registarEventos();

    }

    public void setFileArtigos(File ficheiro) {
        this.ficheiros_artigos = ficheiro;
        fla = new FicheiroLegacyArtigos();
        fla.setFileEventos(ficheiros_artigos);
        fla.setHg_legacy(hg_legacy);
        fla.criarListaArtigos(fla.lerFile());
        hg_artigos = fla.getHg_submissao();
    }

    public void setFileRevisoes(File ficheiro) {
        this.ficheiro_revisoes = ficheiro;
        flr = new FicheiroLegacyRevisoes();
        flr.setFicheiro_revisores(ficheiro_revisores);
        flr.setHg_legacy(hg_legacy);
        flr.setHg_submissao(hg_artigos);
        flr.criarListaRevisoes(flr.lerFile());
    }

    public void setFileRevisores(File ficheiro) {
        this.ficheiro_revisores = ficheiro;
        fle.setFileRevisores(ficheiro, m_empresa);
    }

    /**
     * @return the hg_legacy
     */
    public HashMap<Evento, String> getHg_legacy() {
        return hg_legacy;
    }

    /**
     * @param hg_legacy the hg_legacy to set
     */
    public void setHg_legacy(HashMap<Evento, String> hg_legacy) {
        this.hg_legacy = hg_legacy;
    }

    private void registarEventos() {
        Iterator<Evento> keySetIterator = hg_legacy.keySet().iterator();
        while (keySetIterator.hasNext()) {
            Evento key = keySetIterator.next();
            m_empresa.getRegistoEventos().registaEvento(key);
            CP cp = key.novaCP();
            key.getState().setRegistado();
            key.getState().setTerminado();
        }
    }
}
