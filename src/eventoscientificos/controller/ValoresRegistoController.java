/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.EventoState.EventoRegistadoState;
import eventoscientificos.model.FormulaPagamento;
import eventoscientificos.model.FormulaPagamento1;
import eventoscientificos.model.FormulaPagamento2;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dnamorim
 */
public class ValoresRegistoController {
    
    private Empresa empresa;
    private Evento evento;
    private FormulaPagamento formulapag;
    
    public ValoresRegistoController(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public void setEvento(Evento e) {
        this.evento = e;
    }
    
    public boolean setPrecoFull(String preco){
        if(!preco.isEmpty()) {   
            try {
              evento.setPrecoFull(Float.parseFloat(preco));
              return true;
            } catch(NumberFormatException ex) {
                return false;
            } catch(IllegalArgumentException ex) {
                return false;
            }
        } else {
            return false;
        } 
    }    
    
    public boolean setPrecoShort(String preco) {
        if(!preco.isEmpty()) {
            try {
              evento.setPrecoShort(Float.parseFloat(preco));
              return true;
            } catch(NumberFormatException ex) {
                return false;
            } catch(IllegalArgumentException ex) {
                return false;
            } 
        } else {
            return false;
        }
    }
    
    public boolean setPrecoPoster(String preco) {
        if(!preco.isEmpty()) {
            try {
              evento.setPrecoPoster(Float.parseFloat(preco));
              return true;
            } catch(NumberFormatException ex) {
                return false;
            } catch(IllegalArgumentException ex) {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public Object[] getEventosOrganizador(Utilizador u) {
        List<Evento> list = empresa.getRegistoEventos().getEventosOrganizador(u);
        return obterArrayItemsEvento(list);
    }
    
    public Object[] obterArrayItemsEvento(List<Evento> dados) {
        List<Evento> items = new ArrayList<Evento>();

        for (int i = 0; i < dados.size(); i++) {
            if(dados.get(i).getState() instanceof EventoRegistadoState) {
                items.add(dados.get(i));
            }
        }
        return items.toArray();
    }

    public boolean setValoresRegistoEvento() {
        return evento.setValoresRegistoDefinidos();
    }
    
    public void setFormulaPagamento(ItemFormula item) {
        FormulaPagamento f = item.getFormulaPagamento();
        this.evento.setFormulaPagamento(f);
    }
    
    public List<ItemFormula> obterItemsFormula(List<Class<? extends FormulaPagamento>> dados) {
        List<ItemFormula> items = new ArrayList<ItemFormula>();
        for (Class<? extends FormulaPagamento> cls : dados) {
            items.add(new ItemFormula(cls));
        }
        return items;
    }
    
    public List<ItemFormula> getFormulasPagamento() {
        List<Class<? extends FormulaPagamento>> formulaPagamento = empresa.getFormulasPagamento();
        return obterItemsFormula(formulaPagamento);
    }
    
    
    public class ItemEvento {

        private Evento evento;

        public ItemEvento(Evento e) {
            this.evento = e;
        }

        public Evento getEvento() {
            return evento;
        }

        public void setEvento(Evento e) {
            this.evento = e;
        }

        @Override
        public String toString() {
            return String.format("%s", evento.getTitulo());
        }
    }
    
    public class ItemFormula {

        private FormulaPagamento formula;
        private String nome;

        public ItemFormula(Class<? extends FormulaPagamento> cls) {
            if(cls == FormulaPagamento1.class) {
                this.formula= new FormulaPagamento1(evento);
                this.nome = "Fórmula 1";
            } else if(cls == FormulaPagamento2.class) {
                this.formula = new FormulaPagamento2(evento);
                this.nome = "Fórmula 2";
            }
        }

        public FormulaPagamento getFormulaPagamento() {
            return formula;
        }

        public void setFormulaPagamento(FormulaPagamento formula) {
            this.formula = formula;
        }
        
        public String getDescricao() {
            return formula.descricao();
        }

        @Override
        public String toString() {
            return String.format("%s", this.nome);
        }
    }
}
