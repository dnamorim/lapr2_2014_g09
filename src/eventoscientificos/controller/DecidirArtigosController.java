/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Decisao;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.EventoState.EventoRevistoState;
import eventoscientificos.model.MecanismoDecisao;
import eventoscientificos.model.MecanismoPesoMaior;
import eventoscientificos.model.ProcessoDecisao;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author dnamorim
 */
public class DecidirArtigosController {
    
    private Empresa empresa;
    private Evento evento;
    private ProcessoDecisao pd;
    private MecanismoDecisao mecanismo;
    
    public DecidirArtigosController(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public Object[] getEventosPodeDecidir(Utilizador u) {
        List<Evento> lstEventos = this.empresa.getRegistoEventos().getEventosOrganizador(u);
        return obterArrayItemEventos(lstEventos);
    }
    
    private Object[] obterArrayItemEventos(List<Evento> lst) {
        List<Evento> ite = new ArrayList<Evento>();
        
        for (int i = 0; i < lst.size(); i++) {
            if(lst.get(i).getState() instanceof EventoRevistoState) {
                ite.add(lst.get(i));
            }
        }
        return ite.toArray();
    } 
    
    public ItemMecanismo[] getMecanismosDecisao() {
        List<Class<? extends MecanismoDecisao>> lstMec = empresa.getMecanismosDecisao();
        return obterArrayItemMecanismos(lstMec);
    }
    
    private ItemMecanismo[] obterArrayItemMecanismos(List<Class<? extends MecanismoDecisao>> lst) {
        ItemMecanismo[] ite = new ItemMecanismo[lst.size()];
        
        for (int i = 0; i < ite.length; i++) {
            ite[i] = new ItemMecanismo(lst.get(i));
        }
        return ite;
    } 
    
    public void criarProcessoDistribuicao() {
        this.pd = new ProcessoDecisao();
        this.pd.setMecanismoDistribuicao(mecanismo);
        this.pd.setEvento(evento);
        this.pd.decidir();
    }
    
    public String resultadoDistribuicao() {
        String resDist = "";
        for (Decisao dist : this.pd.getProcessoDecisao()) {
            resDist += String.format("Artigo: %s (%s)%n%n", dist.getArtigo().getTitulo(), dist.getDescricao());
        }
        return resDist;
    }
    
    public boolean registarDecisoes() {
        boolean flagErro = false;
        for (Submissao s : this.evento.getListaSubmissoes()) {
            Artigo a = s.getArtigo();
            for (ListIterator<Decisao> itd = this.pd.getProcessoDecisao().listIterator(); itd.hasNext();) {
                Decisao d = itd.next();
                if(d.getArtigo().equals(a)) {
                    a.setDecisao(d);
                    break;
                }
            }
            if(!s.setDecidida()) {
                flagErro = true;
            }
            
        }
        if(!flagErro) {
            return this.evento.setDecidido();
        } else {
            return false;
        }
    }
    
    
    public void setEvento(Evento e) {
        this.evento = e;
    }
    
    public void setMecanismo(MecanismoDecisao m) {
        this.mecanismo = m;
    }
    
    public class ItemEvento {

        private Evento evento;

        public ItemEvento(Evento e) {
            this.evento = e;
        }

        public Evento getEvento() {
            return evento;
        }

        public void setEvento(Evento e) {
            this.evento = e;
        }

        @Override
        public String toString() {
            return String.format("%s", evento.getTitulo());
        }
    }
    
    public class ItemMecanismo {

        private MecanismoDecisao mecanismo;
        

        public ItemMecanismo(Class<? extends MecanismoDecisao> cls) {
            if(cls == MecanismoPesoMaior.class) {
                this.mecanismo= new MecanismoPesoMaior();
            }
        }

        public MecanismoDecisao getMecanismo() {
            return mecanismo;
        }

        public void setMecanismoDecisao(MecanismoDecisao mec) {
            this.mecanismo = mec;
        }
        
        public String getDescricao() {
            return mecanismo.descricaoMecanismo();
        }

        @Override
        public String toString() {
            return String.format("%s", this.mecanismo.nameMecanismo());
        }
    }
}
