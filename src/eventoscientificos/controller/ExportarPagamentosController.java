/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.EventoState.EventoRegistadosNoEventoState;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author dnamorim
 */
public class ExportarPagamentosController {
    
    private Empresa emp;
    private Utilizador user;
    private Evento evento;
    
    public ExportarPagamentosController(Empresa e, Utilizador u) {
        this.emp = e;
        this.user = u;
    }

    public Object[] getEventosPodeExportar() {
        List<Evento> lst = this.emp.getRegistoEventos().getEventosOrganizador(user);
        return obterArrayEventos(lst);
    }
    
    private Object[] obterArrayEventos(List<Evento> lst) {
        List<Evento> lstExp = new ArrayList<Evento>();
        for (Evento ev : lst) {
            if(ev.getState() instanceof EventoRegistadosNoEventoState) {
                lstExp.add(ev);
            }
            
        }
        return lstExp.toArray();
    }

    public void setEvento(Evento ite) {
        this.evento = ite;
    }

    public boolean criarFicheiro(String fileToSave) {
        try {
	    FileWriter writer = new FileWriter(fileToSave);
 
	    writer.append("Artigo");
	    writer.append(',');
            writer.append("Autor");
	    writer.append(',');
            writer.append("Data Pagamento");
	    writer.append('\n');
 
            for (Submissao s : this.evento.getListaSubmissoes()) {
                if(s.isPaga()) {
                    writer.append(String.format("%s (%s)", s.getArtigo().getTitulo(), s.getArtigo().getTipo()));
                    writer.append(',');
                    writer.append(String.format("%s (%s)", s.getPagamento().getAutorPagamento().getNome(), s.getPagamento().getAutorPagamento().getEMail()));
                    writer.append(',');
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = s.getPagamento().getDataPagamento();
                    writer.append(dateFormat.format(date));
                    writer.append('\n');
                }
            }
 
	    writer.flush();
	    writer.close();
            return true;
	}
	catch(IOException e)
	{
	     return false;
	} 
        
        
    
    }
    
}
