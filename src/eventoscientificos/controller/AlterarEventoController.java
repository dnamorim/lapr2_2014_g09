/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Utilizador;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Tiago
 */
public class AlterarEventoController {

    private Evento evento;
    private Empresa empresa;

    public AlterarEventoController(Empresa empresa, Evento evento) {
        this.empresa = empresa;
        this.evento = evento;
    }

    /**
     * @return the evento
     */
    public Evento getEvento() {
        return evento;
    }

    /**
     * @param evento the evento to set
     */
    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public boolean addOrganizador(String strId) {
        Utilizador u = empresa.getRegistoUtilizadores().getUtilizador(strId);

        if (u != null) {
            return evento.addOrganizador(u);
        } else {
            return false;
        }
    }

    public boolean setDataInicio(Date dataInicio) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dataInicio);
        evento.setDataInicio(cal);
        return true;

    }

    public boolean setDataFim(Date dataFim) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(dataFim);
        evento.setDataFim(cal);
        return true;

    }

    public boolean setDataLimiteSubmissao(Date dataLimiteSubmissao) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(dataLimiteSubmissao);
        evento.setDataLimiteSubmissao(cal);
        return true;

    }

    public boolean setDataLimiteSubmissaoFinal(Date dataLimiteSubmissaoFinal) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(dataLimiteSubmissaoFinal);
        evento.setDataLimiteSubmissaoFinal(cal);
        return true;

    }

    public boolean setDataLimiteRevisao(Date dataLimiteRevisao) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(dataLimiteRevisao);
        evento.setDataLimiteRevisao(cal);
        return true;

    }

    public boolean setDataLimiteRegisto(Date dataLimiteRegisto) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(dataLimiteRegisto);
        evento.setDataLimiteRegisto(cal);
        return true;

    }
}
