package eventoscientificos.controller;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.EventoState.EventoDecididoState;
import eventoscientificos.model.Organizador;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Objects;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 */
public class NotificarAutoresController {

    private Empresa m_empresa;
    private Revisao m_revisao;
    private Evento m_evento;
    private Utilizador user;
    

    /**
     * Construtor da classe que recebe como parâmetro a empresa e que chama o
     * método assignOrganizador() para através dos dados devolver o organizador
     * com esses mesmos dados.
     *
     * @param empresa empresa responsável pelo evento
     */
    public NotificarAutoresController(Empresa empresa, Utilizador user) {
        m_empresa = empresa;
        this.user = user;
    }

  

    /**
     * Devolve a lista de eventos com o auxilio do método
     * obterArrayItemsEvento().
     *
     * @return lista de eventos
     */
    public Object[] getListaEvento() {;
        List<Evento> list = m_empresa.getRegistoEventos().getEventosOrganizador(this.user);
        return obterArrayItemsEvento(list);
    }

    /**
     * Modifica o evento.
     *
     * @param e evento
     */
    public void setEvento(Evento e) {
        this.m_evento = e;
    }

    /**
     * Devolve o evento.
     *
     * @return evento
     */
    public Evento getEvento() {
        return m_evento;
    }

    /**
     * Devolve a lista de submissões.
     *
     * @return lista submissões
     */
    private List<Submissao> getListaSubmissao() {
        return m_evento.getListaSubmissoes();
    }

    /**
     * Devolve um array com a lista de eventos.
     *
     * @param dados lista de eventos
     * @return lista de eventos sob a forma de array
     */
    public Object[] obterArrayItemsEvento(List<Evento> dados) {
       List<ItemEvento> items = new ArrayList<ItemEvento>();
       
        for (Evento dado : dados) {
            if(dado.getState() instanceof EventoDecididoState) {
                items.add(new ItemEvento(dado));
            }
        }

        return items.toArray();
    }

    /**
     * Responsável pela criação do ficheiro .xml com o auxilio do método
     * criarNotificacaoArtigo(), para notificar o autor da decisão.
     *
     * @throws FileNotFoundException
     */
    public void criarFicheiroNotificacao(String strFile) throws FileNotFoundException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("notification_list");
            doc.appendChild(rootElement);

            List<Submissao> listSubmissoes = this.getListaSubmissao();

            for (Submissao s : listSubmissoes) {
                Element submissao = doc.createElement("submission");
                rootElement.appendChild(submissao);
                criarNotificacaoArtigo(s, submissao, doc);
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(strFile));

            transformer.transform(source, result);
            
            this.m_evento.setNotificado();
            
            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);
            transformer.transform(source, result);
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    /**
     * Continuação da criação do ficheiro .xml percorrendo os artigos e
     * inserindo as decisões no ficheiro.
     *
     * @param s submissão
     */
    private void criarNotificacaoArtigo(Submissao s, Element submissao, Document doc) {
        Artigo m_artigo = s.getArtigo();

        Element paperTitle = doc.createElement("paper_title");
        paperTitle.appendChild(doc.createTextNode(m_artigo.getTitulo()));
        submissao.appendChild(paperTitle);

        Element paperType = doc.createElement("paper_type");
        paperType.appendChild(doc.createTextNode(m_artigo.getTipo().toString()));
        submissao.appendChild(paperType);

        Element authorName = doc.createElement("author_name");
        authorName.appendChild(doc.createTextNode(m_artigo.getAutorCorrespondente().getNome()));
        submissao.appendChild(authorName);

        Element authorEmail = doc.createElement("author_email");
        authorEmail.appendChild(doc.createTextNode(m_artigo.getAutorCorrespondente().getEMail()));
        submissao.appendChild(authorEmail);

        Element decision = doc.createElement("decision");
        decision.appendChild(doc.createTextNode(m_artigo.getDecisao().getDescricao()));
        submissao.appendChild(decision);

        Element comment = doc.createElement("comment");
        submissao.appendChild(comment);

        for (int i = 0; i < m_artigo.getRevisoes().size(); i++) {
            m_revisao = m_artigo.getRevisoes().get(i);
            Element reviewNr = doc.createElement("review_number");
            reviewNr.appendChild(doc.createTextNode(String.format("%d", i+1)));
            comment.appendChild(reviewNr);
            
            Element reviewComment = doc.createElement("reviewer_comments");
            reviewComment.appendChild(doc.createTextNode(m_revisao.getJustificacao()));
            comment.appendChild(reviewComment);

        }
    }

    /**
     * Classe "anónima" aproveitada de outra classe criada por Duarte Nuno
     * Amorim para devolver, modificar e apresentar informação relativa ao
     * evento.
     */
    public class ItemEvento {

        private Evento evento;

        /**
         * Construtor desta classe.
         *
         * @param e evento
         */
        public ItemEvento(Evento e) {
            this.evento = e;
        }

        /**
         * Devolve o evento.
         *
         * @return evento
         */
        public Evento getEvento() {
            return evento;
        }

        /**
         * Modifica o evento.
         *
         * @param e evento
         */
        public void setEvento(Evento e) {
            this.evento = e;
        }

        /**
         * Método toString() com a informação relativa ao evento.
         *
         * @return informação do evento sob a forma de String
         */
        public String toString() {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-DD");
            return String.format("%s", evento.getTitulo());
        }

        /**
         * Método equals para comparar um objecto fornecido como parâmetro com o
         * objecto actual.
         *
         * @param obj objecto a comparar
         * @return resultado da comparação
         */
        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final ItemEvento other = (ItemEvento) obj;
            if (!Objects.equals(this.evento, other.evento)) {
                return false;
            }
            return true;
        }

    }
}
