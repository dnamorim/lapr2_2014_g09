/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;


import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.EventoState.EventoRegistadoState;
import eventoscientificos.model.EventoState.EventoValoresRegistoDefinidoState;
import eventoscientificos.model.Topico;
import eventoscientificos.model.TopicosACM;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author iazevedo
 */

public class CriarTopicosEventoController {
    
    private Empresa m_empresa;
    private Evento m_evento;
    private int maxTopicos;
    

    public CriarTopicosEventoController(Empresa empresa) {
        m_empresa = empresa;
    }
    
    public Object[] getEventosOrganizador(Utilizador u) {
        List<Evento> list = m_empresa.getRegistoEventos().getEventosOrganizador(u);
        return obterArrayItemsEvento(list);
    }
    
    public void setEvento(Evento e) {
         m_evento = e;
     }
    
    
    public Topico addTopico(String strCodigo) {
        return TopicosACM.getTopicoByCodigoACM(strCodigo);
    }
   
   public boolean registaTopico(Topico t)
   {
       
       if(!m_evento.getTopicos().contains(t)) {
           return m_evento.addTopico(t);
       }

       return false;
   }
   
    private Object[] obterArrayItemsEvento(List<Evento> dados) {
        List<Evento> items = new ArrayList<Evento>();

        for (int i = 0; i < dados.size(); i++) {
            if(dados.get(i).getState() instanceof EventoValoresRegistoDefinidoState) {
                items.add(dados.get(i));
            }
        }
        return items.toArray();
    }

    public boolean registarTopicosEvento() {
        return m_evento.setTopicosCriados();
    }
   
   public class ItemEvento {

        private Evento evento;

        public ItemEvento(Evento e) {
            this.evento = e;
        }

        public Evento getEvento() {
            return evento;
        }

        public void setEvento(Evento e) {
            this.evento = e;
        }

        @Override
        public String toString() {
            return String.format("%s", evento.getTitulo());
        }
    }

    
}

