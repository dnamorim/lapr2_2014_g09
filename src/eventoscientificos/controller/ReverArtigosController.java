/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 *
 * @author andrémiguel
 */
public class ReverArtigosController {

    private Empresa m_empresa;
    private Utilizador ut;
    private Evento m_evento;
    private Artigo m_artigo;
    private Revisao revisao;
    private Revisor m_revisor;
    private Submissao s;
    
    
    public ReverArtigosController(Empresa e, Utilizador u) {
        this.m_empresa = e;
        this.ut = u;
    }

    public ItemEvento[] getListaEvento() {
        List<Evento> list = m_empresa.getRegistoEventos().getEventosRevisor(ut);
        return obterArrayItemsEvento(list);
    }
    
    public boolean isRevisto() {
        boolean flag = false;
        for (Revisao r : this.m_artigo.getRevisoes()) {
            if(r.getM_revisor().equals(this.m_revisor)) {
                flag = true;
            }
        }
        return flag;
    }

    private void assignRevisor() {
        List<Revisor> listRev = this.m_evento.getCP().getListaRevisores();
        
        for (Revisor r : listRev) {
            if(r.getUtilizador().equals(ut)) {
                m_revisor = r;
            }
        }
    }
    
    public void iniciarRevisao() {
        this.revisao = this.m_artigo.novaRevisao(this.m_revisor);
    }
    
    public ItemArtigo[] getListaSubmissao() {
        List<Submissao> list = this.m_evento.getListaSubmissoesARever(ut);
        return obterArrayItemsArtigo(list);
    }
    
    public void setEvento(Evento e) {
        this.m_evento = e;
        assignRevisor();
    }
    
    
    public void setArtigo(ItemArtigo a) {
        this.m_artigo = a.getArtigo();
        this.s = a.getSubmissao();
    }
    
    public Artigo getArtigo() {
        return this.m_artigo;
    }
    
    public Revisor getRevisor() {
       return m_revisor;
    }
    
    public Revisao getRevisao() {
        return this.revisao;
    }

    public void setConfianca(int conf) {
        this.revisao.setConfianca(conf);
    }

    public void setAdequacao(int adeq) {
        this.revisao.setAdequacao(adeq);
    }
    public void setQualidade(int qual) {
        this.revisao.setQualidade(qual);
    }

    public void setOriginalidade(int orig) {
        this.revisao.setOriginalidade(orig);
    }

    public void setRecomendacao(boolean rec) {
        this.revisao.setRecomendacao(rec);
    }

    public void setJustificacao(String just) {
        this.revisao.setJustificacao(just);
    }
    
    public void submeterRevisao() {
        this.m_artigo.addRevisao(revisao);
        this.s.setRevisto();
    }
    
    
    private ItemEvento[] obterArrayItemsEvento(List<Evento> dados) {
        ItemEvento[] items = new ItemEvento[dados.size()];

        for (int i = 0; i < items.length; i++) {
            items[i] = new ItemEvento(dados.get(i));
        }
        return items;
    }
    
    private ItemArtigo[] obterArrayItemsArtigo(List<Submissao> dados) {
        ItemArtigo[] items = new ItemArtigo[dados.size()];

        for (int i = 0; i < items.length; i++) {
            items[i] = new ItemArtigo(dados.get(i));
        }

        return items;
    }

    int getConfianca() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public class ItemEvento {

        private Evento evento;

        public ItemEvento(Evento e) {
            this.evento = e;
        }

        public Evento getEvento() {
            return evento;
        }

        public void setEvento(Evento e) {
            this.evento = e;
        }

        @Override
        public String toString() {
            return String.format("%s", evento.getTitulo());
        }

    }
    
    public class ItemArtigo {
        private Submissao s;
        private Artigo artigo;

        public ItemArtigo(Submissao s) {
            this.artigo = s.getArtigo();
            this.s = s;
        }

        public Artigo getArtigo() {
            return artigo;
        }
        
        public Submissao getSubmissao() {
            return s;
        }

        public void setArtigo(Artigo a) {
            this.artigo = a;
        }

        @Override
        public String toString() {
            return String.format("%s (%s)", artigo.getTitulo(), artigo.getTipo().toString());
        }
    }
}
