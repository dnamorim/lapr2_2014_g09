/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.EventoState.EventoCPDefinidaState;
import eventoscientificos.model.EventoState.EventoCameraReadyState;
import eventoscientificos.model.EventoState.EventoDistribuidoState;
import eventoscientificos.model.EventoState.EventoNotificadoState;
import eventoscientificos.model.EventoState.EventoRegistadosNoEventoState;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dnamorim
 */
public class AccoesStatesController {
    private Empresa empresa;
    
    public AccoesStatesController(Empresa empresa) {
        this.empresa = empresa;
    }
    
    //CPDefinida -> ProntoADistribuir State
    public Object[] getEventosOrganizadorDistribuir(Utilizador u) {
        List<Evento> list = empresa.getRegistoEventos().getEventosOrganizador(u);
        return obterArrayItemsEventoDistribuir(list);
    }
    
    private Object[] obterArrayItemsEventoDistribuir(List<Evento> dados) {
        List<Evento> items = new ArrayList<Evento>();

        for (int i = 0; i < dados.size(); i++) {
            if(dados.get(i).getState() instanceof EventoCPDefinidaState) {
                items.add(dados.get(i));
            } 
        }
        return items.toArray();
    }
    
    public boolean setProntoADistribuir(Evento e) {
        return e.setProntoADistribuir();
    }
    
    //Distribuido -> Revisto State
    public Object[] getEventosOrganizadorRevisto(Utilizador u) {
        List<Evento> list = empresa.getRegistoEventos().getEventosOrganizador(u);
        return obterArrayItemsEventoRevisto(list);
    }
    
    private Object[] obterArrayItemsEventoRevisto(List<Evento> dados) {
        List<Evento> items = new ArrayList<Evento>();

        for (int i = 0; i < dados.size(); i++) {
            if(dados.get(i).getState() instanceof EventoDistribuidoState) {
                items.add(dados.get(i));
            } 
        }
        return items.toArray();
    }
    
    public boolean setRevisto(Evento e) {
        return e.setRevisto();
    }
    
    //Notificado -> CameraReady State
    public Object[] getEventosOrganizadorCameraReady(Utilizador u) {
        List<Evento> list = empresa.getRegistoEventos().getEventosOrganizador(u);
        return obterArrayItemsEventoCameraReady(list);
    }
    
    private Object[] obterArrayItemsEventoCameraReady(List<Evento> dados) {
        List<Evento> items = new ArrayList<Evento>();

        for (int i = 0; i < dados.size(); i++) {
            if(dados.get(i).getState() instanceof EventoNotificadoState) {
                items.add(dados.get(i));
            }
        }
        return items.toArray();
    }
    
    public boolean setCameraReady(Evento e) {
        return e.setCameraReady();
    }
    
    
    //CameraReady -> RegistadosNoEventoState
    public Object[] getEventosOrganizadorRegEvento(Utilizador u) {
        List<Evento> list = empresa.getRegistoEventos().getEventosOrganizador(u);
        return obterArrayItemsEventoRegEvento(list);
    }
    
    private Object[] obterArrayItemsEventoRegEvento(List<Evento> dados) {
        List<Evento> items = new ArrayList<Evento>();

        for (int i = 0; i < dados.size(); i++) {
            if(dados.get(i).getState() instanceof EventoCameraReadyState) {
                items.add(dados.get(i));
            } 
        }
        return items.toArray();
    }
    
    public boolean setRegistadosNoEvento(Evento e) {
        return e.setRegistadosNoEvento();
    }
    
    //RegistadosNoEvento -> Terminado State
    public Object[] getEventosOrganizadorTerminado(Utilizador u) {
        List<Evento> list = empresa.getRegistoEventos().getEventosOrganizador(u);
        return obterArrayItemsEventoTerminado(list);
    }
    
    private Object[] obterArrayItemsEventoTerminado(List<Evento> dados) {
        List<Evento> items = new ArrayList<Evento>();

        for (int i = 0; i < dados.size(); i++) {
            if(dados.get(i).getState() instanceof EventoRegistadosNoEventoState) {
                items.add(dados.get(i));
            }
        }
        return items.toArray();
    }
    
    public boolean setTerminado(Evento e) {
        return e.setTerminado();
    }
    
    public class ItemEvento {

        private Evento evento;

        public ItemEvento(Evento e) {
            this.evento = e;
        }

        public Evento getEvento() {
            return evento;
        }

        public void setEvento(Evento e) {
            this.evento = e;
        }

        @Override
        public String toString() {
            return String.format("%s", evento.getTitulo());
        }
    }
}
