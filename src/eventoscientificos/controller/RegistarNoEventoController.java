/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;


import eventoscientificos.model.Autor;
import eventoscientificos.model.CanadaExpressPagamento;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.MetodoPagamento;
import eventoscientificos.model.Pagamento;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import eventoscientificos.model.VisaoLightPagamento;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import pt.ipp.isep.dei.eapli.canadaexpress.CanadaExpress;

/**
 *
 * @author dnamorim
 */
public class RegistarNoEventoController {
    private Empresa empresa;
    private Utilizador user;
    private Autor autor;
    private Evento evento;
    private MetodoPagamento mPagamento;
    private List<Submissao> lstSubAPagar;
    
    
    public RegistarNoEventoController(Empresa empresa, Utilizador user) {
        this.empresa = empresa;
        this.user = user;
    }
    
    public ItemEvento[] getEventosPodeRegistar() {
        List<Evento> lst = this.empresa.getRegistoEventos().getListaEventosPodeRegistar(user);
        return this.obterArrayItemsEvento(lst);
    }
    
    public ItemSubmissao[] getArtigosPodeRegistar() {
        List<Submissao> lst = this.evento.getListaSubmissoesPodeRegistar(user);
        return this.obterArrayItemsSubissao(lst);
    }
    
    public ItemMetodoPagamento[] getMetodosPagamento() {
        List<Class<? extends MetodoPagamento>> lst = this.empresa.getMetodosPagamento();
        return this.obterArrayItemsMetodosPagamento(lst);
    }
    
    public void setMetodoPagamento(MetodoPagamento mp) {
        this.mPagamento = mp;
    }
    
    public void setDadosPagamento(String nrCartao, Date dataValidade) {
        mPagamento.setInfoPagamento(nrCartao, dataValidade, evento.getDataLimiteRegisto().getTime());
    }
    
    public Evento getEvento() {
        return this.evento;
    }
    
    public void setEvento(Evento ite) {
        evento = ite;
    }
    
    public void setListaSubmissoesAPagar(List<Submissao> lstSubs) {
        this.lstSubAPagar = lstSubs;
    }
    
    public float getValorAPagar() {
        return this.evento.getFormulaPagamento().valorAPagar(lstSubAPagar);
    }
    
    public boolean efectuarPagamento() {
        return mPagamento.pagamento(this.getValorAPagar());
    }
    
    private boolean setPagamentoEfectuado() {
        for (Iterator<Submissao> it = lstSubAPagar.listIterator(); it.hasNext();) {
                Submissao s = it.next();
                Pagamento p = s.novoPagamento();
                p.setDataPagamento(mPagamento.getDataPagamento());
                p.setAutor(s.getArtigo().getAutor(user));
                s.addPagamento(p);
                if(!s.setSubmissaoRegistada()) {
                    return false;
                }
        }
        return true;
    }
    
    public boolean setSubmissoesRegistada() {
        return setPagamentoEfectuado();
    }
    
    public ItemEvento[] obterArrayItemsEvento(List<Evento> dados) {
        ItemEvento[] items = new ItemEvento[dados.size()];

        for (int i = 0; i < items.length; i++) {
            items[i] = new ItemEvento(dados.get(i));
        }
        return items;
    }
    
    public ItemSubmissao[] obterArrayItemsSubissao(List<Submissao> dados) {
        ItemSubmissao[] items = new ItemSubmissao[dados.size()];
        
        for (int i = 0; i < items.length; i++) {
            items[i] = new ItemSubmissao(dados.get(i));
        }
        return items;
    }
    
    public ItemMetodoPagamento[] obterArrayItemsMetodosPagamento(List<Class<? extends MetodoPagamento>> dados) {
        ItemMetodoPagamento[] items = new ItemMetodoPagamento[dados.size()];
        
        for (int i = 0; i < items.length; i++) {
            items[i] = new ItemMetodoPagamento(dados.get(i));
        }
        return items;
    }
    
    public class ItemEvento {
        private Evento evento;
        
        public ItemEvento(Evento e) {
            this.evento = e;
        }

        public Evento getEvento() {
            return this.evento;
        }

        public void setEvento(Evento e) {
            this.evento = e;
        }

        @Override
        public String toString() {
            return String.format("%s", evento.getTitulo());
        }
    }
    
    public class ItemSubmissao {
        private Submissao s;

        public ItemSubmissao(Submissao s) {
            this.s = s;
        }

        public Submissao getSubmissao() {
            return s;
        }

        public void setSubmissao(Submissao s) {
            this.s = s;
        }

        @Override
        public String toString() {
            return String.format("%s (%s)", s.getArtigo().getTitulo(), s.getArtigo().getTipo().toString());
        }
    }
    
    public class ItemMetodoPagamento {
        private MetodoPagamento mp;
        private String nome;

        public ItemMetodoPagamento(Class<? extends MetodoPagamento> cls) {
            if(cls == CanadaExpressPagamento.class) {
                this.mp = new CanadaExpressPagamento();
                this.nome = "Canada Express";
            } else if(cls == VisaoLightPagamento.class) {
                this.mp = new VisaoLightPagamento();
                this.nome = "Visao Light";
            }
        }

        public MetodoPagamento getMetodoPagamento() {
            return mp;
        }

        public void setMetodoPagamento(MetodoPagamento mp) {
            this.mp = mp;
        }

        @Override
        public String toString() {
            return String.format("%s", this.nome);
        }
    }
}
