/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.*;
import java.io.File;
import java.util.List;

/**
 *
 * @author i130672
 */
public class ImportarEventosController {

    Empresa m_empresa;
    File ficheiro;
    FicheiroEvento ficheiroEvento;
    List<Evento> lista_evento;

    public ImportarEventosController(Empresa m_empresa) {
        this.m_empresa = m_empresa;
    }

    public void setFile(File ficheiro) {
        this.ficheiro = ficheiro;
        String nome = ficheiro.getName();
        if (nome.endsWith("csv")) {
            ficheiroEvento = new FicheiroEventoCSV();
            ficheiroEvento.setFile(ficheiro);
            lista_evento = ficheiroEvento.criarListaEventos(ficheiroEvento.lerFile());
        } else {
            if (nome.endsWith("xml")) {
                ficheiroEvento = new FicheiroEventoXML();
                ficheiroEvento.setFile(ficheiro);
                lista_evento = ficheiroEvento.criarListaEventos(ficheiroEvento.lerFile());
            }
        }

    }

    public File getFile() {
        return this.ficheiro;
    }

    public List<Evento> getListaEventos() {
        return this.lista_evento;
    }

    public void setListaEventos(List<Evento> lista_evento) {
        this.lista_evento = lista_evento;
    }
}
