/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Topico;
import eventoscientificos.model.TopicosACM;
import eventoscientificos.model.Utilizador;
import java.util.List;

/**
 *
 * @author dnamorim
 */
public class TopicosPericiaRevisorController {
    private Empresa empresa;
    private Utilizador utilizador;
    private Evento evento;
    private List<Topico> lstTopRev;
    
    public TopicosPericiaRevisorController(Empresa e, Utilizador u) {
        this.empresa = e;
        this.utilizador = u;
    }
    
    public ItemEvento[] getListaEventosRevisor() {
        List<Evento> lstEventos = this.empresa.getRegistoEventos().getEventosRevisor(utilizador);
        return obterArrayItemsEvento(lstEventos);
    }
    
    private ItemEvento[] obterArrayItemsEvento(List<Evento> lst) {
        ItemEvento[] ite = new ItemEvento[lst.size()];
        
        for (int i = 0; i < ite.length; i++) {
            ite[i] = new ItemEvento(lst.get(i));
        }
        return ite;
    }
    
    public void setEvento(Evento ev) {
        this.evento = ev;
    }
    
    public List<Topico> getTopicosRevisor() {
        for (Revisor r : this.evento.getCP().getListaRevisores()) {
            if(r.getUtilizador().equals(this.utilizador)) {
                this.lstTopRev = r.getListaTopicos();
                return r.getListaTopicos();
            }
        }
        
        return null;
    }
    
    public void eliminarTopicos(List<Topico> eliminar) {
        for (Topico topico : eliminar) {
            lstTopRev.remove(topico);
        }
    }
    
    public boolean adicionarTopico(String codigoACM) {
        Topico adicionar = TopicosACM.getTopicoByCodigoACM(codigoACM);
        if(this.lstTopRev.contains(adicionar)) {
            return false;
        } else {
            return this.lstTopRev.add(adicionar);
        }
    }
    
    public boolean registarTopicos() {
        return this.evento.setCPDefinida();
    }
    
    public class ItemEvento {

        private Evento evento;

        public ItemEvento(Evento e) {
            this.evento = e;
        }

        public Evento getEvento() {
            return evento;
        }

        public void setEvento(Evento e) {
            this.evento = e;
        }

        @Override
        public String toString() {
            return String.format("%s", evento.getTitulo());
        }
    }
    
}
