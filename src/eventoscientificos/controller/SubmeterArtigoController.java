package eventoscientificos.controller;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.TipoArtigo;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Utilizador;
import java.util.List;

/**
 *
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 * @author dnamorim
 */
public class SubmeterArtigoController {

    private Empresa m_empresa;
    private Evento m_evento;
    private Submissao m_submissao;
    private Artigo m_artigo;

    /**
     * Construtor da classe que recebe como parâmetro a empresa.
     *
     * @param empresa empresa responsável pelo evento
     */
    public SubmeterArtigoController(Empresa empresa) {
        m_empresa = empresa;
    }

    /**
     * Método que devolve a lista de eventos para os quais o autor pode submeter
     * artigos.
     *
     * @return lista de eventos
     */
    public ItemEvento[] getEventosPodeSubmeter() {
        List<Evento> lstEventos = this.m_empresa.getRegistoEventos().getListaEventosPodeSubmeter();
        return obterArrayItemsEvento(lstEventos);
    }

    /**
     * Atribui à variável m_evento o evento seleccionado na UI e cria uma nova
     * submissão dentro desse mesmo evento e um nov artigo nessa mesma
     * submissão.
     *
     * @param e evento seleccionado
     */
    public void selectEvento(Evento e) {
        m_evento = e;
        this.m_submissao = this.m_evento.novaSubmissao();
        this.m_artigo = this.m_submissao.novoArtigo();
    }


    /**
     * Modifica o titulo e o resumo do artigo.
     *
     * @param strTitulo titulo do artigo
     * @param strResumo resumo do artigo
     */
    public void setDados(String strTitulo, String strResumo) {
        this.m_artigo.setTitulo(strTitulo);
        this.m_artigo.setResumo(strResumo);
    }

    /**
     * Devolve o autor com os parâmetros introduzidos caso este exista e caso
     * contrário cria um novo autor com esses mesmos parâmetros.
     *
     * @param strNome nome do autor
     * @param strAfiliacao afiliação do autor
     * @param strEmail email do autor
     * @return autor
     */
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail) {
        Utilizador u = m_empresa.getRegistoUtilizadores().getUtilizadorEmail(strEmail);
        if (u != null) {
            return this.m_artigo.novoAutor(strAfiliacao, u);
        }
        return this.m_artigo.novoAutor(strNome, strAfiliacao, strEmail);
    }

    /**
     * Adiciona o autor anterior ao artigo.
     *
     * @param autor autor que se pretende adicionar ao artigo
     * @return o artigo com o autor atribuido
     */
    public boolean addAutor(Autor autor) {
        return this.m_artigo.addAutor(autor);
    }

    /**
     * Devolve os possiveis autores correspondentes do artigo.
     *
     * @return autores correspondentes
     */
    public List<Autor> getPossiveisAutoresCorrespondentes() {
        return this.m_artigo.getPossiveisAutoresCorrespondentes();
    }

    /**
     * Modifica o autor correspondente do artigo.
     *
     * @param autor autor do artigo
     */
    public void setCorrespondente(Autor autor) {
        this.m_artigo.setAutorCorrespondente(autor);
    }

    /**
     * Modifica o ficheiro do artigo.
     *
     * @param strFicheiro ficheiro com artigo
     */
    public void setFicheiro(String strFicheiro) {
        this.m_artigo.setFicheiro(strFicheiro);
    }

    /**
     * Devolve a informação da submissão e do artigo.
     *
     * @return informação
     */
    public String getInfoResumo() {
        return String.format("Título: %s%nTipo: %s%nFicheiro: %s",this.m_artigo.getTitulo(), this.m_artigo.getTipo(), this.m_artigo.getFicheiro());
    }

    /**
     * Regista o artigo na submissão e posteriormente adiciona a submissão ao
     * evento.
     *
     * @return evento com a submissão adicionada
     */
    public boolean registarSubmissao() {
        this.m_submissao.setArtigo(m_artigo);
        return this.m_evento.addSubmissao(m_submissao);
    }

    /**
     * Modifica a lista de tópicos do artigo.
     *
     * @param listaTopicosArtigo lista de tópicos artigo
     */
    public void setListaTopicosArtigo(List<Topico> listaTopicosArtigo) {
        m_artigo.setListaTopicos(listaTopicosArtigo);
    }
    
    public void setTipoArtigo(TipoArtigo tipo) {
        m_artigo.setTipo(tipo);
    }
    
    public boolean eventoHasTopico(Topico t) {
        return m_evento.getTopicos().contains(t);
    }
    
    public ItemEvento[] obterArrayItemsEvento(List<Evento> dados) {
        ItemEvento[] items = new ItemEvento[dados.size()];

        for (int i = 0; i < items.length; i++) {
            items[i] = new ItemEvento(dados.get(i));
        }
        return items;
    }
    
    public class ItemEvento {
        private Evento e;
        
        public ItemEvento(Evento e) {
            this.e = e;
        }

        public Evento getEvento() {
            return e;
        }

        public void setEvento(Evento e) {
            this.e = e;
        }

        @Override
        public String toString() {
            return String.format("%s", e.getTitulo());
        }
    }

}
