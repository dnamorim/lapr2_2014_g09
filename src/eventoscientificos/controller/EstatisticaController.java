/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.EstatisticaEventos;
import eventoscientificos.model.EstatisticaMediaAvaliacoes;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Organizador;
import eventoscientificos.model.Revisao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dnamorim
 */
public class EstatisticaController {

    private Evento m_evento;
    private Utilizador ut;
    private Empresa m_empresa;
    private Organizador m_organizador;
    private List<Submissao> m_listaSubmissoes;
    private EstatisticaEventos eE;
    private EstatisticaMediaAvaliacoes eMa;

    public EstatisticaController(Empresa emp) {
        m_empresa = emp;
    }

    public void setEvento(Evento e) {
        this.m_evento = e;
    }

    public ItemEvento[] getListaEvento() {
        List<Evento> list = m_empresa.getRegistoEventos().getEventosOrganizador(ut);
        if (!list.isEmpty()) {
            assignOrganizador();
        }
        return obterArrayItemsEvento(list);
    }

    private ItemEvento[] obterArrayItemsEvento(List<Evento> dados) {
        ItemEvento[] items = new ItemEvento[dados.size()];

        for (int i = 0; i < items.length; i++) {
            items[i] = new ItemEvento(dados.get(i));
        }
        return items;
    }

    private void assignOrganizador() {
        List<Organizador> listaOrg = m_evento.getListaOrganizadores();

        for (Organizador o : listaOrg) {
            if (o.getUtilizador().equals(ut)) {
                m_organizador = o;
            }
        }
    }

    /**
     * Método que permite saber os artigos de um revisor
     *
     * @param user Utilizador
     * @return lista de artigos do revisor
     */
    public List<Artigo> getArtigosRevisor(Utilizador user) {
        return this.eMa.getArtigosRevisor(user);
    }

    public float getMediaArtigo(Revisao rev) {
        return this.eMa.calcMediaArtigo(rev);
    }

    public float getMediaGlobalArtigos(Utilizador u) {
        return this.eMa.calcMediaGlobalArtigos(u);
    }

    public float getDesvioRevisor(Utilizador u) {
        return this.eMa.calcDesvioRevisor(u);
    }

    public float getDesvioMedioRevisor(Utilizador u) {
        return this.eMa.calcDesvioMedioRevisor(u);
    }

    public float getSomaDesviosRevisor(Utilizador u) {
        return this.eMa.calcSomaDesviosRevisor(u);
    }

    public int getNumeroDesviosDoRevisor(Utilizador u) {
        return this.eMa.calcNumeroDeDesviosRevisor(u);
    }

    public double getVarianciaRevisor(Utilizador u) {
        return this.eMa.calcVarianciaRevisor(u);
    }

    public double getET(Utilizador u) {
        return this.eMa.calcET(u);
    }

    public boolean checkET(Utilizador u) {
        return this.eMa.checkETandZC(u);
    }

    public float getTaxaAceitacao(Evento e) {
        return this.eE.calcTaxaAceitacao(e);
    }

    public float getMediaQualidade(Evento e) {
        return this.eE.calcMediaQualidade(e);
    }

    public float getMediaAdequacao(Evento e) {
        return this.eE.calcMediaAdequacao(e);
    }

    public float getMediaOriginalidade(Evento e) {
        return this.eE.calcMediaOriginalidade(e);
    }

    public float getMediaConfianca(Evento e) {
        return this.eE.calcMediaConfianca(e);
    }
    public void setZConfianca(double z) {
        this.eMa.setZConfianca(z);
    }

    public List<Submissao> getSubmissoesEvento(Evento e) {
        List<Evento> listaEventos = m_empresa.getRegistoEventos().getEventosOrganizador(ut);
        if (listaEventos.contains(e)) {
            m_listaSubmissoes = e.getListaSubmissoes();

        }
        return m_listaSubmissoes;
    }

    public int getNumeroRevisores() {
        return this.eMa.getNumeroRevisores();
    }

    public List<Revisor> getListaRevisoresEventosOrganizador() {
        return this.eMa.getListaRevisoresEventosOrganizador();
    }

    public double getZConfianca() {
        return this.eMa.getZConfianca();
    }

    public class ItemEvento {

        private Evento evento;

        public ItemEvento(Evento e) {
            this.evento = e;
        }

        public Evento getEvento() {
            return evento;
        }

        public void setEvento(Evento e) {
            this.evento = e;
        }

        @Override
        public String toString() {
            return String.format("%s", evento.getTitulo());
        }

    }
}
