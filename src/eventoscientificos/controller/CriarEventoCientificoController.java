package eventoscientificos.controller;


import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Organizador;
import eventoscientificos.model.Utilizador;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Nuno Silva
 */

public class CriarEventoCientificoController
{
    private Empresa m_empresa;
    private Evento m_evento;

    public CriarEventoCientificoController(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public void novoEvento()
    {
        m_evento = m_empresa.getRegistoEventos().novoEvento();
    }

    public String getEventoString()
    {
        return m_evento.toString();
    }

    public boolean setTitulo(String strTitulo)
    {
        if(!strTitulo.isEmpty()) {
            m_evento.setTitulo(strTitulo);
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setDescricao(String strDescricao)
    {
        if(!strDescricao.isEmpty()) {
            m_evento.setDescricao(strDescricao);
            return true;
        } else {
            return false;
        }
    }

   public boolean setPais(String pais) {
       if(!pais.isEmpty()) { 
            m_evento.getLocal().setPais(pais);
            return true;
       } else {
           return false;
       }
    }

    public boolean setCidade(String cidade) {
        if(!cidade.isEmpty()) {
            m_evento.getLocal().setCidade(cidade);
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setDataInicio(Date dataInicio)    {
        if(dataInicio == null || dataInicio.before(m_evento.getDataLimiteRegisto().getTime())) {
            return false;
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dataInicio);
            m_evento.setDataInicio(cal);
            return true;
        }
    }
    
    public boolean setDataFim(Date dataFim)
    {
        if(dataFim == null || dataFim.before(m_evento.getDataInicio().getTime())) {
           return false;
        } else { Calendar cal = Calendar.getInstance();
            cal.setTime(dataFim);
            m_evento.setDataFim(cal);
            return true;
        }
        
    }
    
    public boolean setDataLimiteSubmissao(Date dataLimiteSubmissao)
    {
        if(dataLimiteSubmissao == null) {
            return false;       
        } else { 
            Calendar cal = Calendar.getInstance();
            cal.setTime(dataLimiteSubmissao);
            m_evento.setDataLimiteSubmissao(cal);
            return true;
        }
    }
    
    
    public boolean setDataLimiteSubmissaoFinal(Date dataLimiteSubmissaoFinal)
    {
        if(dataLimiteSubmissaoFinal == null || dataLimiteSubmissaoFinal.before(m_evento.getDataLimiteRevisao().getTime())) {
            return false;
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dataLimiteSubmissaoFinal);
            m_evento.setDataLimiteSubmissaoFinal(cal);
            return true;  
        }
    }
    
    public boolean setDataLimiteRevisao(Date dataLimiteRevisao)
    {
        if(dataLimiteRevisao == null || dataLimiteRevisao.before(m_evento.getDataLimiteSubmissao().getTime())) {
            return false;
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dataLimiteRevisao);
            m_evento.setDataLimiteRevisao(cal);
            return true;
        }
    }
    
    public boolean setDataLimiteRegisto(Date dataLimiteRegisto)
    {
        if(dataLimiteRegisto == null || dataLimiteRegisto.before(m_evento.getDataLimiteSubmissaoFinal().getTime())) {
            return false;
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dataLimiteRegisto);
            m_evento.setDataLimiteRegisto(cal);
            return true;
        }
    }

    public boolean addOrganizador(String strId)
    {
        Utilizador u = m_empresa.getRegistoUtilizadores().getUtilizador(strId);
        
        if( u!=null) {
           return m_evento.addOrganizador(u);
        } else {
            return false;
        }
    }
    
    public boolean registaEvento()
    {
        return m_empresa.getRegistoEventos().registaEvento(m_evento);
    }
    
    public String getStringOrganizador(String strID) {
        Utilizador u = m_empresa.getRegistoUtilizadores().getUtilizador(strID);
        return String.format("%s (%s) - E-Mail: %s", u.getNome(), u.getUsername(), u.getEmail());
    } 

    public boolean setWebsite(String website) {
        try {
            URL url = new URL(website);
            m_evento.setWebsite(website);
            return true;
        } catch (MalformedURLException e) {
            return false;
        }
    }
    
    public String getWebsite() {
        return this.m_evento.getWebsite();
    }

    public boolean setHost(String text) {
        if(text.isEmpty()) {
            return false;
        } else {
            m_evento.setHost(text);
            return true;
        }
    }
    
    public String getHost() {
        return m_evento.getHost();
    }
    
    public Evento getEvento() {
        return this.m_evento;
    }
}

