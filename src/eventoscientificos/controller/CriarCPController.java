
package eventoscientificos.controller;


import eventoscientificos.model.CP;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.EventoState.EventoTopicosCriadosState;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nuno Silva
 */

public class CriarCPController
{
    private Empresa m_empresa;
    private Evento m_evento;
    private CP m_cp;

    public CriarCPController(Empresa empresa)
    {
        m_empresa = empresa;
    }

    public Object[] getEventosOrganizador(Utilizador u)
    {
        List<Evento> list = m_empresa.getRegistoEventos().getEventosOrganizador(u);
        return obterArrayItemsEvento(list);
    
    }
    
    public List<Topico> getTopicosEvento()
    {
        if( m_evento != null )
            return m_evento.getTopicos();
        else
            return null;
    }
    
    public void selectEvento(Evento e)
    {
        m_evento = e;
        
        m_cp = m_evento.novaCP();
    }
    
    public Revisor addMembroCP(String strId)
    {
        Utilizador u = m_empresa.getRegistoUtilizadores().getUtilizador(strId);
        
        if( u!=null)
            return m_cp.addMembroCP( strId, u );
        else
            return null;
    }
    
    public boolean registaMembroCP( Revisor r )
    {
        return m_cp.registaMembroCP(r);
    }
    
    public void setCP()
    {
        m_evento.setCP(m_cp); 
    }
    
    public boolean setCPDefinida() {
        return m_evento.setCPDefinida();
    }
    
    private Object[] obterArrayItemsEvento(List<Evento> dados) {
        List<Evento> items = new ArrayList<Evento>();

        for (int i = 0; i < dados.size(); i++) {
            if(dados.get(i).getState() instanceof EventoTopicosCriadosState) {
                items.add(dados.get(i));
            }
        }
        return items.toArray();
    }
    
    public void setListaTopicosRevisor(Revisor r, List<Topico> listaTopicosRevisor)
    {
        r.setListaTopicos( listaTopicosRevisor );
    }
    
    public class ItemEvento {

        private Evento evento;

        public ItemEvento(Evento e) {
            this.evento = e;
        }

        public Evento getEvento() {
            return evento;
        }

        public void setEvento(Evento e) {
            this.evento = e;
        }

        @Override
        public String toString() {
            return String.format("%s", evento.getTitulo());
        }
    }
}

