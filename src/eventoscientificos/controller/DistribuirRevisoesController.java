/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Distribuicao;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.EventoState.EventoProntoADistribuirState;
import eventoscientificos.model.Mecanismo1;
import eventoscientificos.model.MecanismoDistribuicao;
import eventoscientificos.model.ProcessoDistribuicao;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author dnamorim
 */
public class DistribuirRevisoesController {
    
    private Empresa empresa;
    private Evento evento;
    private ProcessoDistribuicao pd;
    private MecanismoDistribuicao mecanismo;
    
    public DistribuirRevisoesController(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public Object[] getEventosPodeDistribuir(Utilizador u) {
        List<Evento> lstEventos = this.empresa.getRegistoEventos().getEventosOrganizador(u);
        return obterArrayItemEventos(lstEventos);
    }
    
    private Object[] obterArrayItemEventos(List<Evento> lst) {
        List<Evento> ite = new ArrayList<Evento>();
        
        for (int i = 0; i < lst.size(); i++) {
            if(lst.get(i).getState() instanceof EventoProntoADistribuirState) {
                ite.add(lst.get(i));
            }
        }
        return ite.toArray();
    } 
    
    public ItemMecanismo[] getMecanismosDistribuicao() {
        List<Class<? extends MecanismoDistribuicao>> lstMec = empresa.getMecanismosDistribuicao();
        return obterArrayItemMecanismos(lstMec);
    }
    
    private ItemMecanismo[] obterArrayItemMecanismos(List<Class<? extends MecanismoDistribuicao>> lst) {
        ItemMecanismo[] ite = new ItemMecanismo[lst.size()];
        
        for (int i = 0; i < ite.length; i++) {
            ite[i] = new ItemMecanismo(lst.get(i));
        }
        return ite;
    } 
    
    public void criarProcessoDistribuicao() {
        this.pd = new ProcessoDistribuicao();
        this.pd.setMecanismoDistribuicao(mecanismo);
        this.pd.setEvento(evento);
        this.pd.distribuir();
    }
    
    public String resultadoDistribuicao() {
        String resDist = "";
        for (Distribuicao dist : this.pd.getProcessoDistribuicao()) {
            resDist += String.format("Artigo: %s (%s)%n", dist.getArtigo().getTitulo(), dist.getArtigo().getTipo());
            int i = 1;
            for (Revisor rev : dist.getListRevisor()) {
                resDist += String.format("  %d. %s (%s)%n", i, rev.getNome(), rev.getUtilizador().getUsername());
                i++;
            }
            resDist += String.format("%n");
        }
        return resDist;
    }
    
    public boolean registarDistribuicao() {
        boolean flagErro = false;
        for (Submissao s : this.evento.getListaSubmissoes()) {
            Artigo a = s.getArtigo();
            for (ListIterator<Distribuicao> itd = this.pd.getProcessoDistribuicao().listIterator(); itd.hasNext();) {
                Distribuicao d = itd.next();
                if(d.getArtigo().equals(a)) {
                    a.setDistribuicao(d);
                    break;
                }
            }
            if(!s.setDistribuida()) {
                flagErro = true;
            }
            
        }
        if(!flagErro) {
            return this.evento.setDistribuido();
        } else {
            return false;
        }
    }
    
    
    public void setEvento(Evento e) {
        this.evento = e;
    }
    
    public void setMecanismo(MecanismoDistribuicao m) {
        this.mecanismo = m;
    }
    
    public class ItemEvento {

        private Evento evento;

        public ItemEvento(Evento e) {
            this.evento = e;
        }

        public Evento getEvento() {
            return evento;
        }

        public void setEvento(Evento e) {
            this.evento = e;
        }

        @Override
        public String toString() {
            return String.format("%s", evento.getTitulo());
        }
    }
    
    public class ItemMecanismo {

        private MecanismoDistribuicao mecanismo;
        

        public ItemMecanismo(Class<? extends MecanismoDistribuicao> cls) {
            if(cls == Mecanismo1.class) {
                this.mecanismo= new Mecanismo1();
            }
        }

        public MecanismoDistribuicao getMecanismoDistribuicao() {
            return mecanismo;
        }

        public void setFormulaPagamento(MecanismoDistribuicao mec) {
            this.mecanismo = mec;
        }
        
        public String getDescricao() {
            return mecanismo.descricaoMecanismo();
        }

        @Override
        public String toString() {
            return String.format("%s", this.mecanismo.nameMecanismo());
        }
    }
}
