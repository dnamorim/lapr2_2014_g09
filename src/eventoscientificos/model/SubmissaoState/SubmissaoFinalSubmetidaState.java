package eventoscientificos.model.SubmissaoState;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Submissao;
import java.io.Serializable;

/**
 *
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 */
public class SubmissaoFinalSubmetidaState implements SubmissaoState, Serializable {

    Submissao m_submissao;

    public SubmissaoFinalSubmetidaState(Submissao s) {
        m_submissao = s;
    }

    @Override
    public boolean setCriado() {
        return false;
    }

    @Override
    public boolean setSubmetido() {
        return false;
    }

    @Override
    public boolean setDistribuido() {
        return false;
    }

    @Override
    public boolean setRevisto() {
        return false;
    }

    @Override
    public boolean setRejeitado() {
        return false;
    }

    @Override
    public boolean setAceite() {
        return false;
    }

    @Override
    public boolean setFinalSubmetido() {
        return true;
    }

    @Override
    public boolean setRegistado() {
        if(valida()) {
            m_submissao.setState(new SubmissaoRegistadaNoEventoState(m_submissao));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean valida() {
        if(m_submissao.isPaga()) {
            return true;
        } else {
            return false;
        }
    }
}
