package eventoscientificos.model.SubmissaoState;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Submissao;
import java.io.Serializable;

/**
 *
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 */
public class SubmissaoSubmetidaState implements SubmissaoState, Serializable {

    Submissao m_submissao;

    public SubmissaoSubmetidaState(Submissao s) {
        m_submissao = s;
    }

    @Override
    public boolean setCriado() {
        return false;
    }

    @Override
    public boolean setSubmetido() {
        return true;
    }

    @Override
    public boolean setDistribuido() {
        if(valida()) {
            this.m_submissao.setState(new SubmissaoDistribuidaState(m_submissao));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setRevisto() {
        return false;
    }

    @Override
    public boolean setRejeitado() {
        return false;
    }

    @Override
    public boolean setAceite() {
        return false;
    }

    @Override
    public boolean setFinalSubmetido() {
        return false;
    }

    @Override
    public boolean setRegistado() {
        return false;
    }

    @Override
    public boolean valida() {
        return true;
    }

}
