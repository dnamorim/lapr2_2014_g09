package eventoscientificos.model.SubmissaoState;

/**
 *
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 */
public interface SubmissaoState {

    boolean setCriado();

    boolean setSubmetido();

    boolean setDistribuido();

    boolean setRevisto();

    boolean setRejeitado();

    boolean setAceite();

    boolean setFinalSubmetido();

    boolean setRegistado();

    boolean valida();

}
