package eventoscientificos.model.SubmissaoState;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Submissao;
import java.io.Serializable;

/**
 *
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 */
public class SubmissaoRegistadaNoEventoState implements SubmissaoState, Serializable {

    Submissao m_submissao;

    public SubmissaoRegistadaNoEventoState(Submissao s) {
        m_submissao = s;
    }

    @Override
    public boolean setCriado() {
        return false;
    }

    @Override
    public boolean setSubmetido() {
        return false;
    }

    @Override
    public boolean setDistribuido() {
        return false;
    }

    @Override
    public boolean setRevisto() {
        return false;
    }

    @Override
    public boolean setRejeitado() {
        return false;
    }

    @Override
    public boolean setAceite() {
        return false;
    }

    @Override
    public boolean setFinalSubmetido() {
        return false;
    }

    @Override
    public boolean setRegistado() {
        return true;
    }

    @Override
    public boolean valida() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
