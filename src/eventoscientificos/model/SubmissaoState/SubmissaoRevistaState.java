package eventoscientificos.model.SubmissaoState;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Submissao;
import java.io.Serializable;

/**
 *
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 */
public class SubmissaoRevistaState implements SubmissaoState, Serializable {

    Submissao m_submissao;

    public SubmissaoRevistaState(Submissao s) {
        m_submissao = s;
    }

    @Override
    public boolean setCriado() {
        return false;
    }

    @Override
    public boolean setSubmetido() {
        return false;
    }

    @Override
    public boolean setDistribuido() {
        return false;
    }

    @Override
    public boolean setRevisto() {
        return true;
    }

    @Override
    public boolean setRejeitado() {
       if(valida()) {
           this.m_submissao.setState(new SubmissaoRejeitadaState(m_submissao));
           return true;
       } else {
           return false;
       }
    }

    @Override
    public boolean setAceite() {
       if(valida()) {
           this.m_submissao.setState(new SubmissaoAceiteState(m_submissao));
           return true;
       } else {
           return false;
       } 
    }

    @Override
    public boolean setFinalSubmetido() {
        return false;
    }

    @Override
    public boolean setRegistado() {
        return false;
    }

    @Override
    public boolean valida() {
        if(m_submissao.getArtigo().getDecisao().getDecisao()) {
            return true;
        }
        if(!m_submissao.getArtigo().getDecisao().getDecisao()) {
            return true;
        }
        return false;
    }

}
