package eventoscientificos.model.SubmissaoState;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Submissao;
import java.io.Serializable;

/**
 *
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 */
public class SubmissaoCriadaState implements SubmissaoState, Serializable {

    Submissao m_submissao;

    public SubmissaoCriadaState(Submissao s) {
        m_submissao = s;
    }

    @Override
    public boolean setCriado() {
        return true;
    }

    @Override
    public boolean setSubmetido() {
        if (valida()) {
            m_submissao.setState(new SubmissaoSubmetidaState(m_submissao));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setDistribuido() {
        return false;
    }

    @Override
    public boolean setRevisto() {
        return false;
    }

    @Override
    public boolean setRejeitado() {
        return false;
    }

    @Override
    public boolean setAceite() {
        return false;
    }

    @Override
    public boolean setFinalSubmetido() {
        return false;
    }

    @Override
    public boolean setRegistado() {
        return false;
    }

    @Override
    public boolean valida() {
        Artigo art = m_submissao.getArtigo();
        if (art.getTitulo().isEmpty() || art.getResumo().isEmpty()
                || art.getAutorCorrespondente() == null || art.getListaTopicos().size() < 1) {
            return false;
        } else {
            return true;
        }
    }

}
