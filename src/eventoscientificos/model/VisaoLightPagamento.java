/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import pt.ipp.isep.dei.eapli.visaolight.*;

/**
 *
 * @author dnamorim
 */
public class VisaoLightPagamento implements MetodoPagamento {

    private String nrCartao;
    private String dataValidade;
    private String dataAutorizacao;
    private String dataPagamento;
    private DateFormat formatter, formatterOut;
    
    public VisaoLightPagamento() {
        formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatterOut = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.UK);
    }
    
    @Override
    public void setInfoPagamento(String nrCartao, Date dataValidade, Date dataAutorizacao) {
        this.nrCartao = nrCartao;
        
        this.dataValidade = formatter.format(dataValidade);
        this.dataAutorizacao = formatter.format(dataAutorizacao);
    }

    @Override
    public boolean pagamento(float valorAPagar) {
        dataPagamento = VisaoLight.getAutorizacaoDCC(nrCartao, dataValidade, valorAPagar, dataAutorizacao);
        if(dataPagamento != null) {
            return true;
        }
        return false;
    }
    
    public Date getDataPagamento() {
        
        try {
            return formatterOut.parse(dataPagamento);
        } catch (ParseException ex) {
            Logger.getLogger(VisaoLightPagamento.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
