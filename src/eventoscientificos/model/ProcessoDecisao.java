/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class ProcessoDecisao implements Serializable {

    private MecanismoDecisao mecanismo;
    private List<Decisao> listDecisao;
    private Evento evento;
    
    public ProcessoDecisao() {
        listDecisao = new ArrayList<Decisao>();
    }

    public void setListDistribuicao(List<Decisao> listDecisao) {
        this.listDecisao = listDecisao;
    }
    
    public void novaDecisao(Decisao d) {
        this.listDecisao.add(d);
    }
    
    public void setEvento(Evento e) {
        this.evento = e;
    }
    
    public List<Decisao> getProcessoDecisao() {
        return this.listDecisao;
    }
    
    public void setMecanismoDistribuicao(MecanismoDecisao mecanismo) {
        this.mecanismo = mecanismo;
    }
    
    public Evento getEvento() {
        return evento;
    }
    
    public void decidir() {
        this.mecanismo.decidir(this);
    }
    
}

