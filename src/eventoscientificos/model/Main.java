/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import eventoscientificos.ui.*;
import java.io.IOException;
import javax.swing.JOptionPane;



/**
 *
 * @author dnamorim
 */

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String title = "TOCS Gestão de Eventos Científicos";
        
        Empresa empresa = new Empresa();
        
        try {
            empresa.loadData();
            TopicosACM.readTopicosACM();
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Não foram encontrados os dados gravados da Aplicação.\nA Aplicação vai iniciar sem dados", title, JOptionPane.WARNING_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possível ler os dados gravados da Aplicação.\nA aplicação vai iniciar sem dados.", title, JOptionPane.WARNING_MESSAGE);
        } finally {
            LoginUI login= new LoginUI(empresa, title);
            empresa.createAdmin();
        }
    }

}
