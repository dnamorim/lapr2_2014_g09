/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.model.EventoState.*;
import eventoscientificos.model.SubmissaoState.SubmissaoAceiteState;
import eventoscientificos.model.SubmissaoState.SubmissaoDistribuidaState;
import eventoscientificos.model.SubmissaoState.SubmissaoFinalSubmetidaState;
import eventoscientificos.model.SubmissaoState.SubmissaoRegistadaNoEventoState;
import eventoscientificos.model.SubmissaoState.SubmissaoRevistaState;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author dnamorim
 */
public class Evento implements Serializable {

    private String m_strTitulo;
    private String m_strDescricao;
    private Local m_local;
    private String m_website;
    private EventoState m_state;
    private String m_host;
    private float precoShort, precoFull, precoPoster;
    private FormulaPagamento formPag;
  
    private Calendar m_dataInicio;
    private Calendar m_dataFim;
    private Calendar m_dataLimiteSubmissao;
    private Calendar m_dataLimiteSubmissaoFinal;
    private Calendar m_dataLimiteRegisto;
    private Calendar m_dataLimiteRevisao;

    private List<Organizador> m_listaOrganizadores;
    private List<Submissao> m_listaSubmissoes;
    private CP m_cp;
    private List<Topico> m_listaTopicos;

    public Evento() {
        m_local = new Local();
        m_listaOrganizadores = new ArrayList<Organizador>();
        m_listaSubmissoes = new ArrayList<Submissao>();
        m_listaTopicos = new ArrayList<Topico>();
        setState(new EventoCriadoState(this));
    }

    public Evento(String titulo, String descricao) {
        m_local = new Local();
        m_listaOrganizadores = new ArrayList<Organizador>();
        m_listaSubmissoes = new ArrayList<Submissao>();
        m_listaTopicos = new ArrayList<Topico>();
        this.setTitulo(titulo);
        this.setDescricao(descricao);

    }
    
    public void setFormulaPagamento(FormulaPagamento f) {
        this.formPag = f;
    }
    
    public FormulaPagamento getFormulaPagamento() {
        return this.formPag;
    }
    
    public float getValorAPagar(List<Submissao> lstSub) {
        return formPag.valorAPagar(lstSub);
    }
    
    public String getTitulo() {
        return m_strTitulo;
    }

    public Calendar getDataInicio() {
        return m_dataInicio;
    }

    public Calendar getDataFim() {
        return m_dataFim;
    }
    
    public Calendar getDataLimiteSubmissao() {
        return this.m_dataLimiteSubmissao;
    }
    
    public Calendar getDataLimiteSubmissaoFinal() {
        return this.m_dataLimiteSubmissaoFinal;
    }
    
    public Calendar getDataLimiteRegisto() {
        return this.m_dataLimiteRegisto;
    }
 
    public Calendar getDataLimiteRevisao() {
        return this.m_dataLimiteRevisao;
    } 
    
    public String getDescricao() {
        return this.m_strDescricao;
    }
    
    public EventoState getState() {
        return this.m_state;
    }

    public CP novaCP() {
        m_cp = new CP();

        return m_cp;
    }

    public final void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }

    public final void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    public void setDataInicio(Calendar dataInicio) {
        this.m_dataInicio = dataInicio;
    }

    public void setDataFim(Calendar dataFim) {
        this.m_dataFim = dataFim;
    }
    
    public void setDataLimiteSubmissao(Calendar data) {
        this.m_dataLimiteSubmissao = data;
    }
    
    public void setDataLimiteSubmissaoFinal(Calendar data) {
        this.m_dataLimiteSubmissaoFinal = data;
    }
    
    public void setDataLimiteRegisto(Calendar data) {
        this.m_dataLimiteRegisto = data;
    }
    
    public void setDataLimiteRevisao(Calendar data) {
        this.m_dataLimiteRevisao = data;
    }

    public void setLocal(Local m_local) {
        this.m_local = m_local;
    }

    public void setState(EventoState state) {
        m_state = state;
    }

    public Local getLocal() {
        return this.m_local;
    }

    public float getPrecoShort() {
        return precoShort;
    }

    public void setPrecoShort(float precoShort) {
        if(precoPoster>=0) {
            this.precoShort = precoShort;
        } else {
            throw new IllegalArgumentException("Preço Inválido");
        }

    }

    public float getPrecoFull() {
        return precoFull;
    }
    
    public void setPrecoFull(float precoFull) {
        if(precoPoster>=0) {
            this.precoFull = precoFull;
        } else {
            throw new IllegalArgumentException("Preço Inválido");
        }
    }

    public float getPrecoPoster() {
        return precoPoster;
    }

    public void setPrecoPoster(float precoPoster) {
        if(precoPoster>=0) {
            this.precoPoster = precoPoster;
        } else {
            throw new IllegalArgumentException("Preço Inválido");
        }
    }
    
    public List<Organizador> getListaOrganizadores() {
        List<Organizador> lOrg = new ArrayList<Organizador>();

        for (ListIterator<Organizador> it = m_listaOrganizadores.listIterator(); it.hasNext();) {
            lOrg.add(it.next());
        }

        return lOrg;
    }
    
    public List<Submissao> getListaSubmissoesPodeRegistar(Utilizador u) {
        List<Submissao> lSub = new ArrayList<Submissao>();
        
        for (ListIterator<Submissao> it = m_listaSubmissoes.listIterator(); it.hasNext();) {
            Submissao s = it.next();
            if (s.getState() instanceof SubmissaoAceiteState) {
               for(ListIterator<Autor> itAut = s.getArtigo().getListaAutores().listIterator(); itAut.hasNext();) {
                   Autor a = itAut.next();
                   if(a.getUtilizador().equals(u)) {
                       lSub.add(s);
                   }
               }
            }
        }
        
        return lSub;
    }
    
    public List<Submissao> getListaSubmissoesARever(Utilizador u) {
        List<Submissao> lSub = new ArrayList<Submissao>();
        
        for (Submissao s : m_listaSubmissoes) {
            if (s.getState() instanceof SubmissaoDistribuidaState) {
               for(Revisor r : s.getArtigo().getDistribuicao().getListRevisor()) {
                   if(r.getUtilizador().equals(u)) {
                       lSub.add(s);
                       break;
                   }
               }
            }
        }
        
        return lSub;
    }
    
    public boolean podeRegistar(Utilizador u) {
        List<Submissao> lSub = new ArrayList<Submissao>();
        
        for (ListIterator<Submissao> it = m_listaSubmissoes.listIterator(); it.hasNext();) {
            Submissao s = it.next();
            if (s.getState() instanceof SubmissaoFinalSubmetidaState) {
               for(ListIterator<Autor> itAut = s.getArtigo().getListaAutores().listIterator(); itAut.hasNext();) {
                   Autor a = itAut.next();
                   if(a.getUtilizador().equals(u)) {
                       return true;
                   }
               }
            }
        }
        
        return false;
    }
    
    private boolean podeSubmeterFinal(String strID) {
        List<Submissao> lSubFin = new ArrayList<Submissao>();
        
        for (ListIterator<Submissao> it = m_listaSubmissoes.listIterator(); it.hasNext();) {
            Submissao s = it.next();
            if (s.getState() instanceof SubmissaoAceiteState && !(s.isPaga())) {
               for(ListIterator<Autor> itAut = s.getArtigo().getListaAutores().listIterator(); itAut.hasNext();) {
                   Autor a = itAut.next();
                   if(a.getEMail().equals(strID)) {
                       return true;
                   }
               }
            }
        }
        
        return false;
    }
    

    public boolean addOrganizador(Utilizador u) {
        Organizador o = new Organizador(u);

        o.valida();
        
        return addOrganizador(o);
    }

    private boolean addOrganizador(Organizador o) {
        
        if(m_listaOrganizadores.contains(o)) {
            return false;
        } else {
            return m_listaOrganizadores.add(o);
        }
    }

    public boolean valida() {
        if(m_listaOrganizadores.size() >= 1) {
            return true;
        } else {
            return false;
        }
        
    }

    public void setCP(CP cp) {
        m_cp = cp;
    }

    @Override
    public String toString() {
        return this.m_strTitulo;
    }

    public boolean aceitaSubmissoes() {
        if(this.m_state instanceof EventoCPDefinidaState) {
            //if(this.m_dataLimiteSubmissao.getTime().before(new Date())) {
              //  return false;
            //} else {
                return true;
            //}
        } else {
            return false;
        }
    }

    public boolean aceitaSubmissoesFinais(String strID) {
        if(this.m_state instanceof EventoNotificadoState) {
            //if(this.m_dataLimiteSubmissaoFinal.getTime().before(new Date())) return false; else 
            if(this.podeSubmeterFinal(strID)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
  
    
    public boolean aceitaRegistoNoEvento() {
        if(this.m_state instanceof EventoCameraReadyState) {
            if(this.m_dataLimiteRegisto.getTime().before(new Date())) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
    
    public Submissao novaSubmissao() {
        return new Submissao();
    }

    public boolean addSubmissao(Submissao submissao) {
        if (validaSubmissao(submissao) && submissao.setSubmetido()) {
            return this.m_listaSubmissoes.add(submissao);
        } else {
            return false;
        }
    }

    
     public boolean addSubmissaoFinal(Submissao submissao) {
        if (validaSubmissao(submissao) && submissao.setFinalSubmetido()) {
            return this.m_listaSubmissoes.add(submissao);
        } else {
            return false;
        }
    }

    private boolean validaSubmissao(Submissao submissao) {
        return submissao.valida();
    }

    public List<Submissao> getListaSubmissoes() {
        return this.m_listaSubmissoes;
    }

    public void setListaSubmissoes(List<Submissao> ls) {
        this.m_listaSubmissoes = ls;
    }

    public CP getCP() {
        return this.m_cp;
    }

    public List<Topico> getTopicos() {
        return m_listaTopicos;
    }

    public void setTopicos(List<Topico> lt) {
        m_listaTopicos = lt;
    }

    // adicionada na iteração 2
    public Topico novoTopico() {
        return new Topico();
    }

    // adicionada na iteração 2
    public boolean addTopico(Topico t) {
        return this.m_listaTopicos.add(t);
    }

    // adicionada na iteração 2
    public boolean validaTopico(Topico t) {
        if (t.valida() && validaGlobalTopico(t)) {
            return true;
        } else {
            return false;
        }
    }

    // adicionada na iteração 2
    private boolean validaGlobalTopico(Topico t) {
        return true;
    }

    /**
     * @return the website
     */
    public String getWebsite() {
        return m_website;
    }

    /**
     * @param website the website to set
     */
    public void setWebsite(String website) {
        this.m_website = website;
    }

    /**
     * @return the m_host
     */
    public String getHost() {
        return m_host;
    }

    /**
     * @param m_host the m_host to set
     */
    public void setHost(String m_host) {
        this.m_host = m_host;
    }

    /**
     * @param m_listaOrganizadores the m_listaOrganizadores to set
     */
    public void setM_listaOrganizadores(List<Organizador> m_listaOrganizadores) {
        this.m_listaOrganizadores = m_listaOrganizadores;
    }

    public boolean setRegistado() {
        if(m_state.setRegistado()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean setTopicosCriados() {
        if(m_state.setTopicosCriados()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setValoresRegistoDefinidos() {
        if(m_state.setValoresRegistoDefindo()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setCPDefinida() {
        if(m_state.setCPDefinida()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setProntoADistribuir() {
        if(m_state.setProntoADistribuir()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setDistribuido() {
        if(m_state.setDistribuido()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setRevisto() {
        if(m_state.setRevisto()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setDecidido() {
        if(m_state.setDecidido()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setNotificado() {
        if(m_state.setNotificado()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setCameraReady() {
        if(m_state.setCameraReady()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setRegistadosNoEvento() {
        if(m_state.setRegistadosNoEvento()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setTerminado() {
        if(m_state.setTerminado()) {
            return true;
        } else {
            return false;
        }
    }
}
