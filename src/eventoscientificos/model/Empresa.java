package eventoscientificos.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;

/**
 *
 * @author Nuno Silva
 * @author dnamorim
 */
public class Empresa {
    /**
     * Registo de Eventos da Empresa
     */
    private RegistoEventos m_registoEventos;
    
    /**
     * Registo de Utilizadores da Empresa
     */
    private RegistoUtilizadores m_registoUtilizadores; 
    
    private List<Class<? extends FormulaPagamento>> lstFormulas;
    
    private List<Class<? extends MetodoPagamento>> lstMetodosPagamento;
    
    private List<Class<? extends MecanismoDistribuicao>> lstMecanismosDistribuicao;
    
    private List<Class<? extends MecanismoDecisao>> lstMecanismosDecisao;

    /**
     * Constrói uma nova instãncia de Empresa
     */
    public Empresa() {
        this.m_registoEventos = new RegistoEventos();
        this.m_registoUtilizadores = new RegistoUtilizadores();
        
        this.lstFormulas = new ArrayList<Class<? extends FormulaPagamento>>();
        lstFormulas.add(FormulaPagamento1.class);
        lstFormulas.add(FormulaPagamento2.class);
        
        this.lstMetodosPagamento = new ArrayList<Class<? extends MetodoPagamento>>();
        lstMetodosPagamento.add(CanadaExpressPagamento.class);
        lstMetodosPagamento.add(VisaoLightPagamento.class);
        
        this.lstMecanismosDistribuicao = new ArrayList<Class<? extends MecanismoDistribuicao>>();
        lstMecanismosDistribuicao.add(Mecanismo1.class);
        
        this.lstMecanismosDecisao = new ArrayList<Class<? extends MecanismoDecisao>>();
        lstMecanismosDecisao.add(MecanismoPesoMaior.class);
        
        
    }

    public RegistoEventos getRegistoEventos() {
        return m_registoEventos;
    }
    
    public RegistoUtilizadores getRegistoUtilizadores() {
        return m_registoUtilizadores;
    }
    
    public List<Class<? extends FormulaPagamento>> getFormulasPagamento() {
        return this.lstFormulas;
    }
    
    public List<Class<? extends MetodoPagamento>> getMetodosPagamento() {
        return this.lstMetodosPagamento;
    }
    
    public List<Class<? extends MecanismoDistribuicao>> getMecanismosDistribuicao() {
        return this.lstMecanismosDistribuicao;
    }
    
      public List<Class<? extends MecanismoDecisao>> getMecanismosDecisao() {
        return this.lstMecanismosDecisao;
    }
    
    public void saveData() throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("data.bin"));
        out.writeObject(this.m_registoEventos);
        out.writeObject(this.m_registoUtilizadores);
        out.close();
    }
    
    public void loadData() throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("data.bin"));
        this.m_registoEventos = (RegistoEventos) in.readObject();
        this.m_registoUtilizadores = (RegistoUtilizadores) in.readObject();
        in.close();
    }
    
    public void fillInData() {
        int max_users = 50;
        int max_organizadores = 5;
        int max_revisores = 25;
        int max_submissoes = 10;
        
        for(int users=0;users<max_users;users++)
        {
            String id = "user" + users;
            String ds = "Utilizador " + users; 
            
            Utilizador u = new Utilizador(id,"12345",ds,id +"@xxx.pt");
            
            this.m_registoUtilizadores.registaUtilizador(u);
        }
        
        Evento e1 = new Evento("ESOFT Workshop", "Workshop sobre Engenharia de Software");
        this.m_registoEventos.registaEvento(e1);
        
        System.out.println( "Organizadores de evento1" );
        for(int organizadores=0;organizadores<max_organizadores;organizadores++)
        {
            Utilizador utl = this.m_registoUtilizadores.getListaUtilizadores().get(organizadores);
            e1.addOrganizador(utl);
            
            System.out.println( utl.toString() );
        }
        CP cp = e1.novaCP();
        for(int revisores=0;revisores<max_revisores;revisores++)
        {
            Utilizador utl = this.m_registoUtilizadores.getListaUtilizadores().get(revisores);
            Revisor r = cp.addMembroCP(utl.getUsername(),utl);
            cp.registaMembroCP(r);
        }
        e1.setCP(cp);
        
        for(int submissoes=0;submissoes<max_submissoes;submissoes++)
        {
            Submissao sub = e1.novaSubmissao();
            Artigo art = sub.novoArtigo();
            
            art.setTitulo("Artigo " + submissoes);
            sub.setArtigo(art);
            e1.addSubmissao(sub);
        }
    }
    
    public void createAdmin() {
        Utilizador adm = m_registoUtilizadores.novoUtilizador();
        adm.setEmail("admin@tocs.com");
        adm.setNome("Administrador");
        adm.setPassword("letmein");
        adm.setUsername("admin");
        adm.setAdmin();
        m_registoUtilizadores.registaUtilizador(adm);
    }
}
