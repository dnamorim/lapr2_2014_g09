/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author dnamorim
 */
public class FormulaPagamento2 implements FormulaPagamento, Serializable {

    private Evento e;
    
    public FormulaPagamento2(Evento e) {
        this.e = e;
    }

    @Override
    public float valorAPagar(List<Submissao> lstSub) {
        int nShort=0, nFull=0, nPoster=0;
        float pShort = e.getPrecoShort(),pFull = e.getPrecoFull(), pPoster =e.getPrecoPoster();
        
        for (Submissao s : lstSub) {
            if(s.getArtigo().getTipo() == TipoArtigo.SHORT) {
                nShort++;
            }
            if(s.getArtigo().getTipo() == TipoArtigo.FULL) {
                nFull++;
            }
            if(s.getArtigo().getTipo() == TipoArtigo.POSTER) {
                nPoster++;
            }
        }
        
        float valor=0;
        
        if(nShort>0 && nPoster>0){
            valor = nFull*pFull + pShort*(nShort - (nFull/2)) + pPoster*(nPoster - (nFull/2));
        } else if(nShort == 0 && nPoster>0) {
            valor = nFull*pFull + pPoster*(nPoster - (nFull/2));
        } else if(nPoster == 0 && nShort>0) {
            valor = nFull*pFull + pShort*(nShort - (nFull/2));
        } 
        
        return valor;
    }

    @Override
    public String descricao() {
        return String.format("Pagamento Total dos Full Paper dependendo o número de Artigos. Pagamento do Número de Short/Poster Papers subtraído ao metade dos Full Papers.");
    }
    
}
