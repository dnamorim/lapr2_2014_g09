/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public class MecanismoPesoMaior implements MecanismoDecisao {

    /**
     *
     */
    public MecanismoPesoMaior() {

    }

    @Override
    public String descricaoMecanismo() {
        return "Mecanismo Peso Maior: Aceita/Rejeita artigos consoante o maior número de recomendações das revisões.";
    }
    
    @Override
    public String nameMecanismo() {
        return "Mecanismo Peso Maior";
    }

    @Override
    public void decidir(ProcessoDecisao pd) {
        for (Submissao s : pd.getEvento().getListaSubmissoes()) {
            int nrRevisoes = s.getArtigo().getRevisoes().size();
            int aceites = 0;
            for (Revisao r : s.getArtigo().getRevisoes()) {
                if(r.getRecomendacao()) {
                    aceites++;
                }
            }
            Decisao d = s.getArtigo().novaDecisao();
            if((aceites/nrRevisoes) >= 0.5f) {
                d.setDecisao(true);
            } else {
                d.setDecisao(false);
            }
            pd.novaDecisao(d);
        }
}

}
