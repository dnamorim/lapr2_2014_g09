/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author iazevedo
 * @author dnamorim
 */
public class Topico implements Serializable {
    
    private String m_strDescricao;
    private String m_strCodigoACM;

    public Topico(String codigo, String descricao) {
        this.m_strCodigoACM = codigo;
        this.m_strDescricao = descricao;
    }
   
    public Topico() {
        this("","");
    }
           
    public void setDescricao(String strDescricao) {
        this.m_strDescricao = strDescricao;
    }

    public void setCodigoACM(String codigoACM) {
        this.m_strCodigoACM = codigoACM;
    }
    
    public String getDescricao() {
        return this.m_strDescricao;
    }
    
    public String getCodigoACM() {
        return this.m_strCodigoACM;
    }
    
    public boolean valida()
    {
        if((this.m_strCodigoACM.split("\\.").length == 3 || this.m_strCodigoACM.split("\\.").length == 2) && this.m_strDescricao.split("/").length == 2) {
            return true;
        } else {
            return false;
        }
    }
    
    @Override
    public String toString() {
        return String.format("%s - %s", this.m_strCodigoACM, this.m_strDescricao);
    }
    
    @Override
    public boolean equals(Object outroObjecto) {
        if (this == outroObjecto) {
            return true;
        }
        if (outroObjecto == null || this.getClass() != outroObjecto.getClass()) {
            return false;
        }
        Topico t = (Topico) outroObjecto;
        return (this.m_strCodigoACM.equals(t.getCodigoACM()));
    }
}