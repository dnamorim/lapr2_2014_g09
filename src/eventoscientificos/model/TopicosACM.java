/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author dnamorim
 */
public class TopicosACM {
    private static List<Topico> topicosACM; 
    
    static {
        topicosACM = new ArrayList<Topico>();
    }
    
    public static void addTopico(Topico t) {
        if(!existeTopico(t)) {
            topicosACM.add(t);
        }
    }
    
    public static Topico novoTopico() {
        return new Topico();
    }
    
    public static List<Topico> getListaTopicosACM() {
        return topicosACM;
    }
    
    public static void readTopicosACM() throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream("acmtopics.bin"));
        topicosACM = (List<Topico>) in.readObject();
        in.close();
    }

    public static void saveTopicosACM() throws IOException {
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("acmtopics.bin"));
        out.writeObject(topicosACM);
        out.close();
    }

    public static boolean isVazio() {
        return (topicosACM.isEmpty());
    }
    
    public static Topico[] obterArrayTopicos() {
        Topico[] items = new Topico[topicosACM.size()];

        for (int i = 0; i < items.length; i++) {
            items[i] = topicosACM.get(i);
        }

        return items;
    }
    
    public static boolean valida(String codigo) {
       for (ListIterator<Topico> it = topicosACM.listIterator(); it.hasNext();) {
            if(it.next().getCodigoACM().equals(codigo)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean existeTopico(Topico t) {
       for (ListIterator<Topico> it = topicosACM.listIterator(); it.hasNext();) {
            if(it.next().equals(t)) {
                return true;
            }
        }
        return false;
    }
    
    public static Topico getTopicoByCodigoACM(String codigo) {
       for (ListIterator<Topico> it = topicosACM.listIterator(); it.hasNext();) {
            Topico t =(Topico) it.next();
           if(t.getCodigoACM().equals(codigo)) {
                return t;
            }
        }
        return null;
    }
}
    
