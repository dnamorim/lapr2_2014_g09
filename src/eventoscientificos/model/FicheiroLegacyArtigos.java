/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tiago
 */
public class FicheiroLegacyArtigos {

    private File ficheiro_artigos;
    private HashMap hg_legacy;
    private HashMap<Submissao, String> hg_submissao = new HashMap<>();

    public FicheiroLegacyArtigos() {

    }

    public void setFileEventos(File ficheiro) {
        this.ficheiro_artigos = ficheiro;
    }

    public File getFileEventos() {
        return this.ficheiro_artigos;
    }

    /**
     * @return the hg_legacy
     */
    public HashMap getHg_legacy() {
        return hg_legacy;
    }

    /**
     * @param hg_legacy the hg_legacy to set
     */
    public void setHg_legacy(HashMap hg_legacy) {
        this.hg_legacy = hg_legacy;
    }

    public List<String> lerFile() {
        List<String> lista_string_artigos = new ArrayList<>();
        BufferedReader ler = null;
        try {
            ler = new BufferedReader(new FileReader(ficheiro_artigos));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        String linha;
        try {
            while ((linha = ler.readLine()) != null) {
                String[] temp = linha.split(";");
                if (!temp[0].equals("ConferenceID")) {
                    lista_string_artigos.add(linha);
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ler.close();
        } catch (IOException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista_string_artigos;
    }

    public void criarListaArtigos(List<String> ls) {
        for (String linha : ls) {
            String temp[] = linha.split(";");
            Submissao sub = new Submissao();
            sub.getState().setCriado();
            Artigo art = new Artigo();
            art.setTipo(verificarTipoArtigo(temp[2]));
            art.setTitulo(temp[3]);
            if (temp.length >= 7) {
                for (int i = 4; i < temp.length; i = i + 3) {
                    Autor aut = new Autor();
                    aut.setNome(temp[i]);
                    aut.setAfiliacao(temp[i + 1]);
                    aut.setEMail(temp[i + 2]);
                    art.addAutor(aut);
                }
                art.setAutorCorrespondente(art.getListaAutores().get(0));
                art.setResumo(art.getTitulo());
                List<Topico> lt;
                lt = new ArrayList<>();
                lt.add(TopicosACM.getListaTopicosACM().get(0));
                
                art.setListaTopicos(lt);

            }
            sub.setArtigo(art);
            hg_submissao.put(sub, temp[1]);
            Set set = hg_legacy.entrySet();
            Iterator iter = set.iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                if (entry.getValue().equals(temp[0])) {
                    Evento evento = (Evento) entry.getKey();
                    evento.addSubmissao(sub);
                }
            }
        }
    }

    /**
     * @return the hg_submissao
     */
    public HashMap<Submissao, String> getHg_submissao() {
        return hg_submissao;
    }

    /**
     * @param hg_submissao the hg_submissao to set
     */
    public void setHg_submissao(HashMap<Submissao, String> hg_submissao) {
        this.hg_submissao = hg_submissao;
    }

    private TipoArtigo verificarTipoArtigo(String temp) {
        if (temp.trim().equalsIgnoreCase("Full Paper")) {
            return TipoArtigo.FULL;
        } else {
            if (temp.trim().equalsIgnoreCase("Poster")) {
                return TipoArtigo.POSTER;
            } else {
                if (temp.trim().equalsIgnoreCase("Short Paper")) {
                    return TipoArtigo.SHORT;
                } else {
                    return null;
                }
            }
        }
    }
}
