/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author dnamorim
 */
public class Decisao implements Serializable {
    
    private boolean decisao;
    private Artigo art;
    private Notificacao notificacao; 
    
    public Decisao(Artigo art) {
        this.decisao = false;
        this.art = art;
    }
    
    public void setDecisao(boolean decisao) {
        this.decisao = decisao;
    }
    
    public String getDescricao() {
        return (this.decisao == true) ? "Aceite" : "Rejeitado";
    }
    
    public Artigo getArtigo() {
        return this.art;
    }
    
    public boolean getDecisao() {
        return decisao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Decisao other = (Decisao) obj;
        if (this.decisao != other.decisao) {
            return false;
        }
        return true;
    }
    
}
