/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

/**
 *
 * @author Duarte Nuno Amorim <1130674@isep.ipp.pt>
 */
public interface MecanismoDecisao {
    
    public void decidir(ProcessoDecisao pd);
    public String nameMecanismo();
    public String descricaoMecanismo();

}
