/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.model.Evento;
import eventoscientificos.model.EventoState.EventoCameraReadyState;
import eventoscientificos.model.Utilizador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author i130672
 */
public class RegistoEventos implements Serializable {

    private List<Evento> m_listaEventos;

    public RegistoEventos() {
        m_listaEventos = new ArrayList<Evento>();
    }
    
    public Evento novoEvento() {
        return new Evento();
    }

    public boolean registaEvento(Evento e) {
        if (e.setRegistado() && valida(e)) {
            return addEvento(e);
        } else {
            return false;
        }
    }

    private boolean valida(Evento e) {
        return true;
    }

    private boolean addEvento(Evento e) {
        return m_listaEventos.add(e);
    }
    
    public List<Evento> getEventosOrganizador(Utilizador u) {
        List<Evento> leOrganizador = new ArrayList<Evento>();

        if (u != null) {
            for (Iterator<Evento> it = m_listaEventos.listIterator(); it.hasNext();) {
                Evento e = it.next();
                List<Organizador> lOrg = e.getListaOrganizadores();

                boolean bRet = false;
                for (Organizador org : lOrg) {
                    if (org.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leOrganizador.add(e);
                }
            }
        }
        return leOrganizador;
    }
    
    public List<Evento> getEventosRevisor(Utilizador u) {
        List<Evento> leRevisor = new ArrayList<Evento>();

        if (u != null) {
            for (Iterator<Evento> it = m_listaEventos.listIterator(); it.hasNext();) {
                Evento e = it.next();
                List<Revisor> lOrg = e.getCP().getListaRevisores();

                boolean bRet = false;
                for (Revisor rev : lOrg) {
                    if (rev.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leRevisor.add(e);
                }
            }
        }
        return leRevisor;
    }

    public List<Evento> getListaEventosPodeSubmeter() {
        List<Evento> le = new ArrayList<Evento>();

        for (Evento e : m_listaEventos) {
            if (e.aceitaSubmissoes()) {
                le.add(e);
            }
        }

        return le;
    }
    
    public List<Evento> getListaEventosPodeSubmeterFinal(String strID) {
        List<Evento> le = new ArrayList<Evento>();

        for (Evento e : m_listaEventos) {
            if (e.aceitaSubmissoesFinais(strID)) {
                le.add(e);
            }
        }

        return le;
    }
    
    public List<Evento> getListaEventosPodeRegistar(Utilizador u) {
        List<Evento> leRegisto = new ArrayList<Evento>();

        if (u != null) {
            for (Iterator<Evento> it = m_listaEventos.listIterator(); it.hasNext();) {
                Evento e = it.next();
                if(e.podeRegistar(u) && (e.getState() instanceof EventoCameraReadyState)) {
                    leRegisto.add(e);
                }
            }
        }

        return leRegisto;
    }
    
    public List<Evento> getListaEventos(){
        return this.m_listaEventos;
    }

}
