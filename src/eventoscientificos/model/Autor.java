/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import utils.*;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 * @author dnamorim
 */
public class Autor implements Serializable {
    private Utilizador m_Utilizador;
    private String m_strNome;
    private String m_strAfiliacao;
    private String m_strEMail;
    
    public Autor(Utilizador u, String afil) {
        this.setNome(u.getNome());
        this.setAfiliacao(afil);
        this.setUtilizador(u);
        this.setEMail(u.getEmail());
    }
    
    public Autor(String nome, String afiliacao, String email) {
        this.setNome(nome);
        this.setAfiliacao(afiliacao);
        this.setEMail(email);
        this.setUtilizador(null);
    }
    
    public Autor() {
        this("", "", "");   
    }
    
    public void setNome(String strNome)
    {
        this.m_strNome = strNome;
    }
    
    public String getNome() {
        return this.m_strNome;
    }
    
    public void setAfiliacao(String strAfiliacao)
    {
        this.m_strAfiliacao = strAfiliacao;
    }
    
    public String getAfiliacao() {
        return this.m_strAfiliacao;
    }
    
    public void setEMail(String strEMail)
    {
        this.m_strEMail = strEMail;
    }
    
    public String getEMail() {
        return this.m_strEMail;
    }
    
    public void setUtilizador(Utilizador utilizador)
    {
        this.m_Utilizador = utilizador;
    }
    
    public Utilizador getUtilizador() {
        return this.m_Utilizador;
    }
    
    public boolean valida()
    {
        return (Utils.validaEmail(m_strEMail));
    }

    boolean podeSerCorrespondente() 
    {
        return (m_Utilizador != null);
    }
    
    @Override
    public String toString()
    {
        return String.format("%s (%s) - %s", this.m_strNome, this.m_strAfiliacao, this.m_strEMail);
    }
    
    /**
     * 
     * @param obj
     * @return 
     * 
     * Um Autor é identificado pelo seu endereço de e-mail que deve ser único no artigo, 
     * mas não tem que corresponder a um Utilizador.
     */
    @Override
    public boolean equals(Object obj){
        if(this == obj)
            return true;
        else
            if(obj instanceof Autor) {
                Autor aux = (Autor) obj;
                return this.m_strEMail.equals(aux.m_strEMail);
            } else
                return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.m_Utilizador != null ? this.m_Utilizador.hashCode() : 0);
        hash = 41 * hash + (this.m_strEMail != null ? this.m_strEMail.hashCode() : 0);
        return hash;
    }
}
