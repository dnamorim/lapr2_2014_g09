/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author i130672
 */
public class FicheiroEventoXML implements FicheiroEvento {

    File ficheiro;
    List<Evento> lista_eventos = new ArrayList<>();

    @Override
    public File getFile() {
        return this.ficheiro;
    }

    @Override
    public void setFile(File ficheiro) {
        this.ficheiro = ficheiro;
    }

    @Override
    public List<String> lerFile() {
        List<String> eventos = new ArrayList<>();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        org.w3c.dom.Document dom = null;
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FicheiroEventoXML.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            dom = db.parse(ficheiro);
        } catch (SAXException ex) {
            Logger.getLogger(FicheiroEventoXML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FicheiroEventoXML.class.getName()).log(Level.SEVERE, null, ex);
        }
        NodeList nl = dom.getElementsByTagName("event");
        eventos = extractInf(nl);
        return eventos;
    }

    private List<String> extractInf(NodeList nl) {
        List<String> lista_eventos = new ArrayList<>();
        for (int i = 0; i < nl.getLength(); i++) {
            String linha;
            Element el = (Element) nl.item(i);
            String name = getTextValue(el, "name");
            String host = getTextValue(el, "host");
            String city = getTextValue(el, "city");
            String country = getTextValue(el, "country");
            String begin_date = getTextValue(el, "begin_date");
            String end_date = getTextValue(el, "end_date");
            String web_page = getTextValue(el, "web_page");
            NodeList nl_organizers = el.getElementsByTagName("organizer");
            List<String> lista_organizers = getInfoOrganizadores(nl_organizers);
            linha = name + ";" + host + ";" + city + ";" + country + ";" + begin_date + ";" + end_date + ";" + web_page;
            for (String info : lista_organizers) {
                linha = linha + ";" + info;
            }
            lista_eventos.add(linha);
        }
        return lista_eventos;
    }

    @Override
    public List<Evento> criarListaEventos(List<String> lista) {
        for (String string_evento : lista) {
            Evento evento = new Evento();
            evento.getState().setCriadoXML();
            String[] temp = string_evento.split(";");
            if (temp.length >= 1) {
                if (!temp[0].equals("")) {
                    evento.setTitulo(temp[0]);
                } else {
                    evento.setTitulo(null);

                }
            } else {
                evento.setTitulo(null);
            }
            if (temp.length >= 2) {
                if (!temp[1].equals("")) {
                    evento.setHost(temp[1]);
                } else {
                    evento.setHost(null);
                }
            } else {
                evento.setHost(null);
            }
            if (temp.length >= 3) {
                if (!temp[2].equals("")) {
                    evento.getLocal().setCidade(temp[2]);
                } else {
                    evento.getLocal().setCidade(null);
                }
            } else {
                evento.getLocal().setCidade(null);
            }
            if (temp.length >= 4) {
                if (!temp[3].equals("")) {
                    evento.getLocal().setPais(temp[3]);
                } else {
                    evento.getLocal().setPais(null);
                }
            } else {
                evento.getLocal().setPais(null);
            }

            if (temp.length >= 5) {
                if (!temp[4].equals("")) {
                    String[] temp_data_inicio = temp[4].split(",");
                    String[] mes_dia = temp_data_inicio[1].trim().split(" ");
                    int dia = Integer.parseInt(mes_dia[1].trim());
                    int mes = verificarMes(mes_dia[0].trim());
                    int ano = Integer.parseInt(temp_data_inicio[2].trim());
                    Calendar dataInicio = new GregorianCalendar();
                    dataInicio.set(Calendar.YEAR, ano);
                    dataInicio.set(Calendar.MONTH, mes);
                    dataInicio.set(Calendar.DAY_OF_MONTH, dia);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    evento.setDataInicio(dataInicio);
                } else {
                    evento.setDataInicio(null);
                }
            } else {
                evento.setDataInicio(null);
            }

            if (temp.length >= 6) {
                if (!temp[5].equals("")) {
                    String[] temp_data_fim = temp[5].split(",");
                    String[] mes_dia_fim = temp_data_fim[1].trim().split(" ");
                    int dia_fim = Integer.parseInt(mes_dia_fim[1].trim());
                    int mes_fim = verificarMes(mes_dia_fim[0].trim());
                    int ano_fim = Integer.parseInt(temp_data_fim[2].trim());
                    Calendar dataFim = new GregorianCalendar();
                    dataFim.set(Calendar.YEAR, ano_fim);
                    dataFim.set(Calendar.MONTH, mes_fim);
                    dataFim.set(Calendar.DAY_OF_MONTH, dia_fim);
                    SimpleDateFormat sdd = new SimpleDateFormat("dd/MM/yyyy");
                    evento.setDataFim(dataFim);
                } else {
                    evento.setDataFim(null);
                }
            } else {
                evento.setDataFim(null);

            }

            if (temp.length >= 7) {
                if (!temp[6].equals("")) {
                    evento.setWebsite(temp[6]);
                } else {
                    evento.setWebsite(null);
                }
            } else {
                evento.setWebsite(null);
            }

            if (temp.length >= 9) {
                for (int i = 7; i < temp.length; i = i + 2) {
                    Utilizador util = new Utilizador();
                    util.setNome(temp[i]);
                    util.setEmail(temp[i + 1]);
                    util.setUsername(temp[i + 1]);
                    util.setPassword("letmein");
                    evento.addOrganizador(util);

                }
            }
            lista_eventos.add(evento);
        }
        return lista_eventos;
    }

    private String getTextValue(Element el, String tag) {
        String textValue = null;
        NodeList nl = el.getElementsByTagName(tag);
        if (nl != null && nl.getLength() > 0) {
            Element ele = (Element) nl.item(0);
            try {
                textValue = ele.getFirstChild().getNodeValue();
            } catch (NullPointerException e) {
                textValue = "";
            }
        }

        return textValue;
    }

    private List<String> getInfoOrganizadores(NodeList nl) {
        List<String> lista_organizers = new ArrayList<>();
        if (nl != null && nl.getLength() > 0) {
            for (int i = 0; i < nl.getLength(); i++) {
                Element ele = (Element) nl.item(i);
                String org_name = getTextValue(ele, "org_name");
                String email = getTextValue(ele, "email");
                lista_organizers.add(org_name);
                lista_organizers.add(email);

            }

        }
        return lista_organizers;
    }

    private int verificarMes(String mes) {
        int i_mes = 0;
        String[] meses_do_ano = {"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
        for (int i = 0; i < meses_do_ano.length; i++) {
            if (mes.equalsIgnoreCase(meses_do_ano[i])) {
                i_mes = i;
                break;
            }
        }
        return i_mes;
    }
}
