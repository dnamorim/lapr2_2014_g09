/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Tiago
 */
public class FicheiroLegacyEventos {

    private File ficheiro_evento, ficheiro_revisores;
    private HashMap hg_legacy;
    private HashMap hg_revisores = new HashMap<>();

    ;

    public FicheiroLegacyEventos() {

    }

    public void setFileEventos(File ficheiro) {
        this.ficheiro_evento = ficheiro;
    }

    public File getFileEventos() {
        return this.ficheiro_evento;
    }

    public void setFileRevisores(File ficheiro, Empresa m_empresa) {
        this.ficheiro_revisores = ficheiro;
        definirCP(m_empresa);
    }

    public File getFileRevisores() {
        return this.ficheiro_revisores;
    }

    /**
     * @return the hg_legacy
     */
    public HashMap getHg_legacy() {
        return hg_legacy;
    }

    /**
     * @param hg_legacy the hg_legacy to set
     */
    public void setHg_legacy(HashMap hg_legacy) {
        this.hg_legacy = hg_legacy;
    }

    /**
     * @return the hg_revisores
     */
    public HashMap getHg_revisores() {
        return hg_revisores;
    }

    /**
     * @param hg_revisores the hg_revisores to set
     */
    public void setHg_revisores(HashMap hg_revisores) {
        this.hg_revisores = hg_revisores;
    }

    public List<String> lerFile() {
        List<String> lista_string_eventos = new ArrayList<>();
        BufferedReader ler = null;
        try {
            ler = new BufferedReader(new FileReader(ficheiro_evento));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        String linha;
        try {
            while ((linha = ler.readLine()) != null) {
                String[] temp = linha.split(";");
                if (!temp[0].equals("Year")) {
                    lista_string_eventos.add(linha);
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ler.close();
        } catch (IOException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista_string_eventos;
    }

    public void criarListaEventos(List<String> ls) throws IOException {
        List<Evento> lista_eventos = new ArrayList<>();
        Formatter log = new Formatter(new File("log_eventos.txt"));
        log.format("Documento de erros da migração de dados legacy%n");
        for (String linha : ls) {
            Evento evento = new Evento();
            evento.getState().setCriado();
            String[] temp = linha.split(";");
            if (temp.length >= 2) {
                if (!temp[1].isEmpty()) {
                    evento.setTitulo(temp[1]);
                } else {
                    log.format("O evento de ID:" + temp[2] + " foi importado sem título.%n");
                }
            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem título.%n");
            }

            if (temp.length >= 4) {
                if (!temp[3].isEmpty()) {
                    evento.setHost(temp[3]);
                } else {
                    log.format("O evento de ID:" + temp[2] + " foi importado sem host.%n");
                }
            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem host.%n");
            }

            if (temp.length >= 5) {
                if (!temp[4].isEmpty()) {
                    evento.getLocal().setCidade(temp[4]);
                } else {
                    log.format("O evento de ID:" + temp[2] + " foi importado sem cidade.%n");
                }
            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem cidade.%n");
            }

            if (temp.length >= 6) {
                if (!temp[5].isEmpty()) {
                    evento.getLocal().setPais(temp[5]);
                } else {
                    log.format("O evento de ID:" + temp[2] + " foi importado sem país.%n");
                }
            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem país.%n");
            }

            if (temp.length >= 7) {
                if (!temp[6].isEmpty()) {
                    evento.setDataLimiteSubmissao(obterData(temp[6]));
                } else {
                    log.format("O evento de ID:" + temp[2] + " foi importado sem data limite de submissão.%n");
                }

            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem data limite de submissão.%n");
            }

            if (temp.length >= 8) {
                if (!temp[7].isEmpty()) {
                    evento.setDataLimiteRevisao(obterData(temp[7]));
                } else {
                    log.format("O evento de ID:" + temp[2] + " foi importado sem data limite de revisão.%n");
                }
            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem data limite de revisão.%n");
            }

            if (temp.length >= 9) {
                if (!temp[8].isEmpty()) {
                    evento.setDataLimiteSubmissaoFinal(obterData(temp[8]));
                } else {
                    log.format("O evento de ID:" + temp[2] + " foi importado sem data limite de submissão final.%n");
                }
            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem data limite de submissão final.%n");
            }

            if (temp.length >= 10) {
                if (!temp[9].isEmpty()) {
                    evento.setDataLimiteRegisto(obterData(temp[9]));
                } else {
                    log.format("O evento de ID:" + temp[2] + " foi importado sem data limite de registo.%n");
                }
            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem data limite de registo.%n");
            }

            if (temp.length >= 11) {
                if (!temp[10].isEmpty()) {
                    evento.setDataInicio(obterData(temp[10]));
                } else {
                    log.format("O evento de ID:" + temp[2] + " foi importado sem data de início.%n");
                }
            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem data de início.%n");
            }

            if (temp.length >= 12) {
                if (!temp[11].isEmpty()) {
                    int n_dias = Integer.parseInt(temp[11].trim());
                    evento.setDataFim(obterDataFim(temp[10], n_dias));
                } else {
                    log.format("O evento de ID:" + temp[2] + " foi importado sem data de fim.%n");
                }
            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem data de fim.%n");
            }
            if (temp.length >= 13) {
                if (!temp[12].isEmpty()) {
                    evento.setWebsite(temp[12]);
                } else {
                    log.format("O evento de ID:" + temp[2] + " foi importado sem website.%n");
                }
            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem website.%n");
            }

            if (temp.length >= 15) {
                for (int i = 13; i < temp.length; i = i + 2) {
                    Utilizador util = new Utilizador();
                    util.setNome(temp[i]);
                    util.setEmail(temp[i + 1]);
                    util.setUsername(temp[i + 1]);
                    util.setPassword("letmein");
                    evento.addOrganizador(util);
                }
            } else {
                log.format("O evento de ID:" + temp[2] + " foi importado sem organizadores.%n%n");
            }
            hg_legacy.put(evento, temp[2]);
        }
        log.close();
    }

    public Calendar obterData(String string_data) {
        String[] temp_data_inicio = string_data.split(",");
        String derp = temp_data_inicio[1].trim();
        String[] mes_dia = derp.split(" ");
        int dia = Integer.parseInt(mes_dia[1]);
        int mes = verificarMes(mes_dia[0]);
        int ano = Integer.parseInt(temp_data_inicio[2].trim());
        Calendar data = new GregorianCalendar();
        data.set(Calendar.YEAR, ano);
        data.set(Calendar.MONTH, mes);
        data.set(Calendar.DAY_OF_MONTH, dia);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return data;
    }

    public Calendar obterDataFim(String dataInicio, int n_dias) {
        String[] temp_data_inicio = dataInicio.split(",");
        String derp = temp_data_inicio[1].trim();
        String[] mes_dia = derp.split(" ");
        int dia = Integer.parseInt(mes_dia[1]);
        int mes = verificarMes(mes_dia[0]);
        int ano = Integer.parseInt(temp_data_inicio[2].trim());
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar dataFim = new GregorianCalendar();
        dataFim.set(Calendar.YEAR, ano);
        dataFim.set(Calendar.MONTH, mes);
        dataFim.set(Calendar.DAY_OF_MONTH, dia);
        dataFim.add(Calendar.DAY_OF_MONTH, n_dias);
        return dataFim;

    }

    private int verificarMes(String mes) {
        int i_mes = 0;
        String[] meses_do_ano = {"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
        for (int i = 0; i < meses_do_ano.length; i++) {
            if (mes.equalsIgnoreCase(meses_do_ano[i])) {
                i_mes = i;
                break;
            }
        }
        return i_mes;
    }

    private void definirCP(Empresa m_empresa) {
        List<String> lista_string_revisores = lerFileRevisores();
        for (String linha : lista_string_revisores) {
            String[] temp = linha.split(";");
            Set set = hg_legacy.entrySet();
            Iterator iter = set.iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                if (entry.getValue().equals(temp[0])) {
                    Evento evento = (Evento) entry.getKey();
                    Utilizador util = new Utilizador();
                    util.setNome(temp[2]);
                    util.setEmail(temp[2]);
                    util.setUsername(temp[2]);
                    util.setPassword("letmein");
                    Revisor rev = new Revisor(util);
                    evento.getCP().registaMembroCP(rev);
                    hg_revisores.put(rev, temp[1]);

                }
            }
        }
        Iterator<Evento> keySetIterator = hg_legacy.keySet().iterator();
        while (keySetIterator.hasNext()) {
            Evento key = keySetIterator.next();
            if(key.getCP()!=null){
                key.getState().setCPDefinida();
            }
        }
    }

    public List<String> lerFileRevisores() {
        List<String> lista_string_revisores = new ArrayList<>();
        BufferedReader ler = null;
        try {
            ler = new BufferedReader(new FileReader(ficheiro_revisores));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        String linha;
        try {
            while ((linha = ler.readLine()) != null) {
                String[] temp = linha.split(";");
                if (!temp[0].equals("ConferenceID")) {
                    lista_string_revisores.add(linha);
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ler.close();
        } catch (IOException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista_string_revisores;
    }

}
