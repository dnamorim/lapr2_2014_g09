package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 */
public enum TipoArtigo implements Serializable {

    FULL("Full"),
    SHORT("Short"),
    POSTER("Poster");

    private final String tipo;

    /**
     * Construtor da classe que recebe como parâmetro o tipo do Artigo.
     *
     * @param tipo String com o tipo de artigo.
     */
    TipoArtigo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Devolve a descrição do tipo do ficheiro.
     *
     * @return tipo sob forma de String
     */
    @Override
    public String toString() {
        return this.tipo;
    }
}
