/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author dnamorim
 */
public class Notificacao implements Serializable {
    
    private Decisao decisao;
    private boolean notificado;
    
    public Notificacao(Decisao decisao) {
        this.decisao = decisao;
        this.notificado = false;
    }
    
    public void setNotificado(boolean not) {
        this.notificado = not;
    }
    
    public boolean getNotificacao() {
        return this.notificado;
    }
    
    public Decisao getDecisao() {
        return this.decisao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Notificacao other = (Notificacao) obj;
        if (!Objects.equals(this.decisao, other.decisao)) {
            return false;
        }
        if (this.notificado != other.notificado) {
            return false;
        }
        return true;
    }
    
    
            
            
}
