/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 * Adaptado a partir de: http://www.mkyong.com/java/how-to-read-xml-file-in-java-dom-parser/
 * @author dnamorim
 */
public class ImportarTopicosXML {
    
    private File fileXML;
    
    public ImportarTopicosXML(String pathFile) throws IOException, SAXException, ParserConfigurationException {
        
        String areaDesc = "";
        String subareaDesc = "";
        String code = "";
        String sub2areaDesc ="";
        
        TopicosACM topACM = new TopicosACM();
        
        fileXML = new File(pathFile);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fileXML);
            
        doc.getDocumentElement().normalize();
 
        NodeList nList = doc.getElementsByTagName("node");

        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);

            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                code = eElement.getAttribute("id");
                String text = eElement.getAttribute("label");
                String[] codeArr = code.split("\\.");

                if(codeArr.length == 1 && !text.equalsIgnoreCase("")) {
                    areaDesc = text;
                }
                if(codeArr.length == 2 && !text.equalsIgnoreCase("")) {
                    subareaDesc = text;
                    registerTopic(code, areaDesc, subareaDesc);
                }
                if(codeArr.length == 3 && !text.equalsIgnoreCase("")) {
                    sub2areaDesc = text;
                    registerTopic(code, subareaDesc, sub2areaDesc);
                }
            }
        }

    }
    
    private static void registerTopic(String code, String areaDesc, String subareaDesc) {
        //System.out.println(String.format("%s - %s/%s", code, areaDesc, subareaDesc));
        Topico t = TopicosACM.novoTopico();
        t.setCodigoACM(code);
        t.setDescricao(String.format("%s / %s", areaDesc, subareaDesc));
        if(!TopicosACM.getListaTopicosACM().contains(t)) {
            TopicosACM.addTopico(t);
        }
    }
    
}
