package eventoscientificos.model;

import java.io.Serializable;
import pt.ipp.isep.dei.eapli.visaolight.*;
import utils.Utils;

/**
 *
 * @author Nuno Silva
 */
public final class Utilizador implements Serializable {

    private String m_strNome;
    private String m_strUsername;
    private String m_strPassword;
    private String m_strEmail;
    private boolean admin;

    public Utilizador() {
    }

    public Utilizador(String username, String pwd, String nome, String email) {
        this.setUsername(username);
        this.setPassword(pwd);
        this.setNome(nome);
        this.setEmail(email);
        admin=false;
    }

        /**
     * @return the m_strNome
     */
    public String getNome() {
        return m_strNome;
    }

    /**
     * @param m_strNome the m_strNome to set
     */
    public void setNome(String m_strNome) {
        this.m_strNome = m_strNome;
    }

    /**
     * @return the m_strUsername
     */
    public String getUsername() {
        return m_strUsername;
    }

    /**
     * @param m_strUsername the m_strUsername to set
     */
    public void setUsername(String m_strUsername) {
        this.m_strUsername = m_strUsername;
    }

    /**
     * @return the m_strPassword
     */
    public String getPassword() {
        return m_strPassword;
    }

    /**
     * @param m_strPassword the m_strPassword to set
     */
    public void setPassword(String m_strPassword) {
        this.m_strPassword = m_strPassword;
    }

    /**
     * @return the m_strEmail
     */
    public String getEmail() {
        return m_strEmail;
    }

    /**
     * @param m_strEmail the m_strEmail to set
     */
    public void setEmail(String m_strEmail) {
        this.m_strEmail = m_strEmail;
    }
    
    public boolean getAdmin() {
        return admin;
    }
    
    public void setAdmin() {
        this.admin=true;
    }
    
    public boolean isAdmin() {
        return admin;
    }

    
    /**
     *
     * @return
     */
    public boolean valida() {
        return Utils.validaEmail(this.getEmail());
    }

    /**
     *
     * @param u
     * @return
     *
     * método alterado na iteração 2
     */
    public boolean mesmoQueUtilizador(Utilizador u) {
        if (getUsername().equalsIgnoreCase(u.getUsername()) || getEmail().equals(u.getEmail())) {
            return true;
        } else {
            return false;
        }
        
    }

    @Override
    public String toString() {
        return String.format("Utilizador:%nNome: %s Username: %s%nE-Mail: %s Password: %s", this.m_strNome, this.m_strUsername, this.m_strEmail, this.m_strPassword);
    }

    /**
     *
     * @param obj
     * @return
     *
     * a forma de identificar um utilizador é através do seu endereço de e-mail
     * ou username
     */
    @Override
    public boolean equals(Object outroObjecto) {
        if (this == outroObjecto) {
            return true;
        }
        if (outroObjecto == null || this.getClass() != outroObjecto.getClass()) {
            return false;
        }
        Utilizador u = (Utilizador) outroObjecto;
        return (this.m_strUsername.equalsIgnoreCase(u.m_strUsername) || this.m_strEmail.equals(u.m_strEmail));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + (this.getUsername() != null ? this.getUsername().hashCode() : 0);
        return hash;
    }   
}
