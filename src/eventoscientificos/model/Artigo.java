package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 */
public class Artigo implements Serializable {

    private String m_strTitulo;
    private String m_strResumo;
    private List<Autor> m_listaAutores;
    private Autor m_autorCorrespondente;
    private String m_pathFileArtigo;
    private List<Topico> m_listaTopicos;
    private TipoArtigo tipo;
    private Distribuicao distribuicao;
    private Decisao decisao;
    
    //A Desactivar
    private List<Revisao> m_listaRevisoes=new ArrayList<>();
    private List<Revisor> m_listaRevisores;
    

    /**
     * Construtor da classe.
     */
    public Artigo() {
        m_listaAutores = new ArrayList<Autor>();
        m_listaTopicos = new ArrayList<Topico>();
    }

    /**
     * Modifica o titulo do artigo.
     *
     * @param strTitulo titulo
     */
    public void setTitulo(String strTitulo) {
        this.m_strTitulo = strTitulo;
    }
    
    public Decisao novaDecisao() {
        return new Decisao(this);
    }
    
    public void setDecisao(Decisao d) {
        this.decisao = d;
    }
    
    public Decisao getDecisao() {
        return this.decisao;
    }

    /**
     * Devolve o titulo do artigo.
     *
     * @return titulo
     */
    public String getTitulo() {
        return this.m_strTitulo;
    }

    /**
     * Modifica o resumo do artigo.
     *
     * @param strResumo resumo
     */
    public void setResumo(String strResumo) {
        this.m_strResumo = strResumo;
    }

    /**
     * Devolve o resumo do artigo.
     *
     * @return resumo
     */
    public String getResumo() {
        return this.m_strResumo;
    }

    /**
     * Modifica o tipo do artigo.
     *
     * @param tipo tipo do artigo
     */
    public void setTipo(TipoArtigo tipo) {
        this.tipo = tipo;
    }

    /**
     * Devolve o tipo do artigo.
     *
     * @return tipo do artigo
     */
    public TipoArtigo getTipo() {
        return tipo;
    }

    /**
     * Devolve a lista de revisões.
     *
     * @return lista revisões
     */
    public List<Revisao> getRevisoes() {
        return m_listaRevisoes;
    }
    
    public Revisao novaRevisao(Revisor r) {
        return new Revisao(this, r);
    }

    public void addRevisao(Revisao r) {
        m_listaRevisoes.add(r);
    }
    
    
    /**
     * Método que define uma lista de revisões do artigo
     *
     * @param novaLista nova lista de revisoes
     */
    public void setRevisoes(List<Revisao> novaLista) {
        m_listaRevisoes.addAll(novaLista);
    }

    /**
     * Método que mostra a lista de revisores do artigo
     *
     * @return Lista de revisores
     */
    public List<Revisor> getRevisores() {
        return m_listaRevisores;
    }
    
    public void setDistribuicao(Distribuicao d) {
        this.distribuicao = d;
    }
    
    public Distribuicao getDistribuicao() {
        return this.distribuicao;
    }

    /**
     * Método que define uma lista de revisores do artigo
     *
     * @param novaListaRevisores Nova lista de revisores do artigo
     */
    public void setRevisores(List<Revisor> novaListaRevisores) {
        m_listaRevisores = novaListaRevisores;
    }

    /**
     * Cria um novo autor recebendo como parâmetro o nome, afiliação e e-mail do
     * autor.
     *
     * @param strNome nome do autor
     * @param strAfiliacao afiliação do autor
     * @param strEmail e-mail do autor
     * @return autor
     */
    public Autor novoAutor(String strNome, String strAfiliacao, String strEmail) {
        Autor autor = new Autor();

        autor.setNome(strNome);
        autor.setAfiliacao(strAfiliacao);
        autor.setEMail(strEmail);

        return autor;
    }

    /**
     * Cria um novo autor recebendo como parâmetro a afiliação e o utilizador
     *
     * @param strAfiliacao afiliação do autor
     * @param utilizador utilizador
     * @return autor
     */
    public Autor novoAutor(String strAfiliacao, Utilizador utilizador) {
        Autor autor = new Autor(utilizador, strAfiliacao);

        return autor;
    }

    /**
     * Adiciona autores à lista de autores.
     *
     * @param autor autor do artigo
     * @return lista de autores
     */
    public boolean addAutor(Autor autor) {
        if (validaAutor(autor)) {
            return m_listaAutores.add(autor);
        } else {
            return false;
        }

    }

    /**
     * Validação do autor.
     *
     * @param autor autor do artigo
     * @return resultado da validação
     */
    private boolean validaAutor(Autor autor) {
        return autor.valida();
    }

    /**
     * Devolve a lista de autores correspondentes.
     *
     * @return lista de autores do artigo
     */
    public List<Autor> getPossiveisAutoresCorrespondentes() {
        List<Autor> la = new ArrayList<Autor>();

        for (Autor autor : this.m_listaAutores) {
            if (autor.podeSerCorrespondente()) {
                la.add(autor);
            }
        }
        return la;
    }

    /**
     * Modifica o autor correspondente.
     *
     * @param autor autor do artigo
     */
    public void setAutorCorrespondente(Autor autor) {
        this.m_autorCorrespondente = autor;
    }

    /**
     * Devolve o autor correspondente.
     *
     * @return autor correspondente
     */
    public Autor getAutorCorrespondente() {
        return this.m_autorCorrespondente;
    }

    /**
     * Modifica o ficheiro através do caminho(path file) do mesmo.
     *
     * @param pathArtigo caminho do artigo
     */
    public void setFicheiro(String pathArtigo) {
        this.m_pathFileArtigo = pathArtigo;
    }

    /**
     * Devolve o ficheiro com base no caminho(path file) do mesmo.
     *
     * @return ficheiro
     */
    public String getFicheiro() {
        return this.m_pathFileArtigo;
    }

    /**
     * Modifica a lista de tópicos do artigo
     *
     * @param listaTopicos lista tópicos
     */
    public void setListaTopicos(List<Topico> listaTopicos) {
        this.m_listaTopicos.addAll(listaTopicos);
    }
    

    /**
     * Devolve a lista de tópicos do artigo.
     *
     * @return lista de tópicos
     */
    public List<Topico> getListaTopicos() {
        return this.m_listaTopicos;
    }
    
    public List<Autor> getListaAutores() {
        return this.m_listaAutores;
    }
    
    /**
     * Retorna o Autor associado a um Utiliador passado por parâmetro
     * @param u Utilizador a procurar
     * @return autor associado ao artigo / null se não encontrado
     */
    public Autor getAutor(Utilizador u) {
        if (u != null) {
            for (Iterator<Autor> it = m_listaAutores.listIterator(); it.hasNext();) {
                Autor a = it.next();
                if(a.getUtilizador().equals(u)) {
                    return a;
                }
            }
        }
        return null;
    }

    /**
     * Devolve a informação do artigo recorrendo ao método toString().
     *
     * @return informação
     */
    public String getInfo() {
        return this.toString();
    }

    /**
     * Validação do artigo.
     *
     * @return resultado
     */
    public boolean valida() {
        return true;
    }

    /**
     * Devolve todos os dados do artigo sobre a forma de String.
     *
     * @return String com os dados
     */
    @Override
    public String toString() {
        return String.format("Artigo:%nTítulo: %s%nResumo: %s%nTipo: %s Ficheiro: %s", this.m_strTitulo, this.m_strResumo, this.tipo.toString(), this.m_pathFileArtigo);
    }

    /**
     * Método equals para comparar um objecto como parâmetro com um actual.
     *
     * @param obj objecto para comparar
     * @return resultado da comparação
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Artigo other = (Artigo) obj;
        if (!Objects.equals(this.m_strTitulo, other.m_strTitulo)) {
            return false;
        }
        if (!Objects.equals(this.m_strResumo, other.m_strResumo)) {
            return false;
        }
        if (!Objects.equals(this.m_listaAutores, other.m_listaAutores)) {
            return false;
        }
        if (!Objects.equals(this.m_autorCorrespondente, other.m_autorCorrespondente)) {
            return false;
        }
        if (!Objects.equals(this.m_pathFileArtigo, other.m_pathFileArtigo)) {
            return false;
        }
        if (!Objects.equals(this.m_listaTopicos, other.m_listaTopicos)) {
            return false;
        }
        if (this.tipo != other.tipo) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + (this.m_strTitulo != null ? this.m_strTitulo.hashCode() : 0);
        hash = 19 * hash + (this.m_strResumo != null ? this.m_strResumo.hashCode() : 0);
        return hash;
    }
}
