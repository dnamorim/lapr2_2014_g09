/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Calendar.*;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author i130672
 */
public class FicheiroEventoCSV implements FicheiroEvento {

    File ficheiro;
    List<Evento> lista_eventos = new ArrayList<>();

    @Override
    public File getFile() {
        return this.ficheiro;
    }

    @Override
    public void setFile(File ficheiro) {
        this.ficheiro = ficheiro;
    }

    @Override
    public List<String> lerFile() {
        BufferedReader ler = null;
        List<String> eventos = new ArrayList<>();
        try {
            ler = new BufferedReader(new FileReader(ficheiro));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        String linha;
        try {
            while ((linha = ler.readLine()) != null) {
                String[] temp = linha.split(";");
                if (!temp[0].equals("Year")) {
                    eventos.add(linha);
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ler.close();
        } catch (IOException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
        return eventos;
    }

    @Override
    public List<Evento> criarListaEventos(List<String> lista) {
        String[] temp = lista.get(0).split(";");
        boolean sinalAlt = false;
        for (String verf : temp) {
            if (verf.equals("Submission deadline")) {
                sinalAlt = true;
            }
        }
        if (sinalAlt) {
            lista_eventos = leituraAlt(lista);
        } else {
            lista_eventos = leituraPrinc(lista);
        }
        return lista_eventos;
    }

    private List<Evento> leituraPrinc(List<String> lista) {
        List<Evento> lista_eventos = new ArrayList<>();
        for (String string_evento : lista) {
            Evento evento = new Evento();
            evento.getState().setCriadoCSV();
            String[] temp = string_evento.split(";");
            if (temp.length >= 2) {
                if (!temp[1].isEmpty()) {
                    evento.setTitulo(temp[1]);
                } else {
                    evento.setTitulo(null);

                }
            } else {
                evento.setTitulo(null);
            }

            if (temp.length >= 3) {
                if (!temp[2].isEmpty()) {
                    evento.setHost(temp[2]);
                } else {
                    evento.setHost(null);
                }
            } else {
                evento.setHost(null);
            }
            if (temp.length >= 4) {
                if (!temp[3].isEmpty()) {
                    evento.getLocal().setCidade(temp[3]);
                } else {
                    evento.getLocal().setCidade(null);
                }
            } else {
                evento.getLocal().setCidade(null);
            }

            if (temp.length >= 5) {
                if (!temp[4].isEmpty()) {
                    evento.getLocal().setPais(temp[4]);
                } else {
                    evento.getLocal().setPais(null);
                }
            } else {
                evento.getLocal().setPais(null);
            }
            if (temp.length >= 6) {
                if (!temp[5].isEmpty()) {
                    String[] temp_data_inicio = temp[5].trim().split(",");
                    String derp = temp_data_inicio[1].trim();
                    String[] mes_dia = derp.split(" ");
                    int dia = Integer.parseInt(mes_dia[1]);
                    int mes = verificarMes(mes_dia[0]);
                    int ano = Integer.parseInt(temp_data_inicio[2].trim());
                    Calendar dataInicio = new GregorianCalendar();
                    dataInicio.set(Calendar.YEAR, ano);
                    dataInicio.set(Calendar.MONTH, mes);
                    dataInicio.set(Calendar.DAY_OF_MONTH, dia);
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    evento.setDataInicio(dataInicio);
                    if (temp.length >= 7) {
                        if (!temp[6].isEmpty()) {
                            int n_dias = Integer.parseInt(temp[6].trim());
                            Calendar dataFim = new GregorianCalendar();
                            dataFim.set(Calendar.YEAR, ano);
                            dataFim.set(Calendar.MONTH, mes);
                            dataFim.set(Calendar.DAY_OF_MONTH, dia);
                            dataFim.add(Calendar.DAY_OF_MONTH, n_dias);
                            evento.setDataFim(dataFim);
                        } else {
                            evento.setDataFim(null);
                        }
                    } else {
                        evento.setDataFim(null);
                    }

                } else {
                    evento.setDataInicio(null);
                }
            } else {
                evento.setDataInicio(null);
            }
            if (temp.length >= 8) {
                if (!temp[7].isEmpty()) {
                    evento.setWebsite(temp[7].trim());
                } else {
                    evento.setWebsite(null);
                }
            } else {
                evento.setWebsite(null);
            }

            if (temp.length >= 9) {
                for (int i = 8; i < temp.length; i = i + 2) {
                    Utilizador util = new Utilizador();
                    if (!temp[i].isEmpty()) {
                        util.setNome(temp[i]);
                    } else {
                        util.setNome(null);
                    }
                    if (temp.length >= 10) {
                        if (!temp[i + 1].isEmpty()) {
                            util.setEmail(temp[i + 1]);
                            util.setUsername(temp[i + 1]);
                        } else {
                            util.setEmail(null);
                            util.setUsername(null);
                        }
                    } else {
                        util.setEmail(null);
                        util.setUsername(null);
                    }

                    util.setPassword("letmein");
                    evento.addOrganizador(util);

                }
            }
            lista_eventos.add(evento);
        }
        return lista_eventos;
    }

    private List<Evento> leituraAlt(List<String> lista) {
        List<Evento> lista_eventos = new ArrayList<>();
        for (String string_evento : lista) {
            Evento evento = new Evento();
            String[] temp = string_evento.split(";");
            evento.setTitulo(temp[1]);
            lista_eventos.add(evento);
        }
        return lista_eventos;
    }

    private int verificarMes(String mes) {
        int i_mes = 0;
        String[] meses_do_ano = {"january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"};
        for (int i = 0; i < meses_do_ano.length; i++) {
            if (mes.equalsIgnoreCase(meses_do_ano[i])) {
                i_mes = i;
                break;
            }
        }
        return i_mes;
    }

}
