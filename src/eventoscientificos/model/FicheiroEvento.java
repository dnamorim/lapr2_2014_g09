/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.File;
import java.util.List;

/**
 *
 * @author i130672
 */
public interface FicheiroEvento {

    public File getFile();

    public void setFile(File ficheiro);

    public List<String> lerFile();

    public List<Evento> criarListaEventos(List<String> lista);
}
