/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.controller.EstatisticaController;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author andrémiguel
 */
public class EstatisticaMediaAvaliacoes {

    private Utilizador user;
    private Empresa m_empresa;
    private Evento m_evento;
    private Revisor m_revisor;
    private Submissao m_submissao;
    private RegistoEventos m_registoEventos;
    private List<Submissao> m_listaSubmissoes;
    private List<Artigo> m_listaArtigos;
    private List<Revisor> m_listaRevisores;
    private List<Revisao> m_listaRevisoes;
    private Revisao m_revisao;
    private double zConfianca;

    public void EstatisticaMediaAvaliacoes(Empresa emp, Utilizador u, Evento e) {
        m_empresa = emp;
        user = u;
        m_evento = e ;
    }

    public double getZConfianca() {
        return zConfianca;
    }

    public void setZConfianca(double novaZConfianca) {
        zConfianca = novaZConfianca;
    }

    /**
     * Método que obtém a lista total de revisores que pertencem aos eventos do
     * Organizador
     *
     * @return lista de revisores de todos os eventos
     */
    public List<Revisor> getListaRevisoresEventosOrganizador() {
        List<Evento> listaEventos = m_empresa.getRegistoEventos().getEventosOrganizador(user);

        if (listaEventos != null) {
            for (Evento evt : listaEventos) {
                List<Revisor> listaRevisorEvento = evt.getCP().getListaRevisores();
                m_listaRevisores.addAll(listaRevisorEvento);
            }
        }
        return m_listaRevisores;
    }

    /**
     * Método que retorna o numero de revisores dos eventos selecionados pelo
     * organizador
     *
     * @return numero de revisores
     */
    public int getNumeroRevisores() {
        return (int) m_listaRevisores.size();
    }

    /**
     * Método que obtém a lista total de submissões que pertencem aos eventos do
     * Organizador
     *
     * @param u Utilizador
     * @return lista de submissões de todos os eventos
     */
    public List<Submissao> getSubmissoesEventos(Utilizador u) {
        List<Evento> listaEventos = m_empresa.getRegistoEventos().getEventosOrganizador(u);
        if (listaEventos != null) {
            for (Evento evt : listaEventos) {
                List<Submissao> listaSubmissoes = evt.getListaSubmissoes();
                m_listaSubmissoes.addAll(listaSubmissoes);
            }
        }
        return m_listaSubmissoes;
    }

     /**
     * Método que permite saber os artigos de um revisor
     *
     * @param user Utilizador
     * @return lista de artigos do revisor
     */
    public List<Artigo> getArtigosRevisor(Utilizador user) {
        m_listaArtigos = new ArrayList<>();
        List<Submissao> listaDeSubmissoes = m_evento.getListaSubmissoes();

        for (Submissao s : listaDeSubmissoes) {
            List<Revisor> listaDeRevisores = s.getArtigo().getRevisores();
            for (Revisor r : listaDeRevisores) {
                if (r.getUtilizador().equals(user)) {
                    m_listaArtigos.add(s.getArtigo());
                }
            }
        }
        return m_listaArtigos;
    }

    /**
     * Método que calcula a média de avaliações do artigo
     *
     * @param rev Revisão com o valor de cada avaliação
     * @return média de avaliações do artigo
     */
    public float calcMediaArtigo(Revisao rev) {
        int av1 = rev.getConfianca();
        int av2 = rev.getAdequacao();
        int av3 = rev.getOriginalidade();
        int av4 = rev.getQualidade();

        float mediaArtigo = (float) ((av1 + av2 + av3 + av4) / 4);
        return mediaArtigo;
    }

    /**
     * Método que calcula a média global dos artigos revistos pelo revisor
     *
     * @param u Utilizador
     * @return média global dos artigos revistos pelo revisor
     */
    public float calcMediaGlobalArtigos(Utilizador u) {
        int nrArtigosRevisor = 0;
        float mediaArtigo, soma = 0;
        List<Evento> listaEventos = m_empresa.getRegistoEventos().getEventosOrganizador(user);

        for (Evento evt : listaEventos) {
            m_listaRevisores = evt.getCP().getListaRevisores();
            for (Revisor rev : m_listaRevisores) {
                if (rev.getUtilizador().equals(u)) {
                    m_listaArtigos = this.getArtigosRevisor(u);
                    nrArtigosRevisor = m_listaArtigos.size();
                    for (Artigo a : m_listaArtigos) {
                        m_listaRevisoes = a.getRevisoes();
                        for (Revisao rvs : m_listaRevisoes) {
                            if (rvs.getRevisor().equals(rev)) {
                                mediaArtigo = calcMediaArtigo(rvs);
                                soma += mediaArtigo;
                            }
                        }
                    }
                }
            }
        }
        return (float) (soma / nrArtigosRevisor);
    }

    /**
     * Método que calcula o desvio para cada revisor
     *
     * @param u Utilizador
     * @return desvio
     */
    public float calcDesvioRevisor(Utilizador u) {
        float mediaArtigo = 0;
        float mediaGlobalArtigos = calcMediaGlobalArtigos(u);
        List<Evento> listaEventos = m_empresa.getRegistoEventos().getEventosOrganizador(user);

        for (Evento evt : listaEventos) {
            m_listaRevisores = evt.getCP().getListaRevisores();
            for (Revisor rev : m_listaRevisores) {
                if (rev.getUtilizador().equals(u)) {
                    m_listaArtigos = this.getArtigosRevisor(u);
                    for (Artigo a : m_listaArtigos) {
                        m_listaRevisoes = a.getRevisoes();
                        for (Revisao rvs : m_listaRevisoes) {
                            if (rvs.getRevisor().equals(rev)) {
                                mediaArtigo = calcMediaArtigo(rvs);
                            }
                        }
                    }
                }
            }
        }
        return Math.abs(mediaArtigo - mediaGlobalArtigos);
    }

    /**
     * Método que calcula o desvio médio do revisor
     *
     * @param u Utilizador
     * @return desvio médio do revisor
     */
    public float calcDesvioMedioRevisor(Utilizador u) {
        float desvio = 0, soma = 0, c = 0;
        List<Evento> listaEventos = m_empresa.getRegistoEventos().getEventosOrganizador(user);
        for (Evento evt : listaEventos) {
            m_listaRevisores = evt.getCP().getListaRevisores();
            for (Revisor rev : m_listaRevisores) {
                if (rev.getUtilizador().equals(u)) {
                    soma = calcSomaDesviosRevisor(u);
                    c++;
                }
            }
        }
        return Math.abs(soma / c);
    }

    /**
     * Método que calcula a soma dos desvios todos do revisor
     *
     * @param u Utilizador
     * @return soma dos desvios todos do revisor
     */
    public float calcSomaDesviosRevisor(Utilizador u) {
        float soma = 0;
        float desviosRevisor = 0;

        List<Evento> listaEventos = m_empresa.getRegistoEventos().getEventosOrganizador(user);
        for (Evento evt : listaEventos) {
            m_listaRevisores = evt.getCP().getListaRevisores();
            for (Revisor rev : m_listaRevisores) {
                if (rev.getUtilizador().equals(u)) {
                    calcDesvioRevisor(u);
                    soma += desviosRevisor;
                }
            }
        }
        return soma;
    }

    /**
     * Método que calcula o Numero de desvios do revisor
     *
     * @param u Utilizador
     * @return Numero de desvios do revisor
     */
    public int calcNumeroDeDesviosRevisor(Utilizador u) {
        int numeroDeDesvios = 0;
        List<Evento> listaEventos = m_empresa.getRegistoEventos().getEventosOrganizador(user);
        for (Evento evt : listaEventos) {
            m_listaRevisores = evt.getCP().getListaRevisores();
            for (Revisor rev : m_listaRevisores) {
                if (rev.getUtilizador().equals(u)) {
                    calcDesvioRevisor(u);
                    numeroDeDesvios++;
                }
            }
        }
        return numeroDeDesvios;
    }

    /**
     * Método que calcula a variancia de um Revisor
     *
     * @param u Utilizador
     * @return variancia
     */
    public double calcVarianciaRevisor(Utilizador u) {
        // Método incompleto //
        double variancia = 0;
        double nDesvios = 0, totalDesvios = 0;
        double somaDesvios = 0;
        double desvio = 0;
        List<Evento> listaEventos = m_empresa.getRegistoEventos().getEventosOrganizador(user);
        for (Evento evt : listaEventos) {
            m_listaRevisores = evt.getCP().getListaRevisores();
            for (Revisor rev : m_listaRevisores) {
                if (rev.getUtilizador().equals(u)) {
                    somaDesvios = calcSomaDesviosRevisor(u);
                    totalDesvios = Math.pow(somaDesvios, 2);
                    nDesvios = calcNumeroDeDesviosRevisor(u);
                    desvio = calcDesvioRevisor(u);
                    variancia = (1 / (nDesvios - 1)) * (totalDesvios - (nDesvios * Math.pow(desvio, 2)));
                }
            }
        }
        return variancia;
    }

    /**
     * Método que calcula a Estimativa
     *
     * @param u Utilizador
     * @return estimativa
     */
    public double calcET(Utilizador u) {
        double nDesviosRevisor = 0, desvioMedioRevisor = 0, et = 0;
        double variancia = 0;
        List<Evento> listaEventos = m_empresa.getRegistoEventos().getEventosOrganizador(user);
        for (Evento evt : listaEventos) {
            m_listaRevisores = evt.getCP().getListaRevisores();
            for (Revisor rev : m_listaRevisores) {
                if (rev.getUtilizador().equals(u)) {
                    nDesviosRevisor = calcNumeroDeDesviosRevisor(u);
                    desvioMedioRevisor = calcDesvioMedioRevisor(u);
                    variancia = calcVarianciaRevisor(u);
                    et = (desvioMedioRevisor - 1) / (Math.sqrt(variancia / nDesviosRevisor));
                }
            }
        }
        return et;
    }

    /**
     * Método que verifica se é necessário dar alerta, caso seja retorna true,
     * caso contrário retorna false, caso o revisor não tenha 30 artigos
     * revistos, também retorna false
     *
     * @param u Utilizador
     * @return True ou False
     */
    public boolean checkETandZC(Utilizador u) {
        double zc = 0, et = 0;
        boolean result = false;
        List<Evento> listaEventos = m_empresa.getRegistoEventos().getEventosOrganizador(user);
        for (Evento evt : listaEventos) {
            m_listaRevisores = evt.getCP().getListaRevisores();
            for (Revisor rev : m_listaRevisores) {
                if (rev.getUtilizador().equals(u)) {
                    m_listaArtigos = this.getArtigosRevisor(u);
                    for (Artigo a : m_listaArtigos) {
                        m_listaRevisoes = a.getRevisoes();
                        if (m_listaRevisoes.size() >= 30) {

                            zc = this.getZConfianca();
                            et = this.calcET(u);
                            if (et > zc) {
                                result = true;
                            }
                        }
                    }
                }
            }

        }
        return result;
    }
}
