/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 *
 * @author dnamorim
 */
public class Pagamento implements Serializable {
    
    private Autor autor;
    private Date dataPagamento;
    
    public void setAutor(Autor a) {
        this.autor = a;
    }
    
    public void setDataPagamento(Date data) {
        this.dataPagamento = data;
    }
    
    public Date getDataPagamento() {
        return this.dataPagamento;
    }
    
    public Autor getAutorPagamento() {
        return this.autor;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.autor);
        hash = 79 * hash + Objects.hashCode(this.dataPagamento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pagamento other = (Pagamento) obj;
        if(this.autor.equals(other.autor) && this.dataPagamento.equals(other.dataPagamento)) {
            return true;
        } else {
            return false;
        }
    }
    
}
