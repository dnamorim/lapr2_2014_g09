/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.util.Date;
import pt.ipp.isep.dei.eapli.canadaexpress.*;

/**
 *
 * @author dnamorim
 */
public class CanadaExpressPagamento implements MetodoPagamento {

    private CanadaExpress ce;
    private String nrCartao;
    private Date dataValidade;
    private Date dataAutorizacao;
    private Date dataPagamento;
    
    
    public CanadaExpressPagamento() {
        this.ce = new CanadaExpress();
        this.ce.Init("#CANADA#EXPRESS#EAPLI#");
    }
    
    @Override
    public void setInfoPagamento(String nrCartao, Date dataValidade, Date dataAutorizacao) {
        this.nrCartao = nrCartao;
        this.dataValidade = dataValidade;
        this.dataAutorizacao = dataAutorizacao;
    }

    @Override
    public boolean pagamento(float valorAPagar) {
        try {
            Pedido request = new Pedido(dataValidade, nrCartao, valorAPagar, dataAutorizacao);
            String result = ce.ValidaPedido(request);
            ce.Finish();
            if(result.equals("**Inválida**")) {
                return false;
            } else {
                dataPagamento = new Date();
                return true;
            }
        } catch(IllegalArgumentException ex) {
            return false;
        }
    }

    @Override
    public Date getDataPagamento() {
        return dataPagamento;
    }
    
}
