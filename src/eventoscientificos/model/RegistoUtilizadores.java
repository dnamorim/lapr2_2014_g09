/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import eventoscientificos.model.Utilizador;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dnamorim
 */
public class RegistoUtilizadores implements Serializable {
    
    private List<Utilizador> m_listaUtilizadores;
            
    public RegistoUtilizadores() {
        m_listaUtilizadores = new ArrayList<Utilizador>();
    }
    
    public Utilizador novoUtilizador() {
        return new Utilizador();
    }

    public boolean registaUtilizador(Utilizador u) {
        if (u.valida() && validaUtilizador(u)) {
            return addUtilizador(u);
        } else {
            return false;
        }
    }

    private boolean validaUtilizador(Utilizador u) {
        for (Utilizador uExistente : m_listaUtilizadores) {
            if (uExistente.mesmoQueUtilizador(u)) {
                return false;
            }
        }
        return true;
    }

    public Utilizador getUtilizador(String strId) {
        for (Utilizador u : this.m_listaUtilizadores) {
            String s1 = u.getUsername();
            if (s1.equalsIgnoreCase(strId)) {
                return u;
            }
        }
        return null;
    }

    public Utilizador getUtilizadorEmail(String strEmail) {
        for (Utilizador u : this.m_listaUtilizadores) {
            String s1 = u.getEmail();
            if (s1.equalsIgnoreCase(strEmail)) {
                return u;
            }
        }

        return null;
    }

    private boolean addUtilizador(Utilizador u) {
        return m_listaUtilizadores.add(u);
    }

    
    public List<Utilizador> getListaUtilizadores() {
        return m_listaUtilizadores;
    }

}
