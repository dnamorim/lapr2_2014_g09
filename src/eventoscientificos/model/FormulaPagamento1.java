/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.Serializable;
import java.util.List;


/**
 *
 * @author dnamorim
 */
public class FormulaPagamento1 implements FormulaPagamento, Serializable {

    private Evento e;

    public FormulaPagamento1(Evento e) {
        this.e = e;
    }

    @Override
    public float valorAPagar(List<Submissao> listSub) {
        int[] nrArtigos = new int[3];
        float[] precoArtigos = {e.getPrecoShort(), e.getPrecoFull(), e.getPrecoPoster()};
        
        for (Submissao s : listSub) {
            if(s.getArtigo().getTipo() == TipoArtigo.SHORT) {
                nrArtigos[0]++;
            }
            if(s.getArtigo().getTipo() == TipoArtigo.FULL) {
                nrArtigos[1]++;
            }
            if(s.getArtigo().getTipo() == TipoArtigo.POSTER) {
                nrArtigos[2]++;
            }
        }
        
        float valor = 0;
        
        for (int i = 0; i < precoArtigos.length; i++) {
            valor += precoArtigos[i] * nrArtigos[i];
        }
        
        return valor;
    }

    @Override
    public String descricao() {
        return String.format("Pagamento Equitativo.");
    }
    
}
