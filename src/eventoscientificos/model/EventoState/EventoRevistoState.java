/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model.EventoState;

import eventoscientificos.model.Evento;
import java.io.Serializable;

/**
 *
 * @author dnamorim
 */
public class EventoRevistoState implements EventoState, Serializable {

    private Evento m_e;

    public EventoRevistoState(Evento m_e) {
        this.m_e = m_e;
    }

    @Override
    public boolean setCriadoCSV() {
        return false;
    }

    @Override
    public boolean setLidoCSV() {
        return false;
    }

    @Override
    public boolean setCriadoXML() {
        return false;
    }

    @Override
    public boolean setLidoXML() {
        return false;
    }

    @Override
    public boolean setCriado() {
        return false;
    }

    @Override
    public boolean setRegistado() {
        return false;
    }

    @Override
    public boolean setValoresRegistoDefindo() {
        return false;
    }

    @Override
    public boolean setTopicosCriados() {
        return false;
    }

    @Override
    public boolean setCPDefinida() {
        return false;
    }

    @Override
    public boolean setProntoADistribuir() {
        return false;
    }

    @Override
    public boolean setDistribuido() {
        return false;
    }

    @Override
    public boolean setRevisto() {
        return true;
    }

    @Override
    public boolean setDecidido() {
        if(valida()) {
            this.m_e.setState(new EventoDecididoState(m_e));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setNotificado() {
        return false;
    }

    @Override
    public boolean setCameraReady() {
        return false;
    }

    @Override
    public boolean setRegistadosNoEvento() {
        return false;
    }

    @Override
    public boolean setTerminado() {
        return false;
    }

    @Override
    public boolean valida() {
        return true;
    }
    
}
