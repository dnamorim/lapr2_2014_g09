/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model.EventoState;

import eventoscientificos.model.Evento;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author dnamorim
 */
public class EventoNotificadoState implements EventoState, Serializable {

    private Evento m_e;

    public EventoNotificadoState(Evento m_e) {
        this.m_e = m_e;
    }

    @Override
    public boolean setCriadoCSV() {
        return false;
    }

    @Override
    public boolean setLidoCSV() {
        return false;
    }

    @Override
    public boolean setCriadoXML() {
        return false;
    }

    @Override
    public boolean setLidoXML() {
        return false;
    }

    @Override
    public boolean setCriado() {
        return false;
    }

    @Override
    public boolean setRegistado() {
        return false;
    }

    @Override
    public boolean setValoresRegistoDefindo() {
        return false;
    }

    @Override
    public boolean setTopicosCriados() {
        return false;
    }

    @Override
    public boolean setCPDefinida() {
        return false;
    }

    @Override
    public boolean setProntoADistribuir() {
        return false;
    }

    @Override
    public boolean setDistribuido() {
        return false;
    }

    @Override
    public boolean setRevisto() {
        return false;
    }

    @Override
    public boolean setDecidido() {
        return false;
    }

    @Override
    public boolean setNotificado() {
        return true;
    }

    @Override
    public boolean setCameraReady() {
        if(valida()) {
            m_e.setState(new EventoCameraReadyState(m_e));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setRegistadosNoEvento() {
        return false;
    }

    @Override
    public boolean setTerminado() {
        return false;
    }

    @Override
    public boolean valida() {
        //if(m_e.getDataLimiteSubmissaoFinal().getTime().after(new Date())) return true; else return false;
        return true;
    }
    
    
    
}
