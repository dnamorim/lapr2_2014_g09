/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model.EventoState;

/**
 *
 * @author dnamorim
 */
public interface EventoState {
    
    boolean setCriadoCSV();
    boolean setLidoCSV();
    
    boolean setCriadoXML();
    boolean setLidoXML();
    
    boolean setCriado(); 
    boolean setRegistado();
    boolean setValoresRegistoDefindo();
    boolean setTopicosCriados(); 
    boolean setCPDefinida();
    boolean setProntoADistribuir();
    boolean setDistribuido();
    boolean setRevisto();
    boolean setDecidido();
    boolean setNotificado();
    boolean setCameraReady();
    boolean setRegistadosNoEvento();
    boolean setTerminado();
    
    boolean valida();
}
