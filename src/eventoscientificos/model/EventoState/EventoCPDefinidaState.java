/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model.EventoState;

import eventoscientificos.model.Evento;
import java.io.Serializable;

/**
 *
 * @author dnamorim
 */
public class EventoCPDefinidaState implements EventoState, Serializable {

    private Evento m_e;

    public EventoCPDefinidaState(Evento m_e) {
        this.m_e = m_e;
    }

    @Override
    public boolean setCriadoCSV() {
        return false;
    }

    @Override
    public boolean setLidoCSV() {
        return false;
    }

    @Override
    public boolean setCriadoXML() {
        return false;
    }

    @Override
    public boolean setLidoXML() {
        return false;
    }

    @Override
    public boolean setCriado() {
        return false;
    }

    @Override
    public boolean setRegistado() {
        return false;
    }

    @Override
    public boolean setValoresRegistoDefindo() {
        return false;
    }

    @Override
    public boolean setTopicosCriados() {
        return false;
    }

    @Override
    public boolean setCPDefinida() {
        return true;
    }

    @Override
    public boolean setProntoADistribuir() {
        if(valida()) {
            m_e.setState(new EventoProntoADistribuirState(m_e)); 
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setDistribuido() {
        return false;
    }

    @Override
    public boolean setRevisto() {
        return false;
    }

    @Override
    public boolean setDecidido() {
        return false;
    }

    @Override
    public boolean setNotificado() {
        return false;
    }

    @Override
    public boolean setCameraReady() {
        return false;
    }

    @Override
    public boolean setRegistadosNoEvento() {
        return false;
    }

    @Override
    public boolean setTerminado() {
        return false;
    }

    @Override
    public boolean valida() {
        if(m_e.getListaSubmissoes().size() > 0 ) {//&& m_e.getDataLimiteSubmissao().getTime().after(new Date())) {
            return true;
        } else {
            return false;
        }
    }
    
}
