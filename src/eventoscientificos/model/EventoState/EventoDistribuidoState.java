/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model.EventoState;

import eventoscientificos.model.Evento;
import eventoscientificos.model.Submissao;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author dnamorim
 */
public class EventoDistribuidoState implements EventoState, Serializable {

    public EventoDistribuidoState(Evento m_e) {
        this.m_e = m_e;
    }

    private Evento m_e;

    @Override
    public boolean setCriadoCSV() {
        return false;
    }

    @Override
    public boolean setLidoCSV() {
        return false;
    }

    @Override
    public boolean setCriadoXML() {
        return false;
    }

    @Override
    public boolean setLidoXML() {
        return false;
    }

    @Override
    public boolean setCriado() {
        return false;
    }

    @Override
    public boolean setRegistado() {
        return false;
    }

    @Override
    public boolean setValoresRegistoDefindo() {
        return false;
    }

    @Override
    public boolean setTopicosCriados() {
        return false;
    }

    @Override
    public boolean setCPDefinida() {
        return false;
    }

    @Override
    public boolean setProntoADistribuir() {
        return false;
    }

    @Override
    public boolean setDistribuido() {
        return true;
    }

    @Override
    public boolean setRevisto() {
        if(valida()) {
            m_e.setState(new EventoRevistoState(m_e)); 
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean setDecidido() {
        return false;
    }

    @Override
    public boolean setNotificado() {
        return false;
    }

    @Override
    public boolean setCameraReady() {
        return false;
    }

    @Override
    public boolean setRegistadosNoEvento() {
        return false;
    }

    @Override
    public boolean setTerminado() {
        return false;
    }

    @Override
    public boolean valida() {
        //if(m_e.getDataLimiteRevisao().after(new Date())) {
            for (Submissao s : m_e.getListaSubmissoes()) {
                if(!(s.getArtigo().getDistribuicao().getListRevisor().size() == s.getArtigo().getRevisoes().size())) {
                    return false;
                }
            }
            return true;
        //} else return false;
    }
    
    
}
