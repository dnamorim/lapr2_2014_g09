/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andrémiguel
 */
public class EstatisticaEventos {

  
    private Empresa m_empresa;
    private Evento m_evento;
    private Revisao m_revisao;
    private Submissao m_submissao;
    private int[] qualidade;

    public EstatisticaEventos(Empresa emp) {
        m_empresa = emp;
    }

    /**
     * Método que calcula a taxa de aceitação de um evento
     *
     * @return taxa de aceitação de um evento
     */
    public float calcTaxaAceitacao(Evento e) {
        int c = 0;
        int totalArtigos = e.getListaSubmissoes().size();

        List<Submissao> listaDeSubmissoes = e.getListaSubmissoes();
        for (Submissao s : listaDeSubmissoes) {
            if (s.getArtigo().getRevisoes() != null) {
                for (Revisao r : s.getArtigo().getRevisoes()) {
                    if (r.getRecomendacao() != false) {
                        c++;
                    }
                }
            }
        }
        return (c / totalArtigos) * 100;
    }

    /**
     * Calcular média do parâmetro de avaliação qualidade
     *
     * @return media qualidade
     */
    public float calcMediaQualidade(Evento e) {
        int c = 0;
        int soma = 0;
        List<Submissao> listaDeSubmissoes = e.getListaSubmissoes();
        for (Submissao sub : listaDeSubmissoes) {
            if (sub.getArtigo().getRevisoes() != null) {
                for (Revisao rev : sub.getArtigo().getRevisoes()) {
                    for (int i = 0; i < sub.getArtigo().getRevisoes().size(); i++) {
                        soma = soma + sub.getArtigo().getRevisoes().get(i).getQualidade();
                        c++;
                    }
                }
            }

        }
        return (float) soma / c;
    }

    /**
     * Calcular média do parâmetro de avaliação adequação
     *
     * @return media adequacao
     */
    public float calcMediaAdequacao(Evento e) {
        int c = 0;
        int soma = 0;
        List<Submissao> listaDeSubmissoes = e.getListaSubmissoes();
        for (Submissao sub : listaDeSubmissoes) {
            if (sub.getArtigo().getRevisoes() != null) {
                for (Revisao rev : sub.getArtigo().getRevisoes()) {
                    for (int i = 0; i < sub.getArtigo().getRevisoes().size(); i++) {
                        soma = soma + sub.getArtigo().getRevisoes().get(i).getAdequacao();
                        c++;
                    }
                }
            }

        }
        return (float) soma / c;
    }

    /**
     * Calcular média do parâmetro de avaliação originalidade
     *
     * @return media originalidade
     */
    public float calcMediaOriginalidade(Evento e) {
        int c = 0;
        int soma = 0;
        List<Submissao> listaDeSubmissoes = e.getListaSubmissoes();
        for (Submissao sub : listaDeSubmissoes) {
            if (sub.getArtigo().getRevisoes() != null) {
                for (Revisao rev : sub.getArtigo().getRevisoes()) {
                    for (int i = 0; i < sub.getArtigo().getRevisoes().size(); i++) {
                        soma = soma + sub.getArtigo().getRevisoes().get(i).getOriginalidade();
                        c++;
                    }
                }
            }

        }
        return (float) soma / c;
    }

    /**
     * .
     * Calcular média do parâmetro de avaliação confiança
     *
     * @return media confiança
     */
    public float calcMediaConfianca(Evento e) {
        int c = 0;
        int soma = 0;
        List<Submissao> listaDeSubmissoes = e.getListaSubmissoes();
        for (Submissao sub : listaDeSubmissoes) {
            if (sub.getArtigo().getRevisoes() != null) {
                for (Revisao rev : sub.getArtigo().getRevisoes()) {
                    for (int i = 0; i < sub.getArtigo().getRevisoes().size(); i++) {
                        soma = soma + sub.getArtigo().getRevisoes().get(i).getConfianca();
                        c++;
                    }
                }
            }

        }
        return (float) soma / c;
    }

}
