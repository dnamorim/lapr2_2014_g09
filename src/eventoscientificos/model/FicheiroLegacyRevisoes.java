/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tiago
 */
public class FicheiroLegacyRevisoes {

    private File ficheiro_revisores;
    private HashMap<Evento, String> hg_legacy = new HashMap<>();
    private HashMap<Submissao, String> hg_submissao = new HashMap<>();
    private HashMap<Revisao, String> hg_revisoes = new HashMap<>();

    public FicheiroLegacyRevisoes() {

    }

    /**
     * @return the hg_legacy
     */
    public HashMap getHg_legacy() {
        return hg_legacy;
    }

    /**
     * @param hg_legacy the hg_legacy to set
     */
    public void setHg_legacy(HashMap hg_legacy) {
        this.hg_legacy = hg_legacy;
    }

    /**
     * @return the hg_submissao
     */
    public HashMap<Submissao, String> getHg_submissao() {
        return hg_submissao;
    }

    /**
     * @param hg_submissao the hg_submissao to set
     */
    public void setHg_submissao(HashMap<Submissao, String> hg_submissao) {
        this.hg_submissao = hg_submissao;
    }

    /**
     * @return the hg_revisoes
     */
    public HashMap<Revisao, String> getHg_revisoes() {
        return hg_revisoes;
    }

    /**
     * @param hg_revisoes the hg_revisoes to set
     */
    public void setHg_revisoes(HashMap<Revisao, String> hg_revisoes) {
        this.hg_revisoes = hg_revisoes;
    }

    /**
     * @return the ficheiro_revisores
     */
    public File getFicheiro_revisores() {
        return ficheiro_revisores;
    }

    /**
     * @param ficheiro_revisores the ficheiro_revisores to set
     */
    public void setFicheiro_revisores(File ficheiro_revisores) {
        this.ficheiro_revisores = ficheiro_revisores;
    }

    public List<String> lerFile() {
        List<String> lista_string_revisores = new ArrayList<>();
        BufferedReader ler = null;
        try {
            ler = new BufferedReader(new FileReader(getFicheiro_revisores()));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }

        String linha;
        try {
            while ((linha = ler.readLine()) != null) {
                String[] temp = linha.split(";");
                if (!temp[0].equals("ConferenceID")) {
                    lista_string_revisores.add(linha);
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            ler.close();
        } catch (IOException ex) {
            Logger.getLogger(FicheiroEventoCSV.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista_string_revisores;
    }

    public void criarListaRevisoes(List<String> ls) {
        for (String linha : ls) {
            Submissao sub = null ;
            Evento evento=null;
            Revisor reviewer = null;
            String temp[] = linha.split(";");

            Set set = hg_submissao.entrySet();
            Iterator iter = set.iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                if (entry.getValue().equals(temp[1])) {
                    sub = (Submissao) entry.getKey();
                }
            }

            set = hg_legacy.entrySet();
            iter = set.iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry) iter.next();
                if (entry.getValue().equals(temp[0])) {
                    evento = (Evento) entry.getKey();
                    for(Revisor revisor : evento.getCP().getListaRevisores()){
                        if(revisor.getUtilizador().getNome().equalsIgnoreCase(temp[2])){
                            reviewer=revisor;
                        }
                    }
                }
            }
            
            Revisao rev = new Revisao(sub.getArtigo(),reviewer);
            rev.setConfianca(Integer.parseInt(temp[3].trim()));
            rev.setAdequacao(Integer.parseInt(temp[4].trim()));
            rev.setOriginalidade(Integer.parseInt(temp[5].trim()));
            rev.setQualidade(Integer.parseInt(temp[6].trim()));
            rev.setJustificacao("sem justificação");
            
            if(temp[7].trim().equalsIgnoreCase("Accept")){
                rev.setRecomendacao(true);
            }else{
                rev.setRecomendacao(false);
            }
            rev.adicionarAoArtigo();
        }
    }
}
