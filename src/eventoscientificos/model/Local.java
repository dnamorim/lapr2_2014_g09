/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author Nuno Silva
 */

public class Local implements Serializable {
    private String m_cidade;
    private String m_pais;

    public Local() {
    }

    public void setCidade(String strLocal) {
        m_cidade = strLocal;
    }

    public void setPais(String strLocal) {
        m_pais = strLocal;
    }

    @Override
    public String toString() {
        return getPais() + "\n" + getCidade();
    }

    /**
     * @return the m_cidade
     */
    public String getCidade() {
        return m_cidade;
    }

    /**
     * @return the m_pais
     */
    public String getPais() {
        return m_pais;
    }
}
