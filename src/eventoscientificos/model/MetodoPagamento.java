/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.util.Date;

/**
 *
 * @author dnamorim
 */
public interface MetodoPagamento {
    void setInfoPagamento(String nrCartao, Date dataValidade, Date dataAutorizacao);
    boolean pagamento(float valorAPagar);
    Date getDataPagamento();
}
