/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author andrémiguel
 */
public class Revisao implements Serializable {

    /**
     * Artigo
     */
    private Artigo m_artigo;
    /**
     * Revisor
     */
    private Revisor m_revisor;
    /**
     * Avaliação da confiança
     */
    private int confianca;
    /**
     * Avaliação da adequacao
     */
    private int adequacao;
    /**
     * Avaliação da originalidade
     */
    private int originalidade;
    /**
     * Avaliação da qualidade
     */
    private int qualidade;
    /**
     * Recomendação(aceita ou rejeita)
     */
    private boolean recomendacao;
    /**
     * Justificação
     */
    private String justificacao;

    /**
     * Construtor Revisão
     *
     * @param artigo Artigo
     * @param revisor Revisorr
     */
    public Revisao(Artigo artigo, Revisor revisor) {
        this.m_artigo = artigo;
        this.m_revisor = revisor;
    }

    /**
     * Método que mostra o Revisor desta revisão
     *
     * @return Revisor
     */
    public Revisor getRevisor() {
        return this.getM_revisor();
    }

    /**
     * Método que mostra o valor da confiança
     *
     * @return confiança
     */
    public int getConfianca() {
        return confianca;
    }

    /**
     * Método que mostra o valor da adequação
     *
     * @return adequação
     */
    public int getAdequacao() {
        return adequacao;
    }

    /**
     * Método que mostra o valor da originalidade
     *
     * @return originalidade
     */
    public int getOriginalidade() {
        return originalidade;
    }

    /**
     * Método que mostra o valor da qualidade
     *
     * @return qualidade
     */
    public int getQualidade() {
        return qualidade;
    }

    /**
     * Método que mostra a recomendação
     *
     * @return recomendação
     */
    public boolean getRecomendacao() {
        return recomendacao;
    }

    /**
     * Móetodo que mostra a justificação
     *
     * @return justificação
     */
    public String getJustificacao() {
        return justificacao;
    }

    /**
     * Método para definir um novo revisor
     *
     * @param rev novo revisor
     */
    public void setRevisor(Revisor rev) {
        setM_revisor(rev);
    }

    /**
     * Método para definir um novo valor da confiança
     *
     * @param conf nova valor confiança
     */
    public void setConfianca(int conf) {
        confianca = conf;
    }

    /**
     * Método para definir um novo valor da adequação
     *
     * @param adeq novo valor adequação
     */
    public void setAdequacao(int adeq) {
        adequacao = adeq;
    }

    /**
     * Método para definir um novo valor da originalidade
     *
     * @param orig novo valor originalidade
     */
    public void setOriginalidade(int orig) {
        originalidade = orig;
    }

    /**
     * Método para definir um novo valor da qualidade
     *
     * @param qual nova qualidade
     */
    public void setQualidade(int qual) {
        qualidade = qual;
    }

    /**
     * Método para definir uma nova recomendação
     *
     * @param rec nova recomendação
     */
    public void setRecomendacao(boolean rec) {
        recomendacao = rec;
    }

    /**
     * Método para definir uma nova justificação
     *
     * @param just nova justificação
     */
    public void setJustificacao(String just) {
        justificacao = just;
    }

    /**
     * Método que torna o valor da recomendação em formato String
     *
     * @return Estado da revisão
     */
    public String getEstado() {
        if (recomendacao = true) {
            return "Aceite";
        } else {
            return "Rejeitado";
        }
    }

    /**
     * Método para validar a revisão
     *
     * @return valor da validação
     */
    public boolean valida() {
        if (confianca < 0 && confianca > 5 && adequacao < 0 && adequacao > 5 && originalidade < 0 && originalidade > 5 && qualidade < 5 && qualidade > 5) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Método que imprime a informação da revisão
     *
     * @return informação
     */
    @Override
    public String toString() {
        return String.format("Artigo:%s\n"
                + "Confiança: %s\n"
                + "Adequação: %s\n"
                + "Originalidade: %s\n"
                + "Recomendação: %s\n"
                + "Justificação: %s\n", getM_artigo(), confianca, adequacao, originalidade, recomendacao, justificacao);
    }

    /**
     * Método equals para comparar um objecto como parâmetro com um actual.
     *
     * @param obj objecto para comparar
     * @return resultado da comparação
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Revisao other = (Revisao) obj;
        if (!Objects.equals(this.m_revisor, other.m_revisor)) {
            return false;
        }
        if (!Objects.equals(this.confianca, other.confianca)) {
            return false;
        }
        if (!Objects.equals(this.adequacao, other.adequacao)) {
            return false;
        }
        if (!Objects.equals(this.originalidade, other.originalidade)) {
            return false;
        }
        if (!Objects.equals(this.qualidade, other.qualidade)) {
            return false;
        }
        if (!Objects.equals(this.recomendacao, other.recomendacao)) {
            return false;
        }
        if (!Objects.equals(this.justificacao, other.justificacao)) {
            return false;
        }
        return true;
    }

    public void adicionarAoArtigo(){
        this.getM_artigo().getRevisoes().add(this);
    }
    /**
     * Hash code
     *
     * @return hash code
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.getM_revisor());
        hash = 23 * hash + this.confianca;
        hash = 23 * hash + this.adequacao;
        hash = 23 * hash + this.originalidade;
        hash = 23 * hash + this.qualidade;
        hash = 23 * hash + (this.recomendacao ? 1 : 0);
        hash = 23 * hash + Objects.hashCode(this.justificacao);
        return hash;
    }

    /**
     * @return the m_artigo
     */
    public Artigo getM_artigo() {
        return m_artigo;
    }

    /**
     * @param m_artigo the m_artigo to set
     */
    public void setM_artigo(Artigo m_artigo) {
        this.m_artigo = m_artigo;
    }

    /**
     * @return the m_revisor
     */
    public Revisor getM_revisor() {
        return m_revisor;
    }

    /**
     * @param m_revisor the m_revisor to set
     */
    public void setM_revisor(Revisor m_revisor) {
        this.m_revisor = m_revisor;
    }

}
