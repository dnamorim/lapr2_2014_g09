package eventoscientificos.model;

import eventoscientificos.model.SubmissaoState.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class Submissao implements Serializable {

    private Artigo m_artigo;
    private SubmissaoState m_state;
    private Pagamento m_pagamento;
    
    public Submissao() {
        setState(new SubmissaoCriadaState(this));
        m_pagamento = null;
    }

    public Artigo novoArtigo() {
        return new Artigo();
    }

    public String getInfo() {
        return this.toString();
    }
    
    public Pagamento novoPagamento() {
        return new Pagamento();
    }
    
    public void addPagamento(Pagamento pag) {
        this.m_pagamento = pag;
    }

    public void setArtigo(Artigo artigo) {
        this.m_artigo = artigo;
    }

    public Artigo getArtigo() {
        return this.m_artigo;
    }
    
    public Pagamento getPagamento() {
        return this.m_pagamento;
    }

    public boolean valida() {
        return true;
    }
    
    public boolean isPaga() {
        if(m_pagamento == null) {
            return false;
        } else {
            return true;
        }
    }
    
    public void setState(SubmissaoState state) {
        m_state = state;
    }
    public SubmissaoState getState(){
        return m_state;
    }

    public boolean setSubmetido() {
        if (m_state.setSubmetido()) {
            return true;
        } else {
            return false;
        }
    }
   
    
    public boolean setDistribuida() {
        if(m_state.setDistribuido()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setRevisto() {
        if(m_state.setRevisto()) {
            return true;
        } else {
            return false;
        }
    }
    
    public boolean setFinalSubmetido() {
        m_state = new SubmissaoFinalSubmetidaState(this);
        return true;
    }
    
    
    public boolean setSubmissaoRegistada() {
        m_state = new SubmissaoFinalSubmetidaState(this);
        if(m_state.setRegistado()) {
            return true;
        } else {
            return false;
        }
    }
    
    @Override
    public String toString() {
        return String.format("Submissão:%n%s", this.m_artigo);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        final Submissao s = (Submissao) obj;
        return (this.m_artigo.equals(s.getArtigo()) && this.m_state.getClass() == s.getClass()) ;
    }

    public boolean setDecidida() {
        if(this.m_artigo.getDecisao().getDecisao()) {
            if(m_state.setAceite()) {
                return true;
            } else {
                return false;
            }
        } else {
            if(m_state.setRejeitado()) {
                return true;
            } else {
                return false;
            }
        }
    }
    
}
