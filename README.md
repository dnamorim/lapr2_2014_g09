# README #

Repositório Git do Grupo 09 da turma 1DC/1DD para o projecto de LAPR2.

**Relatório do Projecto:** [https://bitbucket.org/1130674/lapr2_2014_g09/downloads/LAPR2_report.pdf](https://bitbucket.org/1130674/lapr2_2014_g09/downloads/LAPR2_report.pdf)

### Membros do Grupo ###

* Duarte Nuno Amorim (1130674@isep.ipp.pt)
* Tiago Ferreira (1130672@isep.ipp.pt)
* Eduardo Pinto (1130466@isep.ipp.pt)
* André Azevedo (1130740@isep.ipp.pt) 

### Orientador ###

* Prof. Fernando Duarte (fjd@isep.ipp.pt)

© LEI-ISEP, 2014