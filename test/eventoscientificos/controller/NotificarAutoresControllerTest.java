package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Local;
import eventoscientificos.model.Organizador;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 */
public class NotificarAutoresControllerTest {

    private static Evento eventTest;

    public NotificarAutoresControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        eventTest = new Evento();
        eventTest.setTitulo("2013 9th International CDIO Conference, MIT, USA");
        eventTest.setDescricao("2013 9th International CDIO Conference, MIT, USA");
        Local l = new Local();
        l.setCidade("Cambridge, MA");
        l.setPais("United States");
        eventTest.setHost("MIT and Harvard School of Engineering and Applied Sciences");
        eventTest.setLocal(l);
        eventTest.setDataInicio(new GregorianCalendar(2013, 6, 9));
        eventTest.setDataFim(new GregorianCalendar(2013, 6, 13));
        eventTest.setDataLimiteRegisto(new GregorianCalendar(2013, 6, 1));
        eventTest.setDataLimiteRevisao(new GregorianCalendar(2013, 4, 1));
        eventTest.setDataLimiteSubmissao(new GregorianCalendar(2013, 3, 20));
        eventTest.setDataLimiteSubmissaoFinal(new GregorianCalendar(2013, 5, 30));
        eventTest.setWebsite("http://laspau.org/cdio2013/");
        eventTest.addOrganizador(new Utilizador("cdio2013@mit.edu", "letmein", "Doris Brodeur", "cdio2013@mit.edu"));
        eventTest.addOrganizador(new Utilizador("revere@harvard.edu", "letmein", "Paul Revere", "revere@harvard.edu"));
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    private Empresa m_empresa;
    private Utilizador m_user;

    /**
     * Test of getListaEvento and obterArrayItemsEvento methods, of class
     * NotificarAutoresController.
     */
    @Test
    public void testGetListaEventoAndObterArrayItemsEvento() {
        System.out.println("getListaEvento e obterArrayItemsEvento");
        m_empresa = new Empresa();
        m_empresa.getRegistoEventos().registaEvento(eventTest);
        m_empresa.getRegistoUtilizadores().registaUtilizador(new Utilizador("cdio2013@mit.edu", "letmein", "Doris Brodeur", "cdio2013@mit.edu"));
        m_user = eventTest.getListaOrganizadores().get(0).getUtilizador();
        NotificarAutoresController instance = new NotificarAutoresController(m_empresa, this.m_user);
        instance.setEvento(eventTest);
        List<Evento> dados = new ArrayList<>();
        dados.add(eventTest);
        assertEquals(instance.obterArrayItemsEvento(dados), instance.getListaEvento());
    }

    /**
     * Test of setEvento and getEvento methods, of class
     * NotificarAutoresController.
     */
    @Test
    public void testSetGetEvento() {
        System.out.println("set e getEvento");
        NotificarAutoresController instance = new NotificarAutoresController(m_empresa, this.m_user);
        Evento e = new Evento();
        instance.setEvento(e);
        Evento expResult = e;
        Evento result = instance.getEvento();
        assertEquals(expResult.toString(), result.toString());
    }


}
