/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author dnamorim
 */
public class RegistarUtilizadorControllerTest {
    
    public RegistarUtilizadorControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    Empresa e = new Empresa();
    RegistarUtilizadorController instance = new RegistarUtilizadorController(e);
    
    
   
    /**
     * Test of setDados method, of class RegistarUtilizadorController.
     */
    @Test
    public void testSetDados() {
        instance.novoUtilizador();
        System.out.println("setDados valid");
        String strUsername = "teste";
        String strPassword = "letmein";
        String strNome = "Teste";
        String strEmail = "teste@tocs.pt";
        Utilizador expResult = new Utilizador(strUsername, strPassword, strNome, strEmail);
        Utilizador result = instance.setDados(strUsername, strPassword, strNome, strEmail);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of setDados method, of class RegistarUtilizadorController.
     */
    @Test
    public void testSetDadosInvalid() {
        instance.novoUtilizador();
        System.out.println("setDados invalid");
        String strUsername = "teste";
        String strPassword = "letmein";
        String strNome = "Teste";
        String strEmail = " tEstE@tocS.pt ";
        Utilizador result = instance.setDados(strUsername, strPassword, strNome, strEmail);
        assertNull(result);
    }
    
    /**
     * Test of setDados method, of class RegistarUtilizadorController.
     */
    @Test
    public void testSetDadosExists() {
        instance.novoUtilizador();
        System.out.println("setDados exists");
        String strUsername = "teste";
        String strPassword = "letmein";
        String strNome = "Teste";
        String strEmail = "teste@tocs.pt";
        instance.setDados(strUsername, strPassword, strNome, strEmail);
        
        instance.novoUtilizador();
        
        Utilizador result = instance.setDados(strUsername, strPassword, "Outro teste", strEmail);
        assertNull(result);
    }
    
}
