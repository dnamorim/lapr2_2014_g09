/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author i130672
 */
public class ImportarEventosControllerTest {

    public ImportarEventosControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setFile method, of class ImportarEventosController.
     */
    @Test
    public void testSetFile() {
        System.out.println("setFile");
        File ficheiro = new File("EventList_CDIO.csv");
        Empresa m_empresa = new Empresa();
        ImportarEventosController instance = new ImportarEventosController(m_empresa);
        instance.setFile(ficheiro);
        File expResult = ficheiro;
        File result = instance.getFile();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFile method, of class ImportarEventosController.
     */
    @Test
    public void testGetFile() {
        System.out.println("getFile");
        ImportarEventosController instance = new ImportarEventosController(new Empresa());
        File ficheiro=new File("EventList_CDIO.csv");
        instance.setFile(ficheiro);
        File expResult = ficheiro;
        File result = instance.getFile();
        assertEquals(expResult, result);
    }

}
