/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.controller;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Revisor;
import eventoscientificos.model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dnamorim
 */
public class ReverArtigosControllerTest {

    private Empresa empresa;
    private Utilizador ut;

    public ReverArtigosControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getListaEvento method, of class ReverArtigosController.
     */
    @Test
    public void testGetListaEvento() {
        System.out.println("getListaEvento");
        ReverArtigosController instance = new ReverArtigosController(empresa, ut);
        Evento e = new Evento();
        ReverArtigosController.ItemEvento[] novo = null;
        ReverArtigosController.ItemEvento[] expResult = null;
        ReverArtigosController.ItemEvento[] result = instance.getListaEvento();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of getListaSubmissao method, of class ReverArtigosController.
     */
    @Test
    public void testGetListaSubmissao() {
        System.out.println("getListaSubmissao");
        ReverArtigosController instance = new ReverArtigosController(empresa, ut);
        ReverArtigosController.ItemArtigo[] expResult = null;
        ReverArtigosController.ItemArtigo[] result = instance.getListaSubmissao();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of setEvento method, of class ReverArtigosController.
     */
    @Test
    public void testSetEvento() {
        System.out.println("setEvento");
        ReverArtigosController instance = new ReverArtigosController(empresa, ut);
        Evento e = new Evento();
        instance.setEvento(e);
        Evento expResult = e;

    }

    /**
     * Test of getConfianca method, of class ReverArtigosController.
     */
    @Test
    public void testSetGetConfianca() {
        System.out.println("getConfianca");
        ReverArtigosController instance = new ReverArtigosController(empresa, ut);
        int conf = 3;
        int expResult = 3;
        instance.setConfianca(conf);
        int result = instance.getConfianca();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getAdequacao method, of class ReverArtigosController.
     */
    @Test
    public void testSetGetAdequacao() {
        System.out.println("getAdequacao");
        ReverArtigosController instance = new ReverArtigosController(empresa, ut);
        int adeq = 2;
        int expResult = 2;
        instance.setAdequacao(adeq);
        int result = instance.getRevisao().getAdequacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of getQualidade method, of class ReverArtigosController.
     */
    @Test
    public void testSetGetQualidade() {
        System.out.println("getQualidade");
        ReverArtigosController instance = new ReverArtigosController(empresa, ut);
        int qual = 4;
        int expResult = 4;
        instance.setQualidade(qual);
        int result = instance.getRevisao().getQualidade();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getOriginalidade method, of class ReverArtigosController.
     */
    @Test
    public void testSetGetOriginalidade() {
        System.out.println("getOriginalidade");
        ReverArtigosController instance = new ReverArtigosController(empresa, ut);
        int orig = 2;
        int expResult = 2;
        instance.setOriginalidade(orig);
        int result = instance.getRevisao().getOriginalidade();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getRecomendacao method, of class ReverArtigosController.
     */
    @Test
    public void testSetGetRecomendacao() {
        System.out.println("getRecomendacao");
        ReverArtigosController instance = new ReverArtigosController(empresa, ut);
        boolean rec = false;
        boolean expResult = false;
        instance.setRecomendacao(rec);
        boolean result = instance.getRevisao().getRecomendacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of getJustificacao method, of class ReverArtigosController.
     */
    @Test
    public void testSetGetJustificacao() {
        System.out.println("getJustificacao");
        ReverArtigosController instance = new ReverArtigosController(empresa, ut);
        String just = "Mau artigo";
        String expResult = "Mau artigo";
        instance.setJustificacao(just);
        String result = instance.getRevisao().getJustificacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }


    /**
     * Test of getRevisor method, of class ReverArtigosController.
     */
    @Test
    public void testGetRevisor() {
        System.out.println("getRevisor");
        ReverArtigosController instance = new ReverArtigosController(empresa, ut);
        Revisor novoRevisor = new Revisor(ut);
        Revisor expResult = novoRevisor;
        Revisor result = instance.getRevisor();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
}
