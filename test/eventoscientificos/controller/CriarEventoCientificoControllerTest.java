/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Local;
import eventoscientificos.model.Utilizador;
import java.util.Date;
import java.util.GregorianCalendar;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author i130672
 */
public class CriarEventoCientificoControllerTest {
    
    private static CriarEventoCientificoController controllerCEC;
    private static Empresa e;
    private static Evento eventTest;
    
    public CriarEventoCientificoControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        e = new Empresa();
        controllerCEC = new CriarEventoCientificoController(e);
        controllerCEC.novoEvento();
        
        eventTest = new Evento();
        eventTest.setTitulo("2013 9th International CDIO Conference, MIT, USA");
        eventTest.setDescricao("2013 9th International CDIO Conference, MIT, USA");
        Local l = new Local();
        l.setCidade("Cambridge, MA");
        l.setPais("United States");
        eventTest.setHost("MIT and Harvard School of Engineering and Applied Sciences");
        eventTest.setLocal(l);
        eventTest.setDataInicio(new GregorianCalendar(2013, 6,9));
        eventTest.setDataFim(new GregorianCalendar(2013, 6,13));
        eventTest.setDataLimiteRegisto(new GregorianCalendar(2013, 6, 1));
        eventTest.setDataLimiteRevisao(new GregorianCalendar(2013, 4, 1));
        eventTest.setDataLimiteSubmissao(new GregorianCalendar(2013, 3, 20));
        eventTest.setDataLimiteSubmissaoFinal(new GregorianCalendar(2013, 5, 30));
        eventTest.setWebsite("http://laspau.org/cdio2013/");
        eventTest.addOrganizador(new Utilizador("cdio2013@mit.edu", "letmein", "Doris Brodeur", "cdio2013@mit.edu"));
        eventTest.addOrganizador(new Utilizador("revere@harvard.edu", "letmein", "Paul Revere", "revere@harvard.edu"));
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    
    
    /**
     * Test of novoEvento method, of class CriarEventoCientificoController.
     */
    @Test
    public void test00NovoEvento() {
        System.out.println("novoEvento");
        CriarEventoCientificoController instance = controllerCEC;
        instance.novoEvento();
    }
/**
     * Test of setTitulo method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetTitulo() {
        System.out.println("setTitulo");
        String strTitulo = eventTest.getTitulo();
        CriarEventoCientificoController instance = controllerCEC;
        instance.setTitulo(strTitulo);
        assertEquals(strTitulo, instance.getEvento().getTitulo());
    }

    /**
     * Test of setDescricao method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String strDescricao = eventTest.getDescricao();
        CriarEventoCientificoController instance = controllerCEC;
        instance.setDescricao(strDescricao);
        assertEquals(strDescricao, instance.getEvento().getDescricao());
    }

    /**
     * Test of setPais method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetPais() {
        System.out.println("setPais");
        String pais = eventTest.getLocal().getPais();
        CriarEventoCientificoController instance = controllerCEC;
        instance.setPais(pais);
        assertEquals(pais, instance.getEvento().getLocal().getPais());
    }

    /**
     * Test of setCidade method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetCidade() {
        System.out.println("setCidade");
        String cidade = eventTest.getLocal().getCidade();
        CriarEventoCientificoController instance = controllerCEC;
        instance.setCidade(cidade);
        assertEquals(cidade, instance.getEvento().getLocal().getCidade());
    }

    /**
     * Test of getEventoString method, of class CriarEventoCientificoController.
     */
    @Test
    public void test09GetEventoString() {
        System.out.println("getEventoString");
        CriarEventoCientificoController instance = controllerCEC;
        
        String expResult = controllerCEC.getEvento().toString();
        String result = instance.getEventoString();
        assertEquals(expResult, result);
    }

    
    /**
     * Test of addOrganizador method, of class CriarEventoCientificoController.
     */
    @Test
    public void test07AddOrganizador() {
        System.out.println("addOrganizador");
        String strId = eventTest.getListaOrganizadores().get(0).getUtilizador().getUsername();
        
        CriarEventoCientificoController instance = controllerCEC;
        e.getRegistoUtilizadores().registaUtilizador(eventTest.getListaOrganizadores().get(0).getUtilizador());
        
        boolean expResult = true;
        boolean result = instance.addOrganizador(strId);
        assertEquals(expResult, result);
    }

    /**
     * Test of registaEvento method, of class CriarEventoCientificoController.
     */
    @Test
    public void test10RegistaEvento() {
        System.out.println("registaEvento");
        CriarEventoCientificoController instance = controllerCEC;
        boolean expResult = true;
        boolean result = instance.registaEvento();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataInicio method, of class CriarEventoCientificoController.
     */
    @Test
    public void test05SetDataInicio() {
        System.out.println("setDataInicio");
        Date dataInicio = eventTest.getDataInicio().getTime();
        CriarEventoCientificoController instance = controllerCEC;
        
        instance.setDataLimiteSubmissao(eventTest.getDataLimiteSubmissao().getTime());
        instance.setDataLimiteRevisao(eventTest.getDataLimiteRevisao().getTime());
        instance.setDataLimiteSubmissaoFinal(eventTest.getDataLimiteSubmissaoFinal().getTime());
        instance.setDataLimiteRegisto(eventTest.getDataLimiteRegisto().getTime());
        
        boolean expResult = true;
        boolean result = instance.setDataInicio(dataInicio);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataFim method, of class CriarEventoCientificoController.
     */
    @Test
    public void test06SetDataFim() {
        System.out.println("setDataFim");
        Date dataFim = eventTest.getDataFim().getTime();
        CriarEventoCientificoController instance = controllerCEC;
        
        instance.setDataLimiteSubmissao(eventTest.getDataLimiteSubmissao().getTime());
        instance.setDataLimiteRevisao(eventTest.getDataLimiteRevisao().getTime());
        instance.setDataLimiteSubmissaoFinal(eventTest.getDataLimiteSubmissaoFinal().getTime());
        instance.setDataLimiteRegisto(eventTest.getDataLimiteRegisto().getTime());
        instance.setDataInicio(eventTest.getDataInicio().getTime());
        
        boolean expResult = true;
        boolean result = instance.setDataFim(dataFim);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataLimiteSubmissao method, of class CriarEventoCientificoController.
     */
    @Test
    public void test01SetDataLimiteSubmissao() {
        System.out.println("setDataLimiteSubmissao");
        Date dataLimiteSubmissao = eventTest.getDataLimiteSubmissao().getTime();
        CriarEventoCientificoController instance = controllerCEC;
        boolean expResult = true;
        boolean result = instance.setDataLimiteSubmissao(dataLimiteSubmissao);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataLimiteSubmissaoFinal method, of class CriarEventoCientificoController.
     */
    @Test
    public void test03SetDataLimiteSubmissaoFinal() {
        System.out.println("setDataLimiteSubmissaoFinal");
        Date dataLimiteSubmissaoFinal = eventTest.getDataLimiteSubmissaoFinal().getTime();
        CriarEventoCientificoController instance = controllerCEC;
        instance.setDataLimiteSubmissao(eventTest.getDataLimiteSubmissao().getTime());
        instance.setDataLimiteRevisao(eventTest.getDataLimiteRevisao().getTime());
        boolean expResult = true;
        boolean result = instance.setDataLimiteSubmissaoFinal(dataLimiteSubmissaoFinal);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataLimiteRevisao method, of class CriarEventoCientificoController.
     */
    @Test
    public void test02SetDataLimiteRevisao() {
        System.out.println("setDataLimiteRevisao");
        Date dataLimiteRevisao = eventTest.getDataLimiteRevisao().getTime();
        CriarEventoCientificoController instance = controllerCEC;
        instance.setDataLimiteSubmissao(eventTest.getDataLimiteSubmissao().getTime());
        boolean expResult = true;
        boolean result = instance.setDataLimiteRevisao(dataLimiteRevisao);
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataLimiteRegisto method, of class CriarEventoCientificoController.
     */
    @Test
    public void test04SetDataLimiteRegisto() {
        System.out.println("setDataLimiteRegisto");
        Date dataLimiteRegisto = eventTest.getDataLimiteSubmissaoFinal().getTime();
        CriarEventoCientificoController instance = controllerCEC;
        instance.setDataLimiteSubmissao(eventTest.getDataLimiteSubmissao().getTime());
        instance.setDataLimiteRevisao(eventTest.getDataLimiteRevisao().getTime());
        instance.setDataLimiteSubmissaoFinal(eventTest.getDataLimiteSubmissaoFinal().getTime());
        boolean expResult = true;
        boolean result = instance.setDataLimiteRegisto(dataLimiteRegisto);
        assertEquals(expResult, result);
    }

    /**
     * Test of getLastOrganizador method, of class CriarEventoCientificoController.
     */
    @Test
    public void test08GetStringOrganizador() {
        System.out.println("getStringOrganizador");
        String strID = eventTest.getListaOrganizadores().get(1).getUtilizador().getUsername();
        
        CriarEventoCientificoController instance = controllerCEC;
        Utilizador u = eventTest.getListaOrganizadores().get(1).getUtilizador();
        e.getRegistoUtilizadores().registaUtilizador(u);
        instance.addOrganizador(strID);
        
        String expResult = String.format("%s (%s) - E-Mail: %s", u.getNome(), u.getUsername(), u.getEmail());
        String result = instance.getStringOrganizador(strID);
        assertEquals(expResult, result);
    }

    /**
     * Test of setWebsite method, of class CriarEventoCientificoController.
     */
    @Test
    public void testSetWebsite() {
        System.out.println("setWebsite");
        String website = eventTest.getWebsite();
        CriarEventoCientificoController instance = controllerCEC;
        boolean expResult = true;
        boolean result = instance.setWebsite(website);
        assertEquals(expResult, result);
    }

    /**
     * Test of getHost method, of class CriarEventoCientificoController.
     */
    @Test
    public void testsetHost() {
        System.out.println("setHost");
        CriarEventoCientificoController instance = controllerCEC;
        String host = eventTest.getHost();
        boolean expResult = true;
        boolean result = instance.setHost(host);
        assertEquals(expResult, result);
    }    
}
