/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.controller;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago
 */
public class MigrarDadosLegacyControllerTest {
    
    public MigrarDadosLegacyControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getFileEventos method, of class MigrarDadosLegacyController.
     */
    @Test
    public void testGetFileEventos() throws IOException {
        System.out.println("getFileEventos");
        File ficheiro= new File("teste.csv");
        MigrarDadosLegacyController instance = new MigrarDadosLegacyController(new Empresa() );
        File expResult = ficheiro;
        instance.setFileEventos(ficheiro);
        File result = instance.getFileEventos();
        assertEquals(expResult, result);
 
    }

    /**
     * Test of getFileArtigos method, of class MigrarDadosLegacyController.
     */
    @Test
    public void testGetFileArtigos() {
        System.out.println("getFileArtigos");
        File ficheiro= new File("teste.csv");
        MigrarDadosLegacyController instance = new MigrarDadosLegacyController(new Empresa() );
        File expResult = ficheiro;
        instance.setFileArtigos(ficheiro);
        File result = instance.getFileArtigos();
        assertEquals(expResult, result);
    }

    /**
     * Test of getFileRevisoes method, of class MigrarDadosLegacyController.
     */
    @Test
    public void testGetFileRevisoes() {
        System.out.println("getFileRevisoes");
        File ficheiro= new File("teste.csv");
        MigrarDadosLegacyController instance = new MigrarDadosLegacyController(new Empresa() );
        File expResult = ficheiro;
        instance.setFileRevisoes(ficheiro);
        File result = instance.getFileRevisoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFileEventos method, of class MigrarDadosLegacyController.
     */
    @Test
    public void testSetFileEventos() throws Exception {
        System.out.println("setFileEventos");
        File ficheiro= new File("teste.csv");
        MigrarDadosLegacyController instance = new MigrarDadosLegacyController(new Empresa() );
        File expResult = ficheiro;
        instance.setFileEventos(ficheiro);
        File result = instance.getFileEventos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFileArtigos method, of class MigrarDadosLegacyController.
     */
    @Test
    public void testSetFileArtigos() {
        System.out.println("setFileArtigos");
        File ficheiro= new File("teste.csv");
        MigrarDadosLegacyController instance = new MigrarDadosLegacyController(new Empresa() );
        File expResult = ficheiro;
        instance.setFileArtigos(ficheiro);
        File result = instance.getFileArtigos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFileRevisoes method, of class MigrarDadosLegacyController.
     */
    @Test
    public void testSetFileRevisoes() {
        System.out.println("setFileRevisoes");
       File ficheiro= new File("teste.csv");
        MigrarDadosLegacyController instance = new MigrarDadosLegacyController(new Empresa() );
        File expResult = ficheiro;
        instance.setFileRevisoes(ficheiro);
        File result = instance.getFileRevisoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getHg_legacy method, of class MigrarDadosLegacyController.
     */
    @Test
    public void testGetHg_legacy() {
        System.out.println("getHg_legacy");
        MigrarDadosLegacyController instance = new MigrarDadosLegacyController(new Empresa());
        HashMap<Evento, String> hashTeste = new HashMap<>();
        hashTeste.put(new Evento(), "DERP");
        instance.setHg_legacy(hashTeste);
        HashMap<Evento, String> expResult = hashTeste;
        HashMap<Evento, String> result = instance.getHg_legacy();
        assertEquals(expResult, result);

    }

    /**
     * Test of setHg_legacy method, of class MigrarDadosLegacyController.
     */
    @Test
    public void testSetHg_legacy() {
        System.out.println("setHg_legacy");
       MigrarDadosLegacyController instance = new MigrarDadosLegacyController(new Empresa());
        HashMap<Evento, String> hashTeste = new HashMap<>();
        hashTeste.put(new Evento(), "DERP");
        instance.setHg_legacy(hashTeste);
        HashMap<Evento, String> expResult = hashTeste;
        HashMap<Evento, String> result = instance.getHg_legacy();
        assertEquals(expResult, result);
    }
    
}
