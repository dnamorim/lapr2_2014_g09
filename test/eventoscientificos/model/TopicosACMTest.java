/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author dnamorim
 */
public class TopicosACMTest {
    
    private Topico t1, t2;
    
    public TopicosACMTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() throws IOException {
        TopicosACM.getListaTopicosACM().clear();
        TopicosACM.saveTopicosACM();
    }
    
    @Before
    public void setUp() {
        t1 = new Topico("B.6.1", "Logic Design/ Design Styles");
        t2 = new Topico("B.3.2", "MEMORY STRUCTURES/ Design Styles");
        TopicosACM.getListaTopicosACM().clear();
    }
    
    @After
    public void tearDown() {
        TopicosACM.getListaTopicosACM().clear();
    }
/**
     * Test of novoTopico method, of class TopicosACM.
     */
    @Test
    public void test00NovoTopico() {
        System.out.println("novoTopico");
        Topico expResult = new Topico();
        Topico result = TopicosACM.novoTopico();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of isVazio method, of class TopicosACM.
     */
    @Test
    public void test01IsVazio() {
        System.out.println("isVazio");
        boolean expResult = true;
        boolean result = TopicosACM.isVazio();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of addTopico method, of class TopicosACM.
     */
    @Test
    public void test02AddTopico() {
        System.out.println("addTopico");
        Topico t = t1;
        TopicosACM.addTopico(t);
        int pos = TopicosACM.getListaTopicosACM().size() - 1;
        assertEquals(t, TopicosACM.getListaTopicosACM().get(pos));
    }

    /**
     * Test of getListaTopicosACM method, of class TopicosACM.
     */
    @Test
    public void test03GetListaTopicosACM() {
        System.out.println("getListaTopicosACM");
        List<Topico> expResult = new ArrayList<Topico>();
        expResult.add(t1);
        expResult.add(t2);
        
        TopicosACM.addTopico(t1);
        TopicosACM.addTopico(t2);
        List<Topico> result = TopicosACM.getListaTopicosACM();
        assertEquals(expResult, result);
    }

    /**
     * Test of isVazio method, of class TopicosACM.
     */
    @Test
    public void test04IsVazioNot() {
        System.out.println("isVazio not");
        TopicosACM.addTopico(t1);
        TopicosACM.addTopico(t2);
        boolean expResult = false;
        boolean result = TopicosACM.isVazio();
        assertEquals(expResult, result);
    }

    /**
     * Test of obterArrayTopicos method, of class TopicosACM.
     */
    @Test
    public void test05ObterArrayTopicos() {
        System.out.println("obterArrayTopicos");
        Topico[] expResult = {t1, t2};
        TopicosACM.addTopico(t1);
        TopicosACM.addTopico(t2);
        Topico[] result = TopicosACM.obterArrayTopicos();
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of valida method, of class TopicosACM.
     */
    @Test
    public void test06Valida() {
        System.out.println("valida");
        String codigo = "B.3.2";
        TopicosACM.addTopico(t1);
        TopicosACM.addTopico(t2);
        boolean expResult = true;
        boolean result = TopicosACM.valida(codigo);
        assertEquals(expResult, result);
    }
    
     /**
     * Test of valida method, of class TopicosACM.
     */
    @Test
    public void test07ValidaNot() {
        System.out.println("valida not");
        String codigo = "B.3.4";
        TopicosACM.addTopico(t1);
        TopicosACM.addTopico(t2);
        boolean expResult = false;
        boolean result = TopicosACM.valida(codigo);
        assertEquals(expResult, result);
    }

    /**
     * Test of existeTopico method, of class TopicosACM.
     */
    @Test
    public void test08ExisteTopico() {
        System.out.println("existeTopico");
        Topico t = t2;
        TopicosACM.addTopico(t1);
        TopicosACM.addTopico(t2);
        boolean expResult = true;
        boolean result = TopicosACM.existeTopico(t);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of existeTopico method, of class TopicosACM.
     */
    @Test
    public void test09ExisteTopicoNot() {
        System.out.println("existeTopico not");
        Topico t = new Topico("B.4.3", "DataBases / Test");
        TopicosACM.addTopico(t1);
        TopicosACM.addTopico(t2);
        boolean expResult = false;
        boolean result = TopicosACM.existeTopico(t);
        assertEquals(expResult, result);
    }

    /**
     * Test of getTopicoByCodigoACM method, of class TopicosACM.
     */
    @Test
    public void test10GetTopicoByCodigoACM() {
        System.out.println("getTopicoByCodigoACM");
        String codigo = "B.6.1";
        Topico expResult = t1;
        TopicosACM.addTopico(t1);
        TopicosACM.addTopico(t2);
        Topico result = TopicosACM.getTopicoByCodigoACM(codigo);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getTopicoByCodigoACM method, of class TopicosACM.
     */
    @Test
    public void test11GetTopicoByCodigoACMNotExists() {
        System.out.println("getTopicoByCodigoACM not exists");
        String codigo = "B.5.1";
        TopicosACM.addTopico(t1);
        TopicosACM.addTopico(t2);
        Topico result = TopicosACM.getTopicoByCodigoACM(codigo);
        assertNull(result);
    } 
    
    /**
     * Test of saveTopicosACM method, of class TopicosACM.
     */
    @Test
    public void test12ReadSaveTopicosACM() throws Exception {
        System.out.println("saveTopicosACM");
        TopicosACM.addTopico(t1);
        TopicosACM.addTopico(t2);
        
        List<Topico> expResult = new ArrayList<Topico>();
        for (Topico topico : TopicosACM.getListaTopicosACM()) {
            expResult.add(topico);
        }
        
        TopicosACM.saveTopicosACM();
        //limpar tópicos do ArrayList
        TopicosACM.getListaTopicosACM().clear();
        TopicosACM.readTopicosACM();
        assertEquals(expResult, TopicosACM.getListaTopicosACM());
    }
    
    
}
