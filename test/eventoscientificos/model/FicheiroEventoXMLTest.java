/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tiago
 */
public class FicheiroEventoXMLTest {

    public FicheiroEventoXMLTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getFile method, of class FicheiroEventoXML.
     */
    @Test
    public void testGetFile() {
        System.out.println("getFile");
        FicheiroEventoXML instance = new FicheiroEventoXML();
        File ficheiro = new File("EventList_CDIO.xml");
        instance.setFile(ficheiro);
        File expResult = ficheiro;
        File result = instance.getFile();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFile method, of class FicheiroEventoXML.
     */
    @Test
    public void testSetFile() {
        System.out.println("setFile");
        FicheiroEventoXML instance = new FicheiroEventoXML();
        File ficheiro = new File("EventList_CDIO.xml");
        instance.setFile(ficheiro);
        File expResult = ficheiro;
        File result = instance.getFile();
        assertEquals(expResult, result);
    }

    /**
     * Test of lerFile method, of class FicheiroEventoXML.
     */
    @Test
    public void testLerFile() {
        System.out.println("lerFile");
        FicheiroEventoXML instance = new FicheiroEventoXML();
        File ficheiro = new File("teste.xml");
        instance.setFile(ficheiro);
        List<String> expResult = new ArrayList<>();
        expResult.add("2013 Latin American Regional CDIO Meeting;Universidad de Chile;Santiago de Chile;Chile;Monday, April 1, 2013;Wednesday, April 3, 2013;www.fs.com");
        List<String> result = instance.lerFile();
        assertEquals(expResult, result);
    }

    /**
     * Test of criarListaEventos method, of class FicheiroEventoXML.
     */
    @Test
    public void testCriarListaEventos() {
        System.out.println("criarListaEventos");
        List<String> lista = new ArrayList<>();
        lista.add("2013 Latin American Regional CDIO Meeting;Universidad de Chile;Santiago de Chile;Chile;Monday, April 1, 2013;Wednesday, April 3, 2013;www.fs.com");
        File ficheiro = new File("teste.xml");
        FicheiroEventoXML instance = new FicheiroEventoXML();
        instance.setFile(ficheiro);
        Evento e = new Evento();
        e.setTitulo("2013 Latin American Regional CDIO Meeting");
        e.setHost("Universidad de Chile");
        e.getLocal().setCidade("Santiago de Chile");
        e.getLocal().setPais("Chile");

        Calendar dataInicio = new GregorianCalendar();
        dataInicio.set(Calendar.YEAR, 2013);
        dataInicio.set(Calendar.MONTH, 3);
        dataInicio.set(Calendar.DAY_OF_MONTH, 1);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        e.setDataInicio(dataInicio);

        Calendar dataFim = new GregorianCalendar();
        dataFim.set(Calendar.YEAR, 2013);
        dataFim.set(Calendar.MONTH, 3);
        dataFim.set(Calendar.DAY_OF_MONTH, 3);
        SimpleDateFormat sdd = new SimpleDateFormat("dd/MM/yyyy");
        e.setDataFim(dataFim);
        e.setWebsite("www.fs.com");
        List<Evento> expResult = new ArrayList<>();
        expResult.add(e);
        List<Evento> result = instance.criarListaEventos(lista);
        assertEquals(expResult.toString(), result.toString());
    }

}
