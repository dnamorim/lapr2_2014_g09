/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dnamorim
 */
public class OrganizadorTest {
    
    public OrganizadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    private Utilizador u = new Utilizador("BuckR", "letMeIn123", "Buck Rogers", "buckr@nasa.gov");
    
    /**
     * Test of valida method, of class Organizador.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        
        Organizador instance = new Organizador(u);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNome method, of class Organizador.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Organizador instance = new Organizador(u);
        String expResult = u.getNome();
        String result = instance.getNome();
        assertEquals(expResult, result);
    }

    /**
     * Test of getUtilizador method, of class Organizador.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        Organizador instance = new Organizador(u);
        Utilizador expResult = new Utilizador("BuckR", "letMeIn123", "Buck Rogers", "buckr@nasa.gov");
        Utilizador result = instance.getUtilizador();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of toString method, of class Organizador.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Organizador instance = new Organizador(u);
        String expResult = u.toString();
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Revisor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object otherObj = new Organizador(new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt"));
        Organizador instance = new Organizador(new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt"));
        boolean expResult = true;
        boolean result = instance.equals(otherObj);
        assertEquals(expResult, result);
    }
    
        /**
     * Test of equals method, of class Revisor.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("equals not");
        Organizador otherObj = new Organizador(new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt"));
        Organizador instance = new Organizador(new Utilizador("Pedro Amorim", "pamorim", "letmein", "pda@isep.ipp.pt"));
        boolean expResult = false;
        boolean result = instance.equals(otherObj);
        assertEquals(expResult, result);
    }

    
}
