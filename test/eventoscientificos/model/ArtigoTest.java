/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import eventoscientificos.model.Topico;
import eventoscientificos.model.Utilizador;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author amartins
 */
public class ArtigoTest {
   
    private Empresa empresa;
    private Revisor revisor;
    private Artigo artigo;
    
    public ArtigoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setTitulo method, of class Artigo.
     */
    @Test
    public void testSetGetTitulo() {
        System.out.println("setTitulo");
        String strTitulo = "As 1001 vantagens de fazer testes";
        Artigo instance = new Artigo();
        instance.setTitulo(strTitulo);
        assertEquals(strTitulo, instance.getTitulo());
    }

    /**
     * Test of setResumo method, of class Artigo.
     */
    @Test
    public void testSetGetResumo() {
        System.out.println("setResumo");
        String strResumo = "O resumo de um artigo";
        Artigo instance = new Artigo();
        instance.setResumo(strResumo);
        assertEquals(strResumo, instance.getResumo());
    }

    /**
     * Test of novoAutor method, of class Artigo.
     */
    @Test
    public void testNovoAutor_3args() {
        System.out.println("novoAutor");
        String strNome = "Duarte Amorim";
        String strAfiliacao = "ISEP";
        String strEmail = "1130674@isep.ipp.pt";
        Artigo instance = new Artigo();
        Autor expResult = new Autor("Duarte Amorim", "ISEP", "1130674@isep.ipp.pt");
        Autor result = instance.novoAutor(strNome, strAfiliacao, strEmail);
        assertEquals(expResult, result);
    }

    /**
     * Test of novoAutor method, of class Artigo.
     */
    @Test
    public void testNovoAutor_2args() {
        System.out.println("novoAutor");
        Utilizador user = new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt");
        String strNome = user.getNome();
        String strAfiliacao = "ISEP";
        String strEmail = user.getEmail();
        Artigo instance = new Artigo();
        Autor expResult = new Autor(user, strAfiliacao);
        Autor result = instance.novoAutor(strAfiliacao, user);
        assertEquals(expResult, result);
    }

    /**
     * Test of addAutor method, of class Artigo.
     */
    @Test
    public void testAddAutor() {
        System.out.println("addAutor");
        Autor autor = new Autor("Duarte Amorim", "ISEP", "1130674@isep.ipp.pt");
        Artigo instance = new Artigo();
        boolean expResult = true;
        boolean result = instance.addAutor(autor);
        assertEquals(expResult, result);
    }

    /**
     * Test of getPossiveisAutoresCorrespondentes method, of class Artigo.
     */
    @Test
    public void testGetPossiveisAutoresCorrespondentes() {
        System.out.println("getPossiveisAutoresCorrespondentes");
        Utilizador u = new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt");
        Autor a1 = new Autor(u, "ISEP");
        Autor a2 = new Autor("Ângelo Martins", "ISEP", "amm@isep.ipp.pt");
        Artigo instance = new Artigo();
        instance.addAutor(a1);
        instance.addAutor(a2);
        
        List<Autor> expResult = new ArrayList<Autor>();
        expResult.add(a1);
        List<Autor> result = instance.getPossiveisAutoresCorrespondentes();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAutorCorrespondente method, of class Artigo.
     */
    @Test
    public void testSetGetAutorCorrespondente() {
        System.out.println("set e getAutorCorrespondente");
        Utilizador u = new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt");
        Autor a1 = new Autor(u, "ISEP");
        Artigo instance = new Artigo();
        instance.addAutor(a1);
        
        instance.setAutorCorrespondente(a1);
        Autor expResult = a1;
        assertEquals(expResult, instance.getAutorCorrespondente());
    }

    /**
     * Test of setFicheiro method, of class Artigo.
     */
    @Test
    public void testSetGetFicheiro() {
        System.out.println("set e getFicheiro");
        String m_Ficheiro = "/Users/TOCS/JUnitArticle.pdf";
        Artigo instance = new Artigo();
        instance.setFicheiro(m_Ficheiro);
        String expResult = m_Ficheiro;
        assertEquals(expResult, instance.getFicheiro());
    }

    /**
     * Test of setListaTopicos method, of class Artigo.
     */
    @Test
    public void testSetGetListaTopicos() {
        System.out.println("set e getListaTopicos");
        Topico t1 = new Topico("H.2.1", "Logical Design---Schema and subschema");
        Topico t2 = new Topico("H.2.3", "[Database Management]: Languages---SQL");
        
        List<Topico> listaTopicos = new ArrayList<Topico>();
        listaTopicos.add(t1);
        listaTopicos.add(t2);
        
        Artigo instance = new Artigo();
        instance.setListaTopicos(listaTopicos);
        
        //Clonagem da Lista para não partilhar referências de memória
        List<Topico> expResult = new ArrayList<Topico>(listaTopicos.size());
        for (Topico t : listaTopicos) {
            expResult.add(new Topico(t.getCodigoACM(), t.getDescricao()));
        }
        
        assertEquals(expResult, instance.getListaTopicos());
    }

    /**
     * Test of getInfo method, of class Artigo.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        Artigo instance = new Artigo();
        instance.setTitulo("TDD and JUnit");
        instance.setResumo("Um estudo de caso sobre o Test-Driven Development em conjunto com o JUnit do JAVA.");
        instance.setTipo(TipoArtigo.SHORT);
        instance.setFicheiro("/Users/TOCS/TDD_JUnit_paper.pdf");
        String expResult = String.format("Artigo:%nTítulo: %s%nResumo: %s%nTipo: %s Ficheiro: %s", instance.getTitulo(), instance.getResumo(), instance.getTipo().toString(), instance.getFicheiro());
        String result = instance.getInfo();
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Artigo.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Artigo instance = new Artigo();
        instance.setTitulo("TDD and JUnit");
        instance.setResumo("Um estudo de caso sobre o Test-Driven Development em conjunto com o JUnit do JAVA.");
        instance.setTipo(TipoArtigo.SHORT);
        instance.setFicheiro("/Users/TOCS/TDD_JUnit_paper.pdf");
        
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Artigo.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Artigo instance = new Artigo();
        instance.setTitulo("TDD and JUnit");
        instance.setResumo("Um estudo de caso sobre o Test-Driven Development em conjunto com o JUnit do JAVA.");
        instance.setTipo(TipoArtigo.SHORT);
        instance.setFicheiro("/Users/TOCS/TDD_JUnit_paper.pdf");
        
        
        String expResult = String.format("Artigo:%nTítulo: %s%nResumo: %s%nTipo: %s Ficheiro: %s", instance.getTitulo(), instance.getResumo(), instance.getTipo().toString(), instance.getFicheiro());
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Artigo.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Utilizador myUt = new Utilizador();
        myUt.setUsername("expert@isep.ipp.pt");
        myUt.setEmail("expert@isep.ipp.pt");
        Autor act = new Autor();
        act.setUtilizador(myUt);
        
        Artigo obj = new Artigo();
        obj.setTitulo("A beleza do JUnit");
        obj.setAutorCorrespondente(act);
        Artigo instance = new Artigo();
        instance.setTitulo("A beleza do JUnit");
        instance.setAutorCorrespondente(act);
        
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    /**
     * Test of setTipo method, of class Artigo.
     */
    @Test
    public void testSetGetTipo() {
        System.out.println("set e getTipo");
        TipoArtigo tipo = TipoArtigo.FULL;
        Artigo instance = new Artigo();
        instance.setTipo(tipo);
        TipoArtigo expResult = tipo;
        assertEquals(expResult, instance.getTipo());
    }    

    /**
     * Test of setTitulo method, of class Artigo.
     */
    @Test
    public void testSetTitulo() {
        System.out.println("setTitulo");
        String strTitulo = "";
        Artigo instance = new Artigo();
        instance.setTitulo(strTitulo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTitulo method, of class Artigo.
     */
    @Test
    public void testGetTitulo() {
        System.out.println("getTitulo");
        Artigo instance = new Artigo();
        String expResult = "";
        String result = instance.getTitulo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setResumo method, of class Artigo.
     */
    @Test
    public void testSetResumo() {
        System.out.println("setResumo");
        String strResumo = "";
        Artigo instance = new Artigo();
        instance.setResumo(strResumo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getResumo method, of class Artigo.
     */
    @Test
    public void testGetResumo() {
        System.out.println("getResumo");
        Artigo instance = new Artigo();
        String expResult = "";
        String result = instance.getResumo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setTipo method, of class Artigo.
     */
    @Test
    public void testSetTipo() {
        System.out.println("setTipo");
        TipoArtigo tipo = null;
        Artigo instance = new Artigo();
        instance.setTipo(tipo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTipo method, of class Artigo.
     */
    @Test
    public void testGetTipo() {
        System.out.println("getTipo");
        Artigo instance = new Artigo();
        TipoArtigo expResult = null;
        TipoArtigo result = instance.getTipo();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of novoAutor method, of class Artigo.
     */
    @Test
    public void testNovoAutor_String_Utilizador() {
        System.out.println("novoAutor");
        String strAfiliacao = "";
        Utilizador utilizador = null;
        Artigo instance = new Artigo();
        Autor expResult = null;
        Autor result = instance.novoAutor(strAfiliacao, utilizador);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAutorCorrespondente method, of class Artigo.
     */
    @Test
    public void testSetAutorCorrespondente() {
        System.out.println("setAutorCorrespondente");
        Autor autor = null;
        Artigo instance = new Artigo();
        instance.setAutorCorrespondente(autor);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getAutorCorrespondente method, of class Artigo.
     */
    @Test
    public void testGetAutorCorrespondente() {
        System.out.println("getAutorCorrespondente");
        Artigo instance = new Artigo();
        Autor expResult = null;
        Autor result = instance.getAutorCorrespondente();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setFicheiro method, of class Artigo.
     */
    @Test
    public void testSetFicheiro() {
        System.out.println("setFicheiro");
        String pathArtigo = "";
        Artigo instance = new Artigo();
        instance.setFicheiro(pathArtigo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFicheiro method, of class Artigo.
     */
    @Test
    public void testGetFicheiro() {
        System.out.println("getFicheiro");
        Artigo instance = new Artigo();
        String expResult = "";
        String result = instance.getFicheiro();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setListaTopicos method, of class Artigo.
     */
    @Test
    public void testSetListaTopicos() {
        System.out.println("setListaTopicos");
        List<Topico> listaTopicos = null;
        Artigo instance = new Artigo();
        instance.setListaTopicos(listaTopicos);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getListaTopicos method, of class Artigo.
     */
    @Test
    public void testGetListaTopicos() {
        System.out.println("getListaTopicos");
        Artigo instance = new Artigo();
        List<Topico> expResult = null;
        List<Topico> result = instance.getListaTopicos();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of hashCode method, of class Artigo.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Artigo instance = new Artigo();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getRevisoes method, of class Artigo.
     */
    @Test
    public void testSetGetRevisoes() {
        System.out.println("getRevisoes");
        Artigo instance = new Artigo();
        Revisao r1 = new Revisao(artigo,revisor);
        List<Revisao> re = new ArrayList();
        re.add(r1);
        instance.setRevisoes(re);
        List<Revisao> expResult = new ArrayList(re.size());
        for (Revisao r : re) {
            expResult.add(new Revisao(artigo,revisor));
        }
        List<Revisao> result = instance.getRevisoes();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
 



}
