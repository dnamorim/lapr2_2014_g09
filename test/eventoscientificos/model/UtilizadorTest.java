/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import eventoscientificos.model.Utilizador;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class UtilizadorTest {
    
    public UtilizadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setNome and getNome method, of class Utilizador.
     */
    @Test
    public void testSetGetNome() {
        System.out.println("setGetNome");
        String strNome = "Buck Rogers";
        Utilizador instance = new Utilizador();
        instance.setNome(strNome);
        String result = instance.getNome();
        String expResult = "Buck Rogers";
        assertEquals(expResult, result);
    }

    /**
     * Test of setUsername and getUsername method, of class Utilizador.
     */
    @Test
    public void testSetGetUsername() {
        System.out.println("setGetUsername");
        String strUsername = "BuckR";
        Utilizador instance = new Utilizador();
        instance.setUsername(strUsername);
        String expResult = "BuckR";
        String result = instance.getUsername();
        assertEquals(expResult, result);
    }

    /**
     * Test of setPassword and getPassword method, of class Utilizador.
     */
    @Test
    public void testSetGetPassword() {
        System.out.println("setGetPassword");
        String strPassword = "letMeIn123";
        Utilizador instance = new Utilizador();
        instance.setPassword(strPassword);
        String expResult = "letMeIn123";
        String result = instance.getPassword();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEmail and getEmail method, of class Utilizador.
     * 
     * Devem ser feitas as validações de negócio aquando do registo de alterações dos dados
     * email tem de ser sempre em minúsculas, não acentuado, sem espaços no meio e conter @
     */
    @Test
    public void testSetGetEmailValid() {
        System.out.println("setEmail");
        String strEmail = "buckr@nasa.gov";
        Utilizador instance = new Utilizador();
        instance.setEmail(strEmail);
        String expResult = "buckr@nasa.gov";
        String result = instance.getEmail();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of valida method, of class Utilizador.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Utilizador instance = new Utilizador();
        String strEmail = "buckr@nasa.gov";
        instance.setEmail(strEmail);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of valida method, of class Utilizador.
     */
    @Test
    public void testValidaNot() {
        System.out.println("valida - false");
        Utilizador instance = new Utilizador();
        String strEmail = " BucKR@NASA.gov ";
        instance.setEmail(strEmail);
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of mesmoQueUtilizador method, of class Utilizador.
     */
    @Test
    public void testMesmoQueUtilizador() {
        System.out.println("mesmoQueUtilizador");
        Utilizador u = new Utilizador("BuckR", "letMeIn123", "Buck Rogers", "buckr@nasa.gov");
        Utilizador instance = new Utilizador("buckr", "beentheredonethat", "Buck Rodgers", "buckr@nasa.gov");
        boolean expResult = true;
        boolean result = instance.mesmoQueUtilizador(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of mesmoQueUtilizador method, of class Utilizador.
     */
    @Test
    public void testMesmoQueUtilizadorNot() {
        System.out.println("mesmoQueUtilizadorNot");
        Utilizador u = new Utilizador("BuckR", "letMeIn123", "Buck Rogers", "buckr@nasa.gov");
        Utilizador instance = new Utilizador("rrabit", "beentheredonethat", "Roger Rabbit", "rrabit@nasa.gov");
        boolean expResult = false;
        boolean result = instance.mesmoQueUtilizador(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Utilizador.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Utilizador instance = new Utilizador("BuckR", "letMeIn123", "Buck Rogers", "buckr@nasa.gov");
        String expResult = String.format("Utilizador:%nNome: %s Username: %s%nE-Mail: %s Password: %s", "Buck Rogers", "BuckR", "buckr@nasa.gov", "letMeIn123");
        String result = instance.toString();
        assertEquals(expResult, result);
        
    }

    /**
     * Test of equals method, of class Utilizador.
     */
    @Test
    public void testEquals() {
        System.out.println("equals - same username");
        Object obj = new Utilizador("buckr","", "Buck Rogers", "buckr@gmail.com");
        Utilizador instance = new Utilizador("buckr","1234", "Buck Rogers", "buckr@nasa.gov");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class Utilizador.
     */
    @Test
    public void testEqualsSameEmail() {
        System.out.println("equals - same email");
        Object obj = new Utilizador("bucky","", "Buck", "buckr@nasa.gov");
        Utilizador instance = new Utilizador("buckr","1234", "Buck Rogers", "buckr@nasa.gov");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class Utilizador.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("equals not");
        Object obj = new Utilizador("bucky","", "Buck Rogers", "buckr@gmail.com");
        Utilizador instance = new Utilizador("buckr","1234", "Buck Rogers", "buckr@nasa.gov");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
}
