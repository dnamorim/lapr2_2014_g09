/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author i130672
 */
public class FicheiroEventoCSVTest {

    public FicheiroEventoCSVTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getFile method, of class FicheiroEventoCSV.
     */
    @Test
    public void testGetFile() {
        System.out.println("getFile");
        FicheiroEventoCSV instance = new FicheiroEventoCSV();
        File ficheiro = new File("teste.csv");
        instance.setFile(ficheiro);
        File expResult = ficheiro;
        File result = instance.getFile();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFile method, of class FicheiroEventoCSV.
     */
    @Test
    public void testSetFile() {
        System.out.println("setFile");
        FicheiroEventoCSV instance = new FicheiroEventoCSV();
        File ficheiro = new File("teste.csv");
        instance.setFile(ficheiro);
        File expResult = ficheiro;
        File result = instance.getFile();
        assertEquals(expResult, result);
    }

    /**
     * Test of lerFile method, of class FicheiroEventoCSV.
     */
    @Test
    public void testLerFile() {
        System.out.println("lerFile");
        FicheiroEventoCSV instance = new FicheiroEventoCSV();
        instance.setFile(new File("teste.csv"));
        List<String> expResult = new ArrayList<>();
        expResult.add("a;b;c;d");
        expResult.add("1;2;3;4");
        List<String> result = instance.lerFile();
        assertEquals(expResult, result);
    }

    /**
     * Test of criarListaEventos method, of class FicheiroEventoCSV.
     */
    @Test
    public void testCriarListaEventos() {
        System.out.println("criarListaEventos");
        FicheiroEventoCSV instance = new FicheiroEventoCSV();

        instance.setFile(new File("teste_dois_eventos.csv"));
        List<String> lista = instance.lerFile();
        Evento evento = new Evento();

        evento.setTitulo("12th International CDIO Conference, Turku, Finland");
        evento.setHost("Turku University of Applied Sciences");
        evento.getLocal().setCidade("Turku");
        evento.getLocal().setPais("Finland");

        Calendar dataInicio = new GregorianCalendar();
        dataInicio.set(Calendar.YEAR, 2016);
        dataInicio.set(Calendar.MONTH, 5);
        dataInicio.set(Calendar.DAY_OF_MONTH, 12);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        evento.setDataInicio(dataInicio);

        Calendar dataFim = new GregorianCalendar();
        dataFim.set(Calendar.YEAR, 2016);
        dataFim.set(Calendar.MONTH, 5);
        dataFim.set(Calendar.DAY_OF_MONTH, 17);
        SimpleDateFormat sdd = new SimpleDateFormat("dd/MM/yyyy");
        evento.setDataFim(dataFim);

        evento.setWebsite("http://www.cdio.fi/");

        Utilizador util = new Utilizador();
        util.setNome("a");
        util.setEmail("b");
        util.setUsername("b");
        util.setPassword("letmein");
        evento.addOrganizador(util);
        
        List<Evento> expResult = new ArrayList<>();
        expResult.add(evento);
        List<Evento> result = instance.criarListaEventos(lista);
        assertEquals(expResult.toString(), result.toString());
    }

}
