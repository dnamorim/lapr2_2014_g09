/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import eventoscientificos.model.EventoState.EventoRegistadoState;
import eventoscientificos.model.EventoState.EventoState;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author i130672
 */
public class EventoTest {
    private Evento eventTest;
    
    public EventoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        eventTest = new Evento();
        eventTest.setTitulo("2013 9th International CDIO Conference, MIT, USA");
        eventTest.setDescricao("2013 9th International CDIO Conference, MIT, USA");
        Local l = new Local();
        l.setCidade("Cambridge, MA");
        l.setPais("United States");
        eventTest.setHost("MIT and Harvard School of Engineering and Applied Sciences");
        eventTest.setLocal(l);
        eventTest.setDataInicio(new GregorianCalendar(2013, 6,9));
        eventTest.setDataFim(new GregorianCalendar(2013, 6,13));
        eventTest.setDataLimiteRegisto(new GregorianCalendar(2013, 6, 1));
        eventTest.setDataLimiteRevisao(new GregorianCalendar(2013, 4, 1));
        eventTest.setDataLimiteSubmissao(new GregorianCalendar(2013, 3, 20));
        eventTest.setDataLimiteSubmissaoFinal(new GregorianCalendar(2013, 5, 30));
        eventTest.setWebsite("http://laspau.org/cdio2013/");
        eventTest.addOrganizador(new Utilizador("cdio2013@mit.edu", "letmein", "Doris Brodeur", "cdio2013@mit.edu"));
        eventTest.addOrganizador(new Utilizador("revere@harvard.edu", "letmein", "Paul Revere", "revere@harvard.edu"));
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of novaCP method, of class Evento.
     */
    @Test
    public void testNovaCP() {
        System.out.println("novaCP");
        Evento instance = new Evento();
        CP result = instance.novaCP();
        Utilizador util = new Utilizador();
        util.setNome("Tiago");
        result.addMembroCP("user1", util);
        CP expResult = instance.getCP();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTitulo method, of class Evento.
     */
    @Test
    public void testSetTitulo() {
        System.out.println("setTitulo");
        String strTitulo = "EVENTO1";
        Evento instance = new Evento();
        instance.setTitulo(strTitulo);
        String result = instance.getTitulo();
        String expResult = "EVENTO1";
        assertEquals(expResult, result);
    }

    /**
     * Test of setDescricao method, of class Evento.
     */
    @Test
    public void testSetDescricao() {
        System.out.println("setDescricao");
        String strDescricao = "é um evento";
        Evento instance = new Evento();
        instance.setDescricao(strDescricao);
        String result = instance.getDescricao();
        String expResult = "é um evento";
        assertEquals(expResult, result);

    }

    /**
     * Test of setDataInicio method, of class Evento.
     */
    @Test
    public void testSetDataInicio() {
        System.out.println("setDataInicio");
        Calendar dataInicio = new GregorianCalendar();
        dataInicio.set(Calendar.YEAR, 1995);
        dataInicio.set(Calendar.MONTH, 8);
        dataInicio.set(Calendar.DAY_OF_MONTH, 17);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Evento instance = new Evento();
        instance.setDataInicio(dataInicio);
        Calendar result = dataInicio;
        Calendar expResult = instance.getDataInicio();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataFim method, of class Evento.
     */
    @Test
    public void testSetDataFim() {
        System.out.println("setDataFim");
        Calendar dataFim = new GregorianCalendar();
        dataFim.set(Calendar.YEAR, 1995);
        dataFim.set(Calendar.MONTH, 8);
        dataFim.set(Calendar.DAY_OF_MONTH, 17);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Evento instance = new Evento();
        instance.setDataFim(dataFim);
        Calendar result = dataFim;
        Calendar expResult = instance.getDataFim();
        assertEquals(expResult, result);
    }

    /**
     * Test of setDataLimiteSubmissao method, of class Evento.
     */
    @Test
    public void testSetDataLimiteSubmissao() {
        System.out.println("setDataLimiteSubmissao");
        Calendar dataSubmissao = new GregorianCalendar();
        dataSubmissao.set(Calendar.YEAR, 1995);
        dataSubmissao.set(Calendar.MONTH, 8);
        dataSubmissao.set(Calendar.DAY_OF_MONTH, 17);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Evento instance = new Evento();
        instance.setDataLimiteSubmissao(dataSubmissao);
        Calendar result = dataSubmissao;
        Calendar expResult = instance.getDataLimiteSubmissao();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLocal method, of class Evento.
     */
    @Test
    public void testGetLocal() {
        System.out.println("getLocal");
        Evento instance = new Evento();
        Local teste = new Local();
        teste.setCidade("Porto");
        teste.setPais("Portugal");
        Local result = null;
        instance.setLocal(teste);
        Local expResult = teste;
        result = instance.getLocal();
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaOrganizadores method, of class Evento.
     */
    @Test
    public void testGetListaOrganizadores() {
        System.out.println("getListaOrganizadores");
        Evento instance = new Evento();
        Utilizador util = new Utilizador();
        util.setNome("Tiago");
        Organizador org = new Organizador(util);
        List<Organizador> lista_teste = new ArrayList<>();
        lista_teste.add(org);
        instance.setM_listaOrganizadores(lista_teste);
        List<Organizador> expResult = lista_teste;
        List<Organizador> result = instance.getListaOrganizadores();
        assertEquals(expResult, result);
    }

    /**
     * Test of addOrganizador method, of class Evento.
     */
    @Test
    public void testAddOrganizador() {
        System.out.println("addOrganizador");
        Utilizador u = new Utilizador();
        u.setNome("Tiago");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.addOrganizador(u);
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Evento.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        boolean expResult = true;
        boolean result = eventTest.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCP method, of class Evento.
     */
    @Test
    public void testSetCP() {
        System.out.println("setCP");
        Evento instance = new Evento();
        CP cp = instance.novaCP();
        Utilizador util = new Utilizador();
        util.setNome("Tiago");
        cp.addMembroCP("user1", util);
        instance.setCP(cp);
        CP expResult = cp;
        CP result = instance.getCP();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Evento.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Evento instance = new Evento();
        instance.setTitulo("EVENTO1");
        String expResult = "EVENTO1+ ...";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of aceitaSubmissoes method, of class Evento.
     */
    @Test
    public void testAceitaSubmissoes() {
        System.out.println("aceitaSubmissoes");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.aceitaSubmissoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of novaSubmissao method, of class Evento.
     */
    @Test
    public void testNovaSubmissao() {
        System.out.println("novaSubmissao");
        Evento instance = new Evento();
        Submissao expResult = new Submissao();
        expResult.setArtigo(new Artigo());
        Submissao result = instance.novaSubmissao();
        result.setArtigo(new Artigo());
        assertEquals(expResult, result);
    }

    /**
     * Test of addSubmissao method, of class Evento.
     */
    @Test
    public void testAddSubmissao() {
        System.out.println("addSubmissao");
        Submissao submissao = new Submissao();
        submissao.setArtigo(new Artigo());
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.addSubmissao(submissao);
        assertEquals(expResult, result);
    }

    /**
     * Test of getListaSubmissoes method, of class Evento.
     */
    @Test
    public void testGetListaSubmissoes() {
        System.out.println("getListaSubmissoes");
        Evento instance = new Evento();
        Submissao sub = new Submissao();
        Artigo art = new Artigo();
        art.setTitulo("Artigo1");
        sub.setArtigo(art);
        instance.addSubmissao(sub);
        List<Submissao> lista_teste = new ArrayList<>();
        lista_teste.add(sub);
        List<Submissao> expResult = lista_teste;
        instance.setListaSubmissoes(lista_teste);
        List<Submissao> result = instance.getListaSubmissoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCP method, of class Evento.
     */
    @Test
    public void testGetCP() {
        System.out.println("getCP");
        Evento instance = new Evento();
        CP teste = new CP();
        Utilizador util = new Utilizador();
        util.setNome("Tiago");
        teste.addMembroCP("user1", util);
        CP expResult = teste;
        instance.setCP(teste);
        CP result = instance.getCP();
        assertEquals(expResult, result);

    }

    /**
     * Test of getTopicos method, of class Evento.
     */
    @Test
    public void testGetTopicos() {
        System.out.println("getTopicos");
        Evento instance = new Evento();
        Topico tp = instance.novoTopico();
        tp.setDescricao("DERP");
        tp.setCodigoACM("12345");
        List<Topico> lista_teste = new ArrayList<>();
        lista_teste.add(tp);
        List<Topico> expResult = lista_teste;
        instance.setTopicos(lista_teste);
        List<Topico> result = instance.getTopicos();
        assertEquals(expResult, result);
    }

    /**
     * Test of novoTopico method, of class Evento.
     */
    @Test
    public void testNovoTopico() {
        System.out.println("novoTopico");
        Evento instance = new Evento();
        Topico expResult = new Topico();
        Topico result = instance.novoTopico();
        assertEquals(expResult, result);
    }

    /**
     * Test of addTopico method, of class Evento.
     */
    @Test
    public void testAddTopico() {
        System.out.println("addTopico");
        Topico t = new Topico();
        t.setDescricao("DERP");
        t.setCodigoACM("12345");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.addTopico(t);
        assertEquals(expResult, result);
    }

    /**
     * Test of validaTopico method, of class Evento.
     */
    @Test
    public void testValidaTopico() {
        System.out.println("validaTopico");
        Topico t = new Topico();
        t.setDescricao("Topico1/t");
        t.setCodigoACM("1\\.2\\.3");
        Evento instance = new Evento();
        boolean expResult = true;
        boolean result = instance.validaTopico(t);
        assertEquals(expResult, result);
    }

    /**
     * Test of getWebsite method, of class Evento.
     */
    @Test
    public void testGetWebsite() {
        System.out.println("getWebsite");
        Evento instance = new Evento();
        String website = "http://google.com";
        String expResult = website;
        instance.setWebsite(website);
        String result = instance.getWebsite();
        assertEquals(expResult, result);
    }

    /**
     * Test of setWebsite method, of class Evento.
     */
    @Test
    public void testSetWebsite() {
        System.out.println("setWebsite");
        Evento instance = new Evento();
        String website = "http://google.com";
        String expResult = website;
        instance.setWebsite(website);
        String result = instance.getWebsite();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTitulo method, of class Evento.
     */
    @Test
    public void testGetTitulo() {
        System.out.println("getTitulo");
        Evento instance = new Evento();
        String titulo = "Titulo";
        String expResult = titulo;
        instance.setTitulo(titulo);
        String result = instance.getTitulo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataInicio method, of class Evento.
     */
    @Test
    public void testGetDataInicio() {
        System.out.println("getDataInicio");
        Evento instance = new Evento();
        Calendar dataInicio = new GregorianCalendar();
        dataInicio.set(Calendar.YEAR, 1995);
        dataInicio.set(Calendar.MONTH, 8);
        dataInicio.set(Calendar.DAY_OF_MONTH, 17);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        instance.setDataInicio(dataInicio);
        Calendar expResult = dataInicio;
        Calendar result = instance.getDataInicio();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataFim method, of class Evento.
     */
    @Test
    public void testGetDataFim() {
        System.out.println("getDataFim");
        Calendar dataFim = new GregorianCalendar();
        dataFim.set(Calendar.YEAR, 1995);
        dataFim.set(Calendar.MONTH, 8);
        dataFim.set(Calendar.DAY_OF_MONTH, 17);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Evento instance = new Evento();
        instance.setDataFim(dataFim);
        Calendar result = dataFim;
        Calendar expResult = instance.getDataFim();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDescricao method, of class Evento.
     */
    @Test
    public void testGetDescricao() {
        System.out.println("getDescricao");
        String strDescricao = "é um evento";
        Evento instance = new Evento();
        instance.setDescricao(strDescricao);
        String result = instance.getDescricao();
        String expResult = "é um evento";
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataLimiteSubmissao method, of class Evento.
     */
    @Test
    public void testGetDataLimiteSubmissao() {
        System.out.println("getDataLimiteSubmissao");
        Calendar dataSubmissao = new GregorianCalendar();
        dataSubmissao.set(Calendar.YEAR, 1995);
        dataSubmissao.set(Calendar.MONTH, 8);
        dataSubmissao.set(Calendar.DAY_OF_MONTH, 17);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Evento instance = new Evento();
        instance.setDataLimiteSubmissao(dataSubmissao);
        Calendar result = dataSubmissao;
        Calendar expResult = instance.getDataLimiteSubmissao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLocal method, of class Evento.
     */
    @Test
    public void testSetLocal() {
        System.out.println("setLocal");
        Evento instance = new Evento();
        Local teste = new Local();
        teste.setCidade("Porto");
        teste.setPais("Portugal");
        Local result = null;
        instance.setLocal(teste);
        Local expResult = teste;
        result = instance.getLocal();
        assertEquals(expResult, result);
    }

    /**
     * Test of setState method, of class Evento.
     */
    @Test
    public void testSetState() {
        System.out.println("setState");
        EventoState state = new EventoRegistadoState(eventTest);
        eventTest.setState(state);
        assertEquals(state, eventTest.getState());
    }

    /**
     * Test of setListaSubmissoes method, of class Evento.
     */
    @Test
    public void testSetListaSubmissoes() {
        System.out.println("setListaSubmissoes");
        Evento instance = new Evento();
        Submissao sub = new Submissao();
        Artigo art = new Artigo();
        art.setTitulo("Artigo1");
        sub.setArtigo(art);
        instance.addSubmissao(sub);
        List<Submissao> lista_teste = new ArrayList<>();
        lista_teste.add(sub);
        List<Submissao> expResult = lista_teste;
        instance.setListaSubmissoes(lista_teste);
        List<Submissao> result = instance.getListaSubmissoes();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTopicos method, of class Evento.
     */
    @Test
    public void testSetTopicos() {
        System.out.println("setTopicos");
        Evento instance = new Evento();
        Topico tp = instance.novoTopico();
        tp.setDescricao("DERP");
        tp.setCodigoACM("12345");
        List<Topico> lista_teste = new ArrayList<>();
        lista_teste.add(tp);
        List<Topico> expResult = lista_teste;
        instance.setTopicos(lista_teste);
        List<Topico> result = instance.getTopicos();
        assertEquals(expResult, result);
    }

    /**
     * Test of getHost method, of class Evento.
     */
    @Test
    public void testGetHost() {
        System.out.println("getHost");
        Evento instance = new Evento();
        String host="Tiago";
        instance.setHost(host);
        String expResult = host;
        String result = instance.getHost();
        assertEquals(expResult, result);
    }

    /**
     * Test of setHost method, of class Evento.
     */
    @Test
    public void testSetHost() {
        System.out.println("setHost");
        Evento instance = new Evento();
        String host="Tiago";
        instance.setHost(host);
        String expResult = host;
        String result = instance.getHost();
        assertEquals(expResult, result);
    }

    /**
     * Test of setM_listaOrganizadores method, of class Evento.
     */
    @Test
    public void testSetM_listaOrganizadores() {
        System.out.println("setM_listaOrganizadores");
        Evento instance = new Evento();
        Utilizador util = new Utilizador();
        util.setNome("Tiago");
        Organizador org = new Organizador(util);
        List<Organizador> lista_teste = new ArrayList<>();
        lista_teste.add(org);
        instance.setM_listaOrganizadores(lista_teste);
        List<Organizador> expResult = lista_teste;
        List<Organizador> result = instance.getListaOrganizadores();
        assertEquals(expResult, result);
    }

    /**
     * Test of getDataLimiteSubmissaoFinal method, of class Evento.
     */
    @Test
    public void testGetSetDataLimiteSubmissaoFinal() {
        System.out.println("get e setDataLimiteSubmissaoFinal");
        Evento instance = new Evento();
        Calendar date = new GregorianCalendar(2013,1,28);
        instance.setDataLimiteSubmissaoFinal(date);
        Calendar result = instance.getDataLimiteSubmissaoFinal();
        assertEquals(date, result);
    }

    /**
     * Test of getDataLimiteRegisto method, of class Evento.
     */
    @Test
    public void testGetSetDataLimiteRegisto() {
        System.out.println("get e setDataLimiteRegisto");
        Evento instance = new Evento();
        Calendar date = new GregorianCalendar(2013,1,28);;
        instance.setDataLimiteRegisto(date);
        Calendar result = instance.getDataLimiteRegisto();
        assertEquals(date, result);
    }

    /**
     * Test of getDataLimiteRevisao method, of class Evento.
     */
    @Test
    public void testGetDataLimiteRevisao() {
        System.out.println("get e setDataLimiteRevisao");
        Evento instance = new Evento();
        Calendar date = new GregorianCalendar(2013,1,28);
        instance.setDataLimiteRevisao(date);
        Calendar result = instance.getDataLimiteRevisao();
        assertEquals(date, result);
    }

    /**
     * Test of setRegistado method, of class Evento.
     */
    @Test
    public void testSetRegistado() {
        System.out.println("setRegistado");
        boolean expResult = true;
        boolean result = eventTest.setRegistado();
        assertEquals(expResult, result);
    }

}
