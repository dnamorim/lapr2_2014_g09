/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import eventoscientificos.model.Utilizador;
import eventoscientificos.model.Autor;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amartins
 */
public class AutorTest {
    
    public AutorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setNome method, of class Autor.
     */
    @Test
    public void testSetGetNome() {
        System.out.println("setGetNome");
        String strNome = "David Malan";
        Autor instance = new Autor();
        instance.setNome(strNome);
        String expResult = "David Malan";
        String result = instance.getNome();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAfiliacao method, of class Autor.
     */
    @Test
    public void testSetGetAfiliacao() {
        System.out.println("setGetAfiliacao");
        String strAfiliacao = "Harvard University";
        Autor instance = new Autor();
        instance.setAfiliacao(strAfiliacao);
        String expResult = "Harvard University";
        String result = instance.getAfiliacao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setEMail method, of class Autor.
     */
    @Test
    public void testSetEMail() {
        System.out.println("setEMail");
        String strEMail = "malan@harvard.edu";
        Autor instance = new Autor();
        instance.setEMail(strEMail);
        String expResult = "malan@harvard.edu";
        String result = instance.getEMail();
        assertEquals(expResult, result);
    }

    /**
     * Test of setUtilizador method, of class Autor.
     */
    @Test
    public void testSetUtilizador() {
        System.out.println("setUtilizador");
        Utilizador user = new Utilizador("malan", "letmein", "David Malan", "malan@harvard.edu");
        Autor instance = new Autor();
        instance.setUtilizador(user);
        Utilizador expResult = user;
        Utilizador result = instance.getUtilizador();
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Autor.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Autor instance = new Autor("David Malan", "Harvard Univeristy", "malan@harvard.edu");
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Autor.
     */
    @Test
    public void testValidaNot() {
        System.out.println("valida not");
        Autor instance = new Autor("David Malan", "Harvard Univeristy", " Malan@Harvard.edu ");
        boolean expResult = false;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }
    
    
    
    /**
     * Test of podeSerCorrespondente method, of class Autor.
     */
    @Test
    public void testPodeSerCorrespondente() {
        System.out.println("podeSerCorrespondente");
        Utilizador u = new Utilizador("malan", "jharvard", "David Malan", "malan@harvard.edu");
        Autor instance = new Autor(u, "Harvard University");
        boolean expResult = true;
        boolean result = instance.podeSerCorrespondente();
        assertEquals(expResult, result);
    }

    /**
     * Test of podeSerCorrespondente method, of class Autor.
     */
    @Test
    public void testPodeSerCorrespondenteNot() {
        System.out.println("podeSerCorrespondenteNot");
        Autor instance = new Autor("David Malan", "Harvard Univeristy", " Malan@Harvard.edu ");
        boolean expResult = false;
        boolean result = instance.podeSerCorrespondente();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of toString method, of class Autor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Autor instance = new Autor("David Malan", "Harvard University", "malan@harvard.edu");
        String expResult = String.format("Autor:%nNome: %s Afiliação: %s E-Mail: %s", "David Malan", "Harvard University", "malan@harvard.edu");
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Autor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Autor obj = new Autor("Ângelo Silva", "INESC", "amm@isep.ipp.pt");
        Autor instance = new Autor("Ângelo Martins", "ISEP", "amm@isep.ipp.pt");
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }

    
    /**
     * Test of equals method, of class Autor.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("equals Not");
        Autor obj = new Autor("Ângelo Martins", "ISEP", "amm@isep.ipp.pt");
        Autor instance = new Autor("David Malan", "Harvard University", "malan@isep.ipp.pt");
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    
}
