/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author dnamorim
 */
public class RevisorTest {
    
    public RevisorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setListaTopicos method, of class Revisor.
     */
    @Test
    public void testSetGetListaTopicos() {
        System.out.println("set e getListaTopicos");
        Topico t1 = new Topico("H.2.1", "Logical Design---Schema and subschema");
        Topico t2 = new Topico("H.2.3", "[Database Management]: Languages---SQL");
        
        List<Topico> listaTopicos = new ArrayList<Topico>();
        listaTopicos.add(t1);
        listaTopicos.add(t2);
        
        Revisor instance = new Revisor(new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt"));
        instance.setListaTopicos(listaTopicos);
        
        //Clonagem da Lista para não partilhar referências de memória
        List<Topico> expResult = new ArrayList<Topico>(listaTopicos.size());
        for (Topico t : listaTopicos) {
            expResult.add(new Topico(t.getCodigoACM(), t.getDescricao()));
        }
        
        assertEquals(expResult, instance.getListaTopicos());
    }

    /**
     * Test of valida method, of class Revisor.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Revisor instance = new Revisor(new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt"));
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of getNome method, of class Revisor.
     */
    @Test
    public void testGetNome() {
        System.out.println("getNome");
        Revisor instance = new Revisor(new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt"));
        String expResult = "Duarte Amorim";
        String result = instance.getNome();
    }

    /**
     * Test of getUtilizador method, of class Revisor.
     */
    @Test
    public void testGetUtilizador() {
        System.out.println("getUtilizador");
        Revisor instance = new Revisor(new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt"));
        Utilizador expResult = new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt");
        Utilizador result = instance.getUtilizador();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Revisor.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Utilizador u = new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt");
        Revisor instance = new Revisor(u);
        List<Topico> listaTopicos = new ArrayList<Topico>();
        Topico t = new Topico("H.2.1", "Logical Design---Schema and subschema");
        listaTopicos.add(t);
        
        instance.setListaTopicos(listaTopicos);
        
        String expResult = String.format("%s: %s", u, t);
        String result = instance.toString();
        assertEquals(expResult, result);
    }


    /**
     * Test of equals method, of class Revisor.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Object otherObj = new Revisor(new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt"));
        Revisor instance = new Revisor(new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt"));
        boolean expResult = true;
        boolean result = instance.equals(otherObj);
        assertEquals(expResult, result);
    }
    
        /**
     * Test of equals method, of class Revisor.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("equals not");
        Object otherObj = new Revisor(new Utilizador("Duarte Amorim", "dnamorim", "letmein", "1130674@isep.ipp.pt"));
        Revisor instance = new Revisor(new Utilizador("Pedro Amorim", "pamorim", "letmein", "pda@isep.ipp.pt"));
        boolean expResult = false;
        boolean result = instance.equals(otherObj);
        assertEquals(expResult, result);
    }
    
}
