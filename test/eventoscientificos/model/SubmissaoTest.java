/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Eduardo Pinto <1130466@isep.ipp.pt>
 */
public class SubmissaoTest {
    
    public SubmissaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    public Artigo a = new Artigo();
    
    public void createArtigo() {
        a.setTitulo("TDD e JUnit");
        a.setResumo("Um estudo sobre Test Driven Development e JUnit");
        a.setFicheiro("/Users/TOCS/tdd_junit_paper.pdf");
        a.setTipo(TipoArtigo.SHORT);
    }
    
    /**
     * Test of novoArtigo method, of class Submissao.
     */
    @Test
    public void testNovoArtigo() {
        System.out.println("novoArtigo");
        Submissao instance = new Submissao();
        Artigo expResult = new Artigo();
        Artigo result = instance.novoArtigo();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInfo method, of class Submissao.
     */
    @Test
    public void testGetInfo() {
        System.out.println("getInfo");
        createArtigo();
        Submissao instance = new Submissao();
        String expResult = String.format("Submissão:%n%s", a);
        instance.setArtigo(a);
        String result = instance.getInfo();
        assertEquals(expResult, result);
    }

    /**
     * Test of setArtigo method, of class Submissao.
     */
    @Test
    public void testGetSetArtigo() {
        System.out.println("get e setArtigo");
        Submissao instance = new Submissao();
        instance.setArtigo(a);
        Artigo expResult = a;
        assertEquals(expResult, instance.getArtigo());
    }

    /**
     * Test of valida method, of class Submissao.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Submissao instance = new Submissao();
        instance.setArtigo(a);
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Submissao.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        createArtigo();
        Submissao instance = new Submissao();
        instance.setArtigo(a);
        String expResult = String.format("Submissão:%n%s", a);
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Submissao.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        createArtigo();
        
        Object obj = new Submissao();
        Artigo a1 = ((Submissao) obj).novoArtigo();
        a1.setTitulo(a.getTitulo());
        a1.setResumo(a.getResumo());
        a1.setTipo(a.getTipo());
        a1.setFicheiro(a.getFicheiro());
        ((Submissao) obj).setArtigo(a1);
        
        Submissao instance = new Submissao();
        instance.setArtigo(a);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class Submissao.
     */
    @Test
    public void testEqualsNot() {
        System.out.println("equals not");
        createArtigo();
        Object obj = new Submissao();
        Artigo a1 = ((Submissao) obj).novoArtigo();
        a1.setTitulo("Padrões GRASP");
        a1.setResumo("Padrões GRASP aplicados ao design de software.");
        a1.setTipo(TipoArtigo.FULL);
        a1.setFicheiro("/Users/TOCS/grasp_paper.pdf");
        ((Submissao) obj).setArtigo(a1);
        
        Submissao instance = new Submissao();
        instance.setArtigo(a);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
    
}
