/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author i130672
 */
public class LocalTest {
    
    public LocalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setCidade method, of class Local.
     */
    @Test
    public void testSetCidade() {
        System.out.println("setCidade");
        String strLocal = "Porto";
        Local instance = new Local();
        instance.setCidade(strLocal);
        String result=instance.getCidade();
        assertEquals(strLocal,result);
    }

    /**
     * Test of setPais method, of class Local.
     */
    @Test
    public void testSetPais() {
        System.out.println("setPais");
        String strLocal = "Portugal";
        Local instance = new Local();
        instance.setPais(strLocal);
        String result=instance.getPais();
        assertEquals(strLocal,result);
    }

    /**
     * Test of toString method, of class Local.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Local instance = new Local();
        instance.setCidade("Porto");
        instance.setPais("Portugal");
        String expResult = "Portugal\nPorto";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of getCidade method, of class Local.
     */
    @Test
    public void testGetCidade() {
        System.out.println("getCidade");
        Local instance = new Local();
        instance.setCidade("Porto");
        String expResult = "Porto";
        String result = instance.getCidade();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPais method, of class Local.
     */
    @Test
    public void testGetPais() {
        System.out.println("getPais");
        Local instance = new Local();
        instance.setPais("Portugal");
        String expResult = "Portugal";
        String result = instance.getPais();
        assertEquals(expResult, result);
    }
    
}
