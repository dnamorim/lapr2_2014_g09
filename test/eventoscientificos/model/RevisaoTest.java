/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author andrémiguel
 */
public class RevisaoTest {

    private Artigo artigo;
    private Revisor revisor;
    private Utilizador u;

    public RevisaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getConfianca method, of class Revisao.
     */
    @Test
    public void testSetGetConfianca() {
        System.out.println("getConfianca");
        Revisao instance = new Revisao(artigo, revisor);
        int conf = 3;
        int expResult = 3;
        instance.setConfianca(conf);
        int result = instance.getConfianca();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getAdequacao method, of class Revisao.
     */
    @Test
    public void testSetGetAdequacao() {
        System.out.println("getAdequacao");
        Revisao instance = new Revisao(artigo, revisor);
        int adeq = 2;
        int expResult = 2;
        instance.setAdequacao(adeq);
        int result = instance.getAdequacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getOriginalidade method, of class Revisao.
     */
    @Test
    public void testSetGetOriginalidade() {
        System.out.println("getOriginalidade");
        Revisao instance = new Revisao(artigo, revisor);
        int orig = 5;
        int expResult = 5;
        instance.setOriginalidade(orig);
        int result = instance.getOriginalidade();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of getQualidade method, of class Revisao.
     */
    @Test
    public void testSetGetQualidade() {
        System.out.println("getQualidade");
        Revisao instance = new Revisao(artigo, revisor);
        int qual = 4;
        int expResult = 4;
        instance.setQualidade(qual);
        int result = instance.getQualidade();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.    
    }

    /**
     * Test of getRecomendacao method, of class Revisao.
     */
    @Test
    public void testSetGetRecomendacao() {
        System.out.println("getRecomendacao");
        Revisao instance = new Revisao(artigo, revisor);
        boolean rec = false;
        boolean expResult = false;
        instance.setRecomendacao(rec);
        boolean result = instance.getRecomendacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getJustificacao method, of class Revisao.
     */
    @Test
    public void testSetGetJustificacao() {
        System.out.println("getJustificacao");
        Revisao instance = new Revisao(artigo, revisor);
        String just = "Artigo com mau portugues";
        String expResult = "Artigo com mau portugues";
        instance.setJustificacao(just);
        String result = instance.getJustificacao();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of toString method, of class Revisao.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Revisao instance = new Revisao(artigo, revisor);
        String novo = "Artigo: "
                + "Confiança: 3"
                + "Adequação: 2"
                + "Originalidade: 5"
                + "Recomendação: false"
                + "Justificação: Artigo com mau portugues";
        String expResult = novo;
        String result = instance.toString();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    /**
     * Test of getRevisor method, of class Revisao.
     */
    @Test
    public void testSetGetRevisor() {
        System.out.println("getRevisor");
        Revisao instance = new Revisao(artigo, revisor);
        Revisor novoRev = new Revisor(u);
        instance.setRevisor(novoRev);
        Revisor expResult = novoRev;
        Revisor result = instance.getRevisor();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.

    }

    

    @Test
    public void testGetEstado() {
        System.out.println("getEstado");
        Revisao instance = new Revisao(artigo,revisor);
        String resultado = "Aceite";
        String expResult = resultado;
        String result = instance.getEstado();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
     
    }

    /**
     * Test of valida method, of class Revisao.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Revisao instance = new Revisao(artigo,revisor);
        boolean resultado = true;
        boolean expResult = resultado;
        boolean result = instance.valida();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

}
