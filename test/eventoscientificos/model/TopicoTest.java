/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eventoscientificos.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dnamorim
 */
public class TopicoTest {
    
    public TopicoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of setDescricao method, of class Topico.
     */
    @Test
    public void testSetGetDescricao() {
        System.out.println("setGetDescricao");
        String strDescricao = "[Database Management]/ Languages---SQL";
        String expResult = "[Database Management]/ Languages---SQL";
        Topico instance = new Topico();
        instance.setDescricao(strDescricao);
        String result = instance.getDescricao();
        assertEquals(expResult, result);
    }

    /**
     * Test of setCodigoACM method, of class Topico.
     */
    @Test
    public void testSetGetCodigoACM() {
        System.out.println("setGetCodigoACM");
        String codigoACM = "H.2.3";
        String expResult = "H.2.3";
        Topico instance = new Topico();
        instance.setCodigoACM(codigoACM);
        String result = instance.getCodigoACM();
        assertEquals(expResult, result);
    }

    /**
     * Test of valida method, of class Topico.
     */
    @Test
    public void testValida() {
        System.out.println("valida");
        Topico instance = new Topico("H.2.3", "[Database Management]/ Languages---SQL");
        boolean expResult = true;
        boolean result = instance.valida();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class Topico.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Topico instance = new Topico("H.2.3", "[Database Management]/ Languages---SQL");
        String expResult = String.format("%s - %s", instance.getCodigoACM(), instance.getDescricao());
        assertEquals(expResult, instance.toString());
    }

    /**
     * Test of equals method, of class Topico.
     */
    @Test
    public void testEqualsTrue() {
        System.out.println("equals");
         Object outroObjecto = new Topico("H.2.3", "[Database Management]/ Languages---SQL");
        Topico instance = new Topico("H.2.3", "[Database Management]/ Languages---SQL");
        boolean expResult = true;
        boolean result = instance.equals(outroObjecto);
        assertEquals(expResult, result);
    }
    
    /**
     * Test of equals method, of class Topico.
     */
    @Test
    public void testNotEquals() {
        System.out.println("not equals");
        Object outroObjecto = new Topico("H.2.1", "Logical Design/ Schema and subschema");
        Topico instance = new Topico("H.2.3", "[Database Management]/ Languages---SQL");
        boolean expResult = false;
        boolean result = instance.equals(outroObjecto);
        assertEquals(expResult, result);
    }
    
    
    /**
     * Test of equals method, of class Topico.
     */
    @Test
    public void testEqualsSameDescription() {
        System.out.println("equals - same Description");
        Object outroObjecto = new Topico("H.2.1", "[Database Management]/ Languages---SQL");
        Topico instance = new Topico("H.2.3", "[Database Management]/ Languages---SQL");
        boolean expResult = false;
        boolean result = instance.equals(outroObjecto);
        assertEquals(expResult, result);
    }
}
